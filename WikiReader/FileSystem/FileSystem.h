#ifndef FILESYSTEM_PLUGIN_H
#define FILESYSTEM_PLUGIN_H

/**
 * @file
 * @brief This file contains implementation of FileSystem service. 
 *
 * This service implements interface ServiceFileFormat and is used to access filesystem directory in WikiReader.
 *
 * This file is part of module FileSystem.dll
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#pragma comment(lib, "../Shared/shared.lib")
#include"../Shared/Types.h"
#include "../Shared/Plugin.h"
#include "../Shared/Preferences.h"
#include "../Shared/StringUtils.h"

#include <fstream>
#include <windows.h>
#include <map>

using namespace std;

/** This template function finds if map contains given key.
 * @param input Input map.
 * @param key Search key.
 * @return True if input map contains key.
 */
template<class K,class T>
bool mapContains(const std::map<K,T> &input, const K &key) 
{
	map<K,T>::const_iterator it = input.lower_bound(key);
	if (it == input.end() || it->first != key )
		return false;
	return true;
}


/** Simple file system access service. */
class FileSystem : public ServiceFileFormat 
{
private:

	/// Indicates if this file format was opened.
	bool opened;
	/// Contains current working directory (containing configuration file).
	wstring cwd;
	/// Map which contains content plugins (values) asociated with various file extensions (key).
	map<string,ServiceContent*> contentPlugins;
	/// Path to directory which is acessible by this class.
	wstring src;

	/** Clear object state.
	 * @return Always false.
	 */
	bool clear() { 
		opened = false;
		cwd.clear();
		map<string,ServiceContent*>::iterator it;
		for(it = contentPlugins.begin();it != contentPlugins.end();++it) {
			delete it->second;
		}
		contentPlugins.clear();
		return false;
	};

public:

	/** Constructor. */
	FileSystem() : opened(false) { };

	/** Destructor. */
	~FileSystem() 
	{
		clear();
	}

	/** Close opened file and clear object settings. */
	void close() 
	{
		clear();
	}

	/** Release this object. */
	void release() 
	{
		delete this;
	}

	/** Release specified resource. 
	 * @param ptr Pointer to resource which we want to delete. 
	 */
	virtual void releaseResource(void *ptr) 
	{
		if (ptr != NULL)
			delete ptr;
	}

	/** Open configuration file. 
	 * @param filename Full path to configuration file.
	 * @return True if everything went ok.
	 */
	bool open(const std::wstring &filename) 
	{
		opened = false;

		// create Preferences object to acess configuration file
		Preferences pref;
		if (!pref.loadPreferences(filename))
			return false;

		// parse current working directory
		cwd = filename.substr(0,filename.find_last_of(PATH_SEPARATOR))+PATH_SEPARATOR;

		// get full path to directory which is accessible with this class 
		src = pref.getStringW("src");
		if (src == Preferences::werror) 
			return false;
		src = StringUtils::getFullPath(src,cwd);
		if (src.empty())
			return false;
		if (src[src.size()-1] != PATH_SEPARATOR)
			src.push_back(PATH_SEPARATOR);

		// get global PluginManager instance for easier access
		PluginManager *manager = PluginManager::instance();
		
		// load content plugins
		vector<string> elements;
		string plugin;
		string path;
		ServiceContent* cont;
		pref.getSectionElements("content",elements);
		for(size_t i=0; i<elements.size(); ++i) {
			path = "content/"+elements[i];
			plugin = pref.getString(path);
			if (plugin == pref.error) 
				continue;
			cont = dynamic_cast<ServiceContent*> (manager->getService(plugin));
			if (cont == NULL)
				continue;
			contentPlugins[elements[i]] = cont;
		}
		
		// set file opened property to true
		opened = true;
		return true;
	}

	/** Get data from accessible directory.
	 * @param id Filename relative to accessible directory.
	 * @param buf Output buffer in which the data will be stored.
	 * @return True if everything went ok.
	 */
	bool getData(const std::string &id, ByteBuffer &buf) 
	{
		// exit if file is not opened
		if (!opened)
			return false;

		ifstream input;

		// create absolute path to requested file
		wstring wid = StringUtils::utf8To16(id);
		wstring path = src+wid;

		// open requested file
		input.open(path.c_str());
		if (!input)
			return false;

		// find file size
		input.seekg(0,ios::end);
		size_t size = input.tellg();
		input.seekg(0,ios::beg);

		// read whole file into buffer
		buf.length = size;
		buf.data = new Byte[size];
		input.read((char*)buf.data,size);	

		// close file
		input.close();

		return true;
	}
	
	/** Process request for resource.
	 * @param req Input request.
	 * @return True if everything went ok.
	 */
	bool process(Request &req) 
	{
		// exit if file is not opened
		if (!opened)
			return false;

		const string &id = req.getId();
		ifstream input;

		// create absolute path to requested file
		wstring wid = StringUtils::utf8To16(id);
		wstring path = src+wid;

		// open requested file
		input.open(path.c_str(), ios::in | ios::binary);
		if (!input)	
			return false;

		// get extension of requested file
		string ext = id.substr(id.find_last_of('.')+1);
		// find if extension is "known" (specified in configuration file)
		if (mapContains<string,ServiceContent*>(contentPlugins,ext)) {
			// proceed with extension based conversion

			// create output buffer
			string outdata;
			outdata.reserve(512);

			// html header
			outdata = "<html>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n"; 
			outdata += "<head>\n<title>";
			outdata += id;
			outdata += "</title>\n</head>\n<body>\n";

			// start response and write header
			req.startResponse(200);
			req.writeChunk(outdata);
			outdata.clear();

			// find file size
			input.seekg(0,ios::end);
			size_t size = input.tellg();
			input.seekg(0,ios::beg);

			// raad whole file into buffer
			ByteBuffer buf;
			buf.length = size;
			buf.data = new Byte[size];
			input.read((char*)buf.data,size);
			
			// convert file data with content service
			ServiceContent *cont = contentPlugins[ext];
			if (!cont->convert(buf,req)) {
				// conversion failed -> cleanup
				delete buf.data;
				return false;
			}

			// write end of html file
			req.writeChunk("\n</body>\n</html>");

			// cleanup
			delete buf.data;
			input.close();

			return true;
		}

		input.seekg(0,ios::beg);

		// allocate 16k buffer
		ByteBuffer buf;
		buf.data = new Byte[16384];
		buf.length = 16384;

		req.startResponse(200);

		// read contents of file and write them to request source
		while(input) {
			input.read((char*)buf.data,buf.length);
			req.writeChunk((char*)buf.data,input.gcount());
		}	
		
		// cleanup
		delete buf.data;
		input.close();

		return true;
	}
};


#endif
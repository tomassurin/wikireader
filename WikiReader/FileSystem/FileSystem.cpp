/**
 * @file
 * @brief This file contains definitions needed for this module to be used as a plugin.
 *
 * This file is part of module FileSystem.dll
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include <string>

#pragma comment(lib, "../Shared/shared.lib")
#include "../Shared/StringUtils.h"
#include "../Shared/Types.h"

#include "FileSystem.h"

/// ID of this plugin.
static const char *id = "FileSystemDll:Plugin to enable access to filesystem";
/// Services of this plugin separated by ";".
static const char *services = "FileSystem";

/** Factory function that creates instances of services in this plugin.
 * @param service Identifier of service (case-insensitive).
 * @return Pointer to created service object. This object must be deleted by calling its release() method (not delete).
 */
DLLEXPORT ServiceBase* getInstance(const char *service)
{
	std::string svc(service);
	svc = StringUtils::toLower(svc);
	ServiceBase *out = NULL;
	if (svc == "filesystem")
		out = new FileSystem();

	return out;
}

/** Get available services.
 * @return Constant pointer to services string.
 */
DLLEXPORT const char* getServices() {
	return services;
}

/** Get ID.
 * @return constant pointer to id string.
 */
DLLEXPORT const char* getId() {
	return id;
}

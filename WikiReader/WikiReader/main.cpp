/** @mainpage
<h1> WikiReader </h1>

WikiReader is program for Windows Mobile powered devices. It's purpose is to give
offline access to data from Wikipedia or other WikiMedia powered project.
*/

/**
 * @file
 * This file contains entry point for program WikiReader.
 *
 * This file is part of module WikiReader.exe
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 * @todo Add templates and parser functions support.
 * @todo Add support for case and accent non-sensitive search. 
 * @todo Add references support.
 * @todo Add support for math symbols and possibly images?
 * @todo Add support for categories.
 * @todo Add support for other online data sources.
 * @todo Add support for interproject linking.
 * @todo Port to other platforms?
 */

#include <windows.h>
#include <stdio.h>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <ios>

#pragma comment(lib, "../Shared/shared.lib")
#include "../Shared/Types.h"
#include "../Shared/Plugin.h"
#include "../Shared/Preferences.h"
#include "../Shared/StringUtils.h"
#include "RequestHandler.h"

using namespace std;

/** Entry point of Windows Mobile based applications. */
int WINAPI _tWinMain(HINSTANCE hcurr/*hInstance*/, HINSTANCE hprev/*hPrevInstance*/, 
                                LPTSTR args/*lpCmdLine*/, int nShowCmd)
{
	RequestHandler *handler = RequestHandler::instance();

	// initialize preferences
	if (!handler->initPreferences(Preferences::exePath()+L"config.xml")) {
		if (!handler->initPreferences(L"\\Storage Card\\wiki\\config.xml")) {
			if (!handler->initPreferences(L"\\SD Card\\wiki\\config.xml"))
				Preferences::instance()->appendToLog("Fatal Error: Can't open config file");
				return 1;
		}
	}

	// get server properties from preferences file
	size_t threads = Preferences::instance()->getInteger("server/threads");
	int port = Preferences::instance()->getInteger("server/port");

	// start server with given preferences
	if (port > 0 && port < 65536) {
		Webserver ws(port,threads);
	}
	else 
		Webserver ws(80,threads);

	return 0;
}
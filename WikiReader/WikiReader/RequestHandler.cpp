/**
 * @file
 * @brief This file contains implementation for RequestHandler.h.
 * 
 * This file is part of module WikiReader.exe
 *
 * @author Tom� �ur�n
 * @version 1.0
*/

#include "RequestHandler.h"
#include <string>

#include "../Shared/MiscUtils.h"

using namespace std;

RequestHandler *RequestHandler::_instance = NULL;

bool RequestHandler::initPreferences(const wstring &config) 
{
	ok = false;
	pref = Preferences::instance();
	plugman = PluginManager::instance();
	assert(pref != NULL && plugman != NULL);

	if (!pref->loadPreferences(config)) 
		return false;

	// parse current work directory
	wstring cwd = config.substr(0,config.find_last_of(PATH_SEPARATOR))+PATH_SEPARATOR;
	vector<string> elements;
	string path;
	wstring src;
	string svc;

	// register and load system plugins
	if (!plugman->registerService("WikiFile",Preferences::exePath()+L"WikiData.dll","WikiFile"))
		Preferences::appendToLog("Error: Can't register service \"WikiFile\"");
	if (!plugman->registerService("FileSystem",Preferences::exePath()+L"FileSystem.dll","FileSystem"))
		Preferences::appendToLog("Error: Can't register service \"FileSystem\"");
	if (!plugman->registerService("WZip",Preferences::exePath()+L"WikiData.dll","WZip"))
		Preferences::appendToLog("Error: Can't register service \"WZip\"");
	if (!plugman->registerService("WikiTitleIndex",Preferences::exePath()+L"WikiData.dll","WikiTitleIndex"))
		Preferences::appendToLog("Error: Can't register service \"WikiTitleIndex\"");
	if (!plugman->registerService("WikiContent",Preferences::exePath()+L"WikiData.dll","WikiContent"))
		Preferences::appendToLog("Error: Can't register service \"WikiContent\"");

	// register and load plugins specified in config file
	pref->getSectionElements("/plugin/",elements);
	for(size_t i=0; i<elements.size(); ++i) {
		path = "/plugin/"+elements[i];
		src = pref->getStringW(path+"/src");
	    svc = pref->getString(path+"/service");
		if (src == pref->werror || svc == pref->error ||
			!plugman->registerService(elements[i],StringUtils::getFullPath(src,cwd),svc)) { 
			Preferences::appendToLog("Error: Can't register service \""+elements[i]+"\"");
			continue;
		}
	}

	// load skin preferences
	cssstyle = pref->getString("/style");
	if (cssstyle == pref->error) {
		cssstyle.clear();
	}

	// load data files
	elements.clear();
	pref->getSectionElements("/data/",elements);
	string mountdir;
	string plugin;
	ServiceFileFormat *data;
	DataFile *datafile = NULL;

	for(size_t i=0; i<elements.size(); ++i) {
		path = "/data/"+elements[i];
		src = pref->getStringW(path+"/src");
		mountdir = pref->getString(path+"/mountdir");
		plugin = pref->getString(path+"/plugin");		
		if (plugin == pref->error)
			plugin.clear();
		if (src == pref->werror || mountdir == pref->error || 
			(data = loadDataFile(StringUtils::getFullPath(src,cwd),plugin)) == NULL) {
			Preferences::appendToLog("Error: Can't load data file \""+elements[i]+"\"");
			continue;
		}

		data->setPreference("mountdir",mountdir);
		data->setPreference("style",cssstyle);
		datafile = new DataFile;
		datafile->data = data;
		datafile->mountdir = mountdir;
		dataFiles.push_back(datafile);
		datafile = NULL;
	}

	ok = true;
	return true;
}

ServiceFileFormat* RequestHandler::loadDataFile(const wstring &datapath, std::string &fileformat) 
{
	ServiceFileFormat* datafile;
	
	if (fileformat.empty()) {
		// read file format from configuration file of datafile
		Preferences datapref;
		if (!datapref.loadPreferences(datapath))
			return NULL;
		fileformat = datapref.getString("/format");
		if (fileformat == datapref.error)
			return NULL;
		if (fileformat.empty())
			return NULL;
	}
	
	// open datafile
	datafile = dynamic_cast<ServiceFileFormat*>(plugman->getService(fileformat));
	if (datafile == NULL)
		return NULL;
	if (!datafile->open(datapath))
		return NULL;
	return datafile;
}

RequestHandler::DataFile *RequestHandler::getDataFile(const std::string &path) {
	size_t matchlength = 0;
	DataFile *bestmatch = NULL;
	DataFile *tmp;
	for(size_t i=0; i<dataFiles.size(); ++i) {
		tmp = dataFiles[i];
		if ( tmp->mountdir.size() > matchlength && StringUtils::startsWith(path,tmp->mountdir)) {
			matchlength = tmp->mountdir.size();
			bestmatch = tmp;				 	
		}
	}
	return bestmatch;
}

void RequestHandler::writeInfo(HttpRequest &r)
{
	if (info.empty()) {
		info.reserve(512);
		info = "<html>\n";
		info += "<head>\n";
		info += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n";
		if (!cssstyle.empty())
			info += "<link rel=\"stylesheet\" href=\""+cssstyle+"\" type=\"text/css\"/>\n";
		info += "<title>WikiReader</title>\n</head>\n<body>\n";
		info += "<a href=\"/Special:quitreader\">Shutdown server</a>";
		for(size_t i=0;i<dataFiles.size();++i) {
			info+= dataFiles[i]->data->getInfo();
			info+="<br/>";
		}
		info+="</body></html>";

	}
	r.writeChunk(info);
}

bool RequestHandler::handleRequest(HttpRequest &r) 
{
	if (r.path_ == "/Special:quitreader") {
		// exit server
		pref->appendToLog("Program quits");
		r.startResponse(200);
		r.answer_  = "<html>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n"; 
		r.answer_ += "<head>\n<title>";
		r.answer_ += "Server shutdown";
		r.answer_ += "</title>\n</head>\n<body>\n";
		r.answer_ += "<h1>Shutting down ...</h1>";
		r.answer_ += "\n</body>\n</html>";
		r.writeChunk(r.answer_);
		// wm doesn't exit after this ?WTF?
		exit(0);
		return false;
	}

	int status = 200;
	string title;
	string body;

	if (!ok) {
		status = 500; // "500 Internal Server Error";		
		title = "Internal Server Error";
	} else {
		 if (r.path_ == "/") {
			// root dir - write info
			r.startResponse(200);
			writeInfo(r);
			return true;
		}

		// get datafile which mountdir best matches with requested path
		DataFile *datafile = getDataFile(r.path_);

		if (datafile == NULL ) {
			// no data file mountdir matches with requested path
			status = 404; // "404 Not Found";
			title      = "Wrong URL";
			body       = "<h1>Wrong URL</h1>";
			
		}
		else {
			// process request in data file
			title = StringUtils::replaceInStrOne(r.path_,datafile->mountdir,"");
			title = title.substr(0,title.find_last_of('#'));
			r.setId(title);
			if (!datafile->data->process(r)) {
				status = 404;
				body = "<h1>Wrong URL</h1>";
			}	
			else 
				return true;
		}
	}

	// write responses (for error states)
	r.startResponse(status);
	r.answer_  = "<html>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n"; 
	r.answer_ += "<head>\n<title>";
	r.answer_ += title;
	r.answer_ += "</title>\n</head>\n<body>\n";
	r.answer_ += body;
	r.answer_ += "\n</body>\n</html>";
	r.writeChunk(r.answer_);
	return true;
}

RequestHandler::~RequestHandler() 
{
	for(size_t i=0;i<dataFiles.size();++i) {
		dataFiles[i]->data->release();
		delete dataFiles[i];
	}
	dataFiles.clear();
}

RequestHandler *RequestHandler::instance()
{
	if (_instance == NULL) {
		_instance = new RequestHandler();
	}	
	return _instance;
}
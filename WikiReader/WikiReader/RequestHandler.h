#ifndef WIKIREQUESTHANDLER_H
#define WIKIREQUESTHANDLER_H

/**
 * @file
 * @brief This file contains declaration of RequestHandler class. 
 * 
 * This file is part of module WikiReader.exe
 *
 * @author Tom� �ur�n
 * @version 1.0
*/

#include <windows.h>
#include "HttpServer/webserver.h"

#pragma comment(lib, "../Shared/shared.lib")
#include "../Shared/Plugin.h"
#include "../Shared/Preferences.h"
#include "../Shared/StringUtils.h"
#include "../Shared/Types.h"
#include "HttpServer/HttpRequest.h"

#include <string>

/** Singleton class. It contains preferences which are initialized from configuration file.
 * It also handles requests from HTTP server. 
 */
class RequestHandler 
{
private:

	/** Structure which contains information about 
	 * file format services and their relative mountdirs. 
	 */
	struct DataFile {
		/// Pointer to instance of file format service.
		ServiceFileFormat *data;
		/// Directory of server from which file format service contents are available.
		std::string mountdir;
	};

	/// Used to store information about loaded data files.
	std::string info;
	/// Contains URL to CSS style.
	std::string cssstyle;
	/// Vector containing loaded data files.
	std::vector<DataFile*> dataFiles;
	/// Pointer to global preferences instance.
	Preferences *pref;
	/// Pointer to global plugin manager instance.
	PluginManager *plugman;
	/// Identifies if this object is in good state (preferences are loaded ...)
	bool ok;

	/** Constructor. */
	RequestHandler() : ok(false), pref(NULL), plugman(NULL) { };
	/** Destructor. */
	~RequestHandler();
	
	/// Pointer to global instance of this class.
	static RequestHandler *_instance;
public:

	/** Access to global instance of this class.
	 * @return Pointer to global instance of this class.
	 */
	static RequestHandler *instance();

	/** Initialize preferences. These preferences are read from input file.
	 * This method also registers services and creates instances of file format services.
	 * @param config Path to configuration file.
	 * @return True if everything went ok.
	 */
	bool initPreferences(const std::wstring &config);

	/** Handles request given as parameter. This method is called from webserver::Request() method.
	 * @param r HTTP request from web server.
	 * @return True if everything went ok.
	 */
	bool handleRequest(HttpRequest &r);		
private:

	/** Loads data file. 
	 * @param path Path to configuration file of data file.
	 * @param format Name of service
	 * @return Pointer to instance of file format service with opened data file give by path argument.
	 */
	ServiceFileFormat *loadDataFile(const std::wstring &path, std::string &format);

	/** Writes info about loaded files to source of request given as parameter.
	 * @param r HTTP request from web server.
	 */
	void writeInfo(HttpRequest &r);

	/** Returns DataFile structure which mountdir best matches with argument path.
	 * @param path Path relative to HTTP server.
	 * @return Pointer to DataFile structure which mountdir best matches from start of path argument.
	 */
	DataFile *getDataFile(const std::string &path);
};

#endif
/*
   WebServer.cpp

   Copyright (C) 2003-2004 Ren� Nyffenegger

   This source code is provided 'as-is', without any express or implied
   warranty. In no event will the author be held liable for any damages
   arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this source code must not be misrepresented; you must not
      claim that you wrote the original source code. If you use this source code
      in a product, an acknowledgment in the product documentation would be
      appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
      misrepresented as being the original source code.

   3. This notice may not be removed or altered from any source distribution.

   Ren� Nyffenegger rene.nyffenegger@adp-gmbh.ch

   Thanks to Tom Lynn who pointed out an error in this source code.
*/

/**
 * @file
 * @brief This file contains implementation of class Webserver.
 *
 * This file is part of module WikiReader.exe
 *
 * @author Ren� Nyffenegger (part of his simple webserver)
 *         <a href="http://www.adp-gmbh.ch/win/misc/Webserver.html">src</a>
 * @author Tom� �ur�n (altered for use in WikiReader - Added really simple thread pool
 *         and removed unecessary stuff in Webserver::Request() method).
 * 
 * @version 1.0
 */

#include "../../Shared/wce/wce_time.h"
#include <iostream>
#include <string>
#include <map>
#include <sstream>
#include <algorithm>
#include <deque>
#include <windows.h>

#include "webserver.h"
#include "Socket.h"
#include "UrlHelper.h"
#include "base64.h"
#include "HttpRequest.h"

#include "../Requesthandler.h"

RequestHandler *Webserver::req_handler = 0;

using namespace std;

// decode non alphanumerical characters from url to hex encoded characters
std::string decodeUrlString(const std::string &str) 
{
	std::string out;
	char tmp[3];
	tmp[2] = 0;
	unsigned char b;
	for(size_t i=0;i < str.size();++i) {
		if (str[i] == '%') {
			tmp[0] = str[++i];
			tmp[1] = str[++i];
			b = (unsigned char)strtol(tmp,NULL,16);
			out.push_back((char)b);
			continue;
		}
		out.push_back(str[i]);
	}
	return out;
}

// queue of requests
static deque<Socket*> toprocess;
// mutex asociated with toprocess queue
static HANDLE toprocess_mutex;
// event asociated with toprocess queue
static HANDLE toprocess_event;

unsigned Webserver::Request(Socket *ptr_s) 
{
	Socket s = *ptr_s;

	std::string line;
	line.reserve(512);

	std::string path;
	std::map<std::string, std::string> params;
	std::string getrequest;
	size_t posStartPath;

	line.clear();
	s.ReceiveLine(line);

	if (line.empty()) {
		s.Close();
		return 0;
	}

	HttpRequest req;

	// get method from request
	if (line.find("GET") == 0) {
		req.method_="GET";
	}
	else if (line.find("POST") == 0) {
		req.method_="POST";
	}
	else {
		s.Close();
		return 1;
	}

	posStartPath = line.find_first_not_of(" ",3);
	getrequest = line.substr(posStartPath);

	SplitGetReq(getrequest, path, params);

	req.status_ = "200 OK";
	req.s_      = &s;
	req.path_   = decodeUrlString(path);

	for(map<string,string>::iterator it=params.begin();it != params.end();++it) {
		it->second = decodeUrlString(it->second);
	}

	req.params_ = params;

	// receive all headers
	while(1) {
		line.clear();
		s.ReceiveLine(line);

		if (line.empty()) break;

		unsigned int pos_cr_lf = line.find_first_of("\x0a\x0d");
		if (pos_cr_lf == 0) break;

		// we can do something here with headers ...
	}

	req.addHeader("Connection: Close");

	// handle this request
	if (!req_handler->handleRequest(req)) {
		// exit program
		req.writeFinalChunk();
		s.Close();
		return 1;
	}

	// write final chunk of chunked encoding
	req.writeFinalChunk();

	s.Close();

	return 0;
}

unsigned Webserver::ThreadProcess(void*) 
{
	Socket *sock;
	DWORD wait_result;

	while(1) {
		wait_result = WaitForSingleObject(toprocess_event, INFINITE);
		
		if (wait_result == WAIT_OBJECT_0) {
			wait_result = WaitForSingleObject(toprocess_mutex, INFINITE);
			if (wait_result == WAIT_OBJECT_0) {

				if (toprocess.empty())
					continue;

				sock = toprocess.front();
				toprocess.pop_front();

				if (toprocess.size() > 0)
					SetEvent(toprocess_event);

				ReleaseMutex(toprocess_mutex);

				if (sock == NULL)
					continue;

				Request(sock);
			}
		}
	}
	return 0;
}


Webserver::Webserver(unsigned int port_to_listen, size_t thread_limit) 
{
	toprocess_mutex = CreateMutex (NULL, FALSE, NULL);
	toprocess_event = CreateEvent(NULL,FALSE,FALSE,NULL);

	if (toprocess_event == NULL || toprocess_mutex == NULL)
		return;

	Preferences::appendToLog("Starting server on port "+StringUtils::fromInt(port_to_listen));

	SocketServer in(port_to_listen,10);

	Socket *ptr_s;

	req_handler = RequestHandler::instance();

	// create thread_limit threads
	if (thread_limit > 1) {
		for(size_t i=0;i<thread_limit;++i) {
			CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)&ThreadProcess, NULL,0,NULL);
		}
	}

	while (1) {
		ptr_s=in.Accept();

		
		if (thread_limit>1) {
			// multithreading

			// add new socket to toprocess queue and trigger event
			WaitForSingleObject( toprocess_mutex, INFINITE );
				toprocess.push_back(ptr_s);
			ReleaseMutex(toprocess_mutex);
			SetEvent(toprocess_event);
		}		
		else {
			// singlethreading
			Request(ptr_s);
		}
	}
}

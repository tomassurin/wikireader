#ifndef HTTP_REQUEST_H
#define HTTP_REQUEST_H

/**
 * @file
 * @brief This file contains implementation of class HttpRequest. 
 *
 * This file is part of module WikiReader.exe
 *
 * @author Ren� Nyffenegger (part of his simple webserver)
 *         <a href="http://www.adp-gmbh.ch/win/misc/webserver.html">src</a>
 * @author Tom� �ur�n (altered for use in WikiReader - Added support for chunked 
 *         encoding, intToHex decoding, response starting and removed some unnecessary fields)
 * 
 * @version 1.0
 */

#include <string>
#include <algorithm>
#include "socket.h"
#include "../../Shared/Request.h"
#include "../../Shared/wce/wce_time.h"

#pragma warning( disable: 4251 )

/** Class which represents HTTP request. */
class HttpRequest : public Request{
public:
	
	/** Constructor. */
	HttpRequest() {}

	/** Decode integer number to hex decoded string (used in chunked encoding for 
	 * declaring length of transfer part)
	 * @param n Input number.
	 * @return String containing hex value of number n.
	 */
	std::string decodeIntToHex(long n) {
		std::string s;

		while(n != 0) {
			s.push_back((n%16) < 10 ? '0'+(n%16) : 'A' + (n%16) - 10);
			n /= 16;
		}

		std::reverse(s.begin(),s.end());
		return s;
	}

	/** Writes part of response to client.
	 * @param ch String to write.
	 * @return True if everything went ok.
	 */
	virtual bool writeChunk(const std::string &ch) {
		if (!s_->SendLine(decodeIntToHex(ch.size())))
			return false;
		if (!s_->SendBytes(ch))
			return false;
		if (!s_->SendLine(""))
			return false;
		return true;
	}

	/** Writes part of response to client.
	 * @param buf Buffer of characters.
	 * @param length Length of input buffer.
	 * @return True if everything went ok.
	 */
	virtual bool writeChunk(char *buf, size_t length) {
		if (!s_->SendLine(decodeIntToHex(length)))
			return false;
		if (!s_->SendBytes(buf,length))
			return false;
		if (!s_->SendLine(""))
			return false;
		return true;
	}

	/** Writes final part of response to client.
	 * @return True if everything went ok.
	 */
	virtual bool writeFinalChunk() {
		if (!s_->SendLine("0"))
			return false;
		if (!s_->SendLine(""))
			return false;
		return true;
	}

	/** Writes start of response to client (status line and headers)
	 * @param status HTTP status code to be sent.
	 * @return True if everything went ok.
	 */
	virtual bool startResponse(int status)
	{
		time_t ltime;
		wceex_time(&ltime);
		tm* gmt= wceex_gmtime(&ltime);

		static std::string const serverName = "SimpleWiki HTTP";

		char* asctime_remove_nl = wceex_asctime(gmt);
		asctime_remove_nl[24] = 0;

		s_->SendBytes("HTTP/1.1 ");

		// status message
		switch (status) {
			case 200:
				s_->SendLine("200 OK");
				break;
			case 401:
				s_->SendLine("401 Unauthorized");
				break;
			case 404:
				s_->SendLine("404 Not Found");
				break;
			case 500:
			default:
				s_->SendLine("500 Internal Server Error");
		}

		// date and server name
		s_->SendLine(std::string("Date: ") + asctime_remove_nl + " GMT");
		s_->SendLine(std::string("Server: ") +serverName);

		// set transfer encoding to chunked
		s_->SendLine("Transfer-Encoding: chunked");

		// send additional headers
		for(size_t i=0;i<headers.size();++i) {
			s_->SendLine(headers[i]);
		}

		// send empty line -> end of headers
		s_->SendLine("");
		return true;
	}

	/** Add header to internal header vector.
	 * @param header HTTP header which we want to add to response.
	 */
	void addHeader(const std::string &header)
	{
		headers.push_back(header);
	}

	/** Get HTTP (or other) parameters.
	 * @param name Parameter name.
	 * @return Parameter value for parameter name.
	 */
	virtual const std::string &getParam(const std::string &name)
	{
		return params_[name];
	}

	/** Set parameters.
	 * @param name Parameter name.
	 * @param val Parameter value. 
	 */
	virtual void setParam(const std::string &name, const std::string &val) 
	{
		params_[name] = val;
	}

	/// Socket of connection to client.
	Socket* s_;
	/// HTTP method (GET or POST).
	std::string method_;
	/// Requested resource path (relative to server url).
	std::string path_;
	/// Map of parameters.
	std::map<std::string, std::string> params_;
	/// Vector of additional headers.
	std::vector<std::string> headers;
	/// Used to transmit server's error status, such as 202 OK, 404 Not Found and so on.
	std::string status_;
	/// Used to store answer.
	std::string answer_;
};

#endif
/**
 * @file
 * 
 * This file is part of module WikiReader.exe
 *
 * @author Ren� Nyffenegger (part of his simple webserver)
 * <a href="http://www.adp-gmbh.ch/win/misc/webserver.html">src</a>
 */

#include <string>

std::string base64_encode(unsigned char const* , unsigned int len);
std::string base64_decode(std::string const& s);

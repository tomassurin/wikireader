/*
   WebServer.h

   Copyright (C) 2003-2004 Ren� Nyffenegger

   This source code is provided 'as-is', without any express or implied
   warranty. In no event will the author be held liable for any damages
   arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this source code must not be misrepresented; you must not
      claim that you wrote the original source code. If you use this source code
      in a product, an acknowledgment in the product documentation would be
      appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
      misrepresented as being the original source code.

   3. This notice may not be removed or altered from any source distribution.

   Ren� Nyffenegger rene.nyffenegger@adp-gmbh.ch

*/

/**
 * @file
 * @brief This file contains declaration of class Webserver.
 *
 * This file is part of module WikiReader.exe
 *
 * @author Ren� Nyffenegger (part of his simple webserver)
 *         <a href="http://www.adp-gmbh.ch/win/misc/Webserver.html">src</a>
 * @author Tom� �ur�n (altered for use in WikiReader - Added really simple thread pool
 *         and removed unecessary stuff in Webserver::Request() method).
 * 
 * @version 1.0
 */
#ifndef WEBSERVER_H
#define WEBSERVER_H

#include <string>
#include <map>

#include "../../Shared/Types.h"

class Socket;
class RequestHandler;

/** Simple HTTP server. */
class Webserver 
{
public:
   
	/** Constructor.
	 * @param port_to_listen Port on which the server will listen.
	 * @param thread_limit Limit of threads that are created for serving requests.
	 */
    Webserver(unsigned int port_to_listen, size_t thread_limit = 1);
 
private:

	/** Handles request from input socket.
	 * @param s Socket of connection to client. 
	 */
    static unsigned __stdcall Request(Socket *s);

	/** Threads entry function. */
	static unsigned __stdcall ThreadProcess(void*);

	/// Pointer to RequestHandler instance.
	static RequestHandler *req_handler;
};

#endif
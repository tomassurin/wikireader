/* 
   Socket.h

   Copyright (C) 2002-2004 Ren� Nyffenegger

   This source code is provided 'as-is', without any express or implied
   warranty. In no event will the author be held liable for any damages
   arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this source code must not be misrepresented; you must not
      claim that you wrote the original source code. If you use this source code
      in a product, an acknowledgment in the product documentation would be
      appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
      misrepresented as being the original source code.

   3. This notice may not be removed or altered from any source distribution.

   Ren� Nyffenegger rene.nyffenegger@adp-gmbh.ch
*/

/**
 * @file
 * @brief This file contains declaration of classes Socket, SocketClient, SocketSelect and SocketServer. 
 *
 * This file is part of module WikiReader.exe
 *
 * @author Ren� Nyffenegger (part of his simple webserver)
 *         <a href="http://www.adp-gmbh.ch/win/misc/webserver.html">src</a>
 * @author Tom� �ur�n (some minor modifications for use in WikiReader).
 * 
 */


#ifndef __SOCKET_H__
#define __SOCKET_H__


#include <WinSock.h>
#include <string>

enum TypeSocket {BlockingSocket, NonBlockingSocket};

/** Socket wrapper class */
class Socket {
public:

  virtual ~Socket();
  Socket(const Socket&);
  Socket& operator=(Socket&);

  void ReceiveLine(std::string &);
  void ReceiveBytes(std::string&);

  void   Close();

  // The parameter of SendLine is not a const reference
  // because SendLine modifes the std::string passed.
  bool   SendLine (std::string);

  // The parameter of SendBytes is a const reference
  // because SendBytes does not modify the std::string passed 
  // (in contrast to SendLine).
  bool   SendBytes(const std::string&);

  bool SendBytes(char *buf, size_t length);

protected:
  friend class SocketServer;
  friend class SocketSelect;

  Socket(SOCKET s);
  Socket();

  SOCKET s_;

  int* refCounter_;

private:
  static void Start();
  static void End();
  static int  nofSockets_;
};

class SocketClient : public Socket {
public:
  SocketClient(const std::string& host, int port);
};


class SocketServer : public Socket {
public:
  SocketServer(int port, int connections, TypeSocket type=BlockingSocket);

  Socket* Accept();
};

class SocketSelect {
  public:
    SocketSelect(Socket const * const s1, Socket const * const s2=NULL, TypeSocket type=BlockingSocket);

    bool Readable(Socket const * const s);

  private:
    fd_set fds_;
};

#endif

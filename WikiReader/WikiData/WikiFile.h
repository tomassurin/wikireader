#ifndef WIKIREADER_WIKIFILE_H
#define WIKIREADER_WIKIFILE_H

/**
 * @file
 * @brief This file contains implementation of WikiFile service.
 *
 * This file is part of module WikiData.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#pragma comment(lib, "../Shared/shared.lib")
#include"../Shared/Types.h"
#include "../Shared/Plugin.h"
#include "../Shared/Preferences.h"
#include "../Shared/StringUtils.h"
#include "../Shared/MiscUtils.h"
#include "WikiHelper.h"

#include <fstream>
#include <windows.h>

using namespace std;

/** This structure contains information about section in table of contents */
struct TocData 
{
	/// TOC Section title.
	string title;
	/// Offset of section in article data.
	size_t offset;
	/// Section level (level of section heading).
	byte level;
};

/** Implementation of wiki file format access service. */
class WikiFile : public ServiceFileFormat {
private:

	/// Indicates if the file is opened.
	bool opened;
	/// Server mount directory of this file (from this directory data of this file are available).
	string mountdir;
	/// URL to main page of original wiki project (e.g. "http://en.wikipedia.org/Main_page").
	string basename;
	/// Name of wiki project (e.g. "Wikipedia").
	string sitename;
	/// Relative url to stylesheet file.
	string cssstyle;
	/// Contains  HTML code of navigation bar (at top of the page).
	string topbar;
	/// Number of articles in project.
	size_t articlesNum;
	/// List of decompression services - one for every volume. 
	/// Service at index i serves for decompression of i-th volume.
	vector<ServiceDecompress*> zip;
	/// Article names index service.
	ServiceIndex *titleIndex;
	/// Conversion service.
	ServiceContent *content;
	/// Setting if first letter is case sensitive
	bool firstLetterCase;
	/// Preferences object. This is used to access configuration file.
	Preferences pref;
	
	/// Mutex asociated with last opened article fields (lastBuf, lastToc, lastId, lastHeaders, lastBodyStart).
	HANDLE lastData_mutex;
	/// Contains data of last opened article.
	ByteBuffer lastBuf;
	/// Contains information about sections in last opened article's TOC.
	vector<TocData> lastToc;
	/// Contains id of last opened article.
	string lastId;
	/// Contains headers of last opened article.
	map<string,string> lastHeaders;
	/// Contains offset of last opened article body.
	size_t lastBodyStart;

public:

	/** Constructor. */
	WikiFile() : opened(false) 
	{
		lastData_mutex = CreateMutex(NULL,FALSE,NULL);
		titleIndex = NULL;
		content	= NULL;
		clear();
	};

	/** Destructor. */
	~WikiFile() 
	{
		clear();
		CloseHandle(lastData_mutex);
	}

	/** Get pointer to Preferences object which contains configuration of data file. 
	 * @return NULL if Preferences object can't be returned or this method is not implemented.
 	 */
	Preferences* getPreferences() 
	{
		return &pref;
	}

	/** Close opened file. */
	void close() 
	{
		clear();
	}

	/** Open data file.
	 * @param filename Full path to data file.
	 * @return True if everything went ok.
	 */
	bool open(const std::wstring &filename) 
	{
		opened = false;

		// inicialize preferences object
		if (!pref.loadPreferences(filename))
			return false;

		// parse current working directory
		wstring cwd = filename.substr(0,filename.find_last_of(L"\\"))+L"\\";

		PluginManager *manager = PluginManager::instance();

		// load decompression plugins
		string plugin = pref.getString("/data/plugin");
		wstring src = pref.getStringW("/data/src");
		int levels = (int) pref.getInteger("/data/volumes");

		if (plugin == pref.error || src == pref.werror || levels > 255)
			return false;

		ServiceDecompress *tzip;
		wstring name;

		for(int i=0;i<levels;++i) {
			tzip = dynamic_cast<ServiceDecompress*> (manager->getService(plugin));
			if (tzip == NULL)
				return false;

			if (i==0)
				name = StringUtils::getFullPath(src,cwd);
			else
				name = StringUtils::getFullPath(src,cwd) + StringUtils::fromIntW(i);

			if (!tzip->open(name)) 
				return clear();
			zip.push_back(tzip);
		}

		// load title index plugin
		plugin = pref.getString("/index/title/plugin");
		src = pref.getStringW("/index/title/src");
		if (plugin == pref.error || src == pref.werror)
			return clear();

		titleIndex = dynamic_cast<ServiceIndex*>(manager->getService(plugin));
		if (titleIndex == NULL) 
			return clear();

		if (!titleIndex->open(StringUtils::getFullPath(src,cwd)))
			return clear();

		// load content plugin
		plugin = pref.getString("/content");
		if (plugin == pref.error)
			return clear();
		content = dynamic_cast<ServiceContent*> (manager->getService(plugin));
		if (content == NULL)
			return clear();

		content->addService(this,"fileformat");

		// load other preferences
		firstLetterCase = pref.getString("/case") != "first-letter";

		basename = pref.getString("/basename");
		if (basename == pref.error)
			basename.clear();

		sitename = pref.getString("/sitename");
		if (sitename == pref.error)
			sitename.clear();

		articlesNum = pref.getInteger("/articles");

		cssstyle = pref.getString("/style");
		if (cssstyle == pref.error) {
			cssstyle.clear();
		}

		opened = true;
		return true;
	}

	/** Set some service preferences (mountdir and style).
 	 * @param name Name of preference.
	 * @param data Value of preference.
	 */
	void setPreference(const std::string &name, const std::string &data)
	{
		if (name == "mountdir") {
			mountdir = data;
		}
		else if (name == "style") {
			if (cssstyle.empty()) {
				cssstyle = data;
			}
		}
	}

	/** Retrieve resource data.
 	 * @param id Resource id.
	 * @param outdata ByteBuffer which will be filled with wanted data.
	 * @return True if everything went ok.
	 */
	bool getData(const std::string &id, ByteBuffer &buf) 
	{
		if (!opened)
			return false;
	
		// find titleIndex
		Request req(id);
		Address adr = titleIndex->exactMatch(req);
		if (!adr.isValid())
			return false;
		
		// extract data
		if (adr.volume < zip.size()) {
			if (!zip[adr.volume]->extract(adr.block,adr.start,adr.end,buf)) {
				buf.releaseBuffer();
				return false;
			}
		}
		if (buf.data == NULL) {
			// cleanup (if error occured)
			buf.releaseBuffer();
			return false;
		}

		return true;
	}

	/** Get some information about data file (this information is show on Home page).
	 * @return String with information about data file.
	 */
	virtual std::string getInfo() 
	{
		string outdata;
		outdata = "<h1>"+sitename+"</h1>";
		outdata +="<form id=\"search\" method=\"get\" action=\""+mountdir+"Special:Search\">\n";
		outdata +="<input id=\"searchText\" size=\"15\" autofocus=\"autofocus\" name=\"search\" />\n";
		outdata +="<input type=\"hidden\" name=\"direction\" value=\"front\" />\n";
		outdata +="<input type=\"submit\" value=\"Search\" />\n";
		outdata +="</form>\n";
		outdata +="<br/>Articles: "+ StringUtils::fromLong(articlesNum);
		outdata +="<br/><a href=\""+basename+"\">Original wiki</a>";
		return outdata;
	}

	/** Process request.
	 * @param req Request object (id must be set to requested resource id; 
	 *            also optional parameters can be set through this object).
	 * @return True if everything went ok.
	 */
	bool process(Request &req) 
	{
		if (!opened)
			return false;

		string outdata;
		outdata.reserve(1024);

		// create HTML head
		outdata = "<html>\n";
		outdata += "<head>\n";
		outdata += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n";
		if (!cssstyle.empty())
			outdata += "<link rel=\"stylesheet\" href=\""+cssstyle+"\" type=\"text/css\"/>\n";
		outdata += "<title>";
		outdata += req.getId();
		outdata += "</title>\n</head>\n<body>\n";

		if (req.getId().empty()) {
			// id is empty -> write info
			clearLast();
			req.addHeader("Content-Type: text/html");
			req.startResponse(200);
			outdata += getInfo();
			req.writeChunk(outdata);
			req.writeChunk("</body>\n</html>");
			return true;
		}

		if (req.getId() == "Special:Search") {
			// search
			clearLast();
			req.addHeader("Content-Type: text/html");
			req.startResponse(200);
			req.writeChunk(outdata);
			req.writeChunk(getTopBar());
			doSpecialSearch(req);
			return true;
		}

		// process resource from data file
		return processResource(outdata, req);
	}

private:
	/** Clear object state -> close opened services and files.
	 * @return False.
	 */
	bool clear() 
	{
		opened = false;

		clearLast();

		for(size_t i=0;i<zip.size();++i) {
			if (zip[i] != NULL) {
				zip[i]->release();
			}
		}
		zip.clear();

		if (titleIndex != NULL) {
			titleIndex->release();
			titleIndex = NULL;
		}
		if (content != NULL) {
			content->release();
			content = NULL;
		}
		return false;
	};

	/** @return HTML code of navigation bar. */
	std::string &getTopBar() 
	{
		if (topbar.empty()) {
			topbar = "<table><tr><td><a href=\"/\">Home</a></td>";
			topbar +="<td>";
			topbar +="<form id=\"search\" method=\"get\" action=\""+mountdir+"Special:Search\">\n";
			topbar +="<input id=\"searchText\" size=\"15\" autofocus=\"autofocus\" name=\"search\" />\n";
			topbar +="<input type=\"hidden\" name=\"direction\" value=\"front\" />\n";
			topbar +="<input type=\"submit\" value=\"Search\" />\n";
			topbar +="</form>\n";
			topbar +="</td></tr></table>\n";
		}
		return topbar;
	}
	/** This method is used to clear last opened article data. */
	void clearLast() 
	{
		WaitForSingleObject(lastData_mutex,INFINITE);
			lastBuf.releaseBuffer();
			lastToc.clear();
			lastHeaders.clear();
			lastBodyStart = 0;
			lastId.clear();
		ReleaseMutex(lastData_mutex);
	}

	/** Process resource from data file (e.g. wiki article).
	 * @param outdata Output buffer (temporary buffer used to store partial results).
	 * @param req Request object.
	 * @return True if everything went ok.
	 */
	bool processResource(std::string &outdata, Request &req)	 
	{
		ByteBuffer processbuf;
		ByteBuffer buf;
		bool proBufAlloc = false;

		// canonize name of requested resource
		string id = canonizeName(req.getId(), firstLetterCase);
		// html encoded name of resource (article)
		string htmlid = StringUtils::encodeUrl(id);

		WaitForSingleObject(lastData_mutex,INFINITE);

		if (id == lastId) {
			// last article is same as this article
			processbuf = lastBuf;
			processbuf.startOffset = lastBodyStart;
		}
		else {
			// last article != this article -> clear last article and load new article data
			lastId = id;
			lastBuf.releaseBuffer();
			lastHeaders.clear();

			// get resource data
			if (!getData(id,buf)) {
				// data couldn't be retrieved
				req.addHeader("Content-Type: text/html");
				req.startResponse(404);
				req.writeChunk(outdata);
				req.writeChunk(getTopBar());
				req.writeChunk("<h1>404 Not Found</h1></body></html>");
				ReleaseMutex(lastData_mutex);
				return true;
			}

			// parse resource headers
		    lastBodyStart = parseHeaders(buf,lastHeaders);
						
			lastBuf = buf;
			// parse sections of article and save them to lastTOC structure
			separateHeaders(buf, 2, 2);

			processbuf = buf;
			processbuf.startOffset = lastBodyStart;
		}

		// calculate offset of start and end of data that we will be converting
		if (!lastToc.empty()) {
			size_t start = StringUtils::toLong(req.getParam("start"));	  // start offset GET param
			size_t end = StringUtils::toLong(req.getParam("end"));		  // end offset GET param 
			if (start == 0 && end == 0) {
				// first part of article (intro)
				processbuf.length = lastToc[0].offset;
			}
			else {
				// other parts of article
				processbuf.length = end;
				processbuf.startOffset = start;
			}
		}

		// start response
		req.addHeader("Content-Type: text/html");
		req.startResponse(200);
		req.writeChunk(outdata);
		outdata.clear();
		req.writeChunk("<a name=\"top\"></a>\n");		
		req.writeChunk(getTopBar());
		
		// write title
		outdata.append("<h1>");
		outdata.append(lastHeaders["Article-Title"]);
		outdata.append("</h1>\n");
		req.writeChunk(outdata);
		outdata.clear();

		// process Redirects
		if (lastHeaders.find("Redirect") != lastHeaders.end()) {
			outdata.append("<span class=\"redirect\">Redirect to <a href=\""+
					StringUtils::encodeUrl(lastHeaders["Redirect"])+"\">"+lastHeaders["Redirect"]+"</a></span>");
			req.writeChunk(outdata);
			outdata.clear();
		}

		// convert resource
		if (!content->convert(processbuf,req)) {
			req.writeChunk("<h1>Error converting resource</h1>");
		}

		// write TOC
		if (!lastToc.empty()) {
			outdata += "<table id=\"toc\" class=\"toc\">\n<tr>\n<td>\n";
			outdata += "<div class=\"toctitle\"><h2>Contents:</h2></div>\n<ul>\n";
			outdata += "<li><a href=\""+htmlid+"\">0 Home</a></li>\n";

			size_t start;
			size_t end;
			for(size_t i=0;i<lastToc.size();++i) {
				start = lastToc[i].offset;
				if (i+1 < lastToc.size()) {
					end = lastToc[i+1].offset;
				}
				else {
					end = lastBuf.length;
				}
				outdata += "<li><a href=\""+htmlid+"?start="+StringUtils::fromLong(start)+"&end=";
				outdata += StringUtils::fromInt(end)+"\">"+StringUtils::fromInt(i+1)+" "+lastToc[i].title+"</a></li>\n";
			}
			outdata +="</ol>\n</td>\n</tr>\n</table>";
			req.writeChunk(outdata);
			outdata.clear();
		}

		// finalize article
		req.writeChunk("<a href=\"#top\">Top</a></body>\n</html>");

		ReleaseMutex(lastData_mutex);

		return true;
	}

	/** Parse sections of article and save them to lastToc structure.
	 * @param buf Input buffer with article data.
	 * @param minlevel Minimal level of section headings that are added to toc.
	 * @param maxlevel Maximal level of section headings that are added to toc.
	 */
	void separateHeaders(ByteBuffer &buf, byte minlevel, byte maxlevel)
	{
		PreprocessWorkData work;
		work.s = buf;
		work.i = 0;

		lastToc.clear();

		char c;
		bool newline = false;

		byte level = 0;
		size_t tablelevel = 0;
		byte level2 = 0;
		TocData t;

		// create string reference
		stringRef sref;
		sref.setString((char*)buf.data, buf.length);;

		while(work.isOk()) {
			c = work.get();

			// start of table (we don't want to split table data even if it contains headings)
			if (c == '{' && work.peek() == '|') {
				newline = false;
				++work;
				++tablelevel;
			}
			// end of table
			else if (tablelevel > 0 && c == '|' && work.peek() == '}') {
				newline = false;
				++work;
				--tablelevel;
			}	  
			// start of new line
			else if (newline) {
				newline = false;
				// process headings
				if (tablelevel == 0 && c == '=') {
					t.offset = work.tellg()-1;
					t.title.clear();
					level = 0;
					while (work.isOk() && c == '=') {
						++level;
						c = work.get();
					}
					
					if (level < minlevel || level > maxlevel) {
						work.unget();
						continue;
					}

					sref.setStart(work.tellg()-1);

					while (work.isOk() && c != '\n' && c != '=') {
						c = work.get();
					}

					if (c == '=') {
						sref.setEnd(work.tellg()-1);
						level2 = 0;
						while (work.isOk() && c =='=') {
							++level2;
							c = work.get();
						}
						work.unget();
						if (level == level2 && level < 7) {
							stringRef sref2 = sref;
							sref2.trim();
							sref2.appendTo(t.title);
							t.level = level;
							lastToc.push_back(t);
						}
					}
					else {
						work.unget();
					}
				}
				else if (c == '\n')
					newline = true;
			}
			else if (c == '\n') {
				newline = true;
			}
		}		
	}

	/** Header parsing. 
	 * @param buf Input buffer with article data.
	 * @param headers Map of <header_id,header_value>.
	 * @return Start of body offset (right after header).
	 */
	size_t parseHeaders(ByteBuffer &buf, map<string,string> &headers)
	{
		if (buf.data == NULL)
			return -1;

		string out;
		size_t i;
		size_t j;

		// parse article title
		for(i=0;i<buf.length;++i) {
			if (buf.data[i] == '\n') {
				out.assign((char*)buf.data,i-0);
				headers["Article-Title"] = out;
				break;
			}
		}

		// parse headers
		while (i < buf.length) {
			++i;
			if (buf.data[i] == '\n') 
				break;

			// parse line
			for(;i<buf.length;++i) {
				if (buf.data[i] == '\n') {
					out.assign((char*)buf.data,i-0);
					j = out.find_first_of(':');
					if (j == out.npos)
						break;
					if (j == out.size()-1)
						headers[StringUtils::trim(out.substr(0,j))] = "";
					else
						headers[StringUtils::trim(out.substr(0,j))] = StringUtils::trim(out.substr(j+1));
					break;
				}
			}
		}
		++i;
		// parse redirect
		if (i+8 < buf.length && buf.data[i] == '#' && tolower(buf.data[i+1]) == 'r' && tolower(buf.data[i+2]) =='e' && 
			 tolower(buf.data[i+3]) == 'd' && tolower(buf.data[i+4]) == 'i' && tolower(buf.data[i+5]) == 'r' &&
			 tolower(buf.data[i+6]) == 'e' && tolower(buf.data[i+7]) == 'c' && tolower(buf.data[i+8]) == 't') {

    		for(j=i+9;j<buf.length;++j) {
				if (buf.data[j] == '[')				
					break;
			}

			if (j+1 < buf.length && buf.data[j] == '[' && buf.data[j+1] == '[')	{
				j +=2;
				size_t start = j;
				for(;j<buf.length;++j) {
					if (buf.data[j] == ']')
						break;
				}
				if (j+1 < buf.length && buf.data[j] == ']' && buf.data[j+1] == ']') {
					out.assign((char*)buf.data+start,j-start);
					headers["Redirect"] = out;
					i = j+2;
				}
			}
		}
		return i;
	}

	/** Do search in title index.
	 * @param req Request for search. 
	 */
	void doSpecialSearch(Request &req)
	{
		vector<Address> result;
		string outdata;
		int n = 50;

		// direction
		bool back = req.getParam("direction")=="back";

		// search name
		req.setId(canonizeName(req.getParam("search")));
		// number of results
		req.setParam("matches",StringUtils::fromInt(n));

		outdata.reserve(8192);
		result.reserve(n);

		// get partial match results from index
		titleIndex->partialMatch(req,result);

		// output results
		if (result.size() == 0) {
			outdata+="<h1>Nothing found!</h1>";
		}
		else {
			if (back) {
				reverse(result.begin(),result.end());
			}
			outdata +="<form id=\"back\" method=\"get\" action=\""+mountdir+"Special:Search\">\n";
			outdata +="<input type=\"hidden\" name=\"search\" value=\""+StringUtils::encodeUrl(result[0].id)+"\" />\n";
			outdata +="<input type=\"hidden\" name=\"direction\" value=\"back\" />\n";
			outdata +="<input type=\"submit\" value=\"<< Previous\" />\n";
			outdata +="</form>\n";
			
			for(size_t i=0;i<result.size();++i) {
				if (result[i].isValid()) {
					outdata +="<a href=\""+StringUtils::encodeUrl(result[i].id)+"\">"+result[i].id+"</a><br/>\n";
				}
			}
			if (n == result.size()) {
				outdata +="<form id=\"search\" method=\"get\" action=\""+mountdir+"Special:Search\">\n";
				outdata +="<input type=\"hidden\" name=\"search\" value=\""+StringUtils::encodeUrl(result[result.size()-1].id)+"\" />\n";
				outdata +="<input type=\"hidden\" name=\"direction\" value=\"front\" />\n";
				outdata +="<input type=\"submit\" value=\"Next >>\" />\n";
				outdata +="</form>\n";
			}
		}
		outdata += "\n</body>\n</html>";
		req.writeChunk(outdata);
	}
};


#endif
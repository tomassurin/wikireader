/**
 * @file
 * @brief This file contains definitions needed for this module to be used as a plugin.
 *
 * This file is part of module WikiData.dll
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include "WZipDec/WZipDec.h"
#include "WikiTitleIndex.h"
#include "WikiFile.h"
#include "WikiContent.h"

#include <string>

#pragma comment(lib, "../Shared/shared.lib")
#include "../Shared/StringUtils.h"
#include "../Shared/Types.h"

/// ID of this plugin.
static const char *id = "WikiData:Basic implementation of wiki file format";
/// Services of this plugin separated by ";".
static const char *services = "Wzip;WikiFile;WikiTitleIndex;WikiContent";

/** Factory function that creates instances of services in this plugin.
 * @param service Identifier of service (case-insensitive).
 * @return Pointer to created service object. This object must be deleted by calling its release() method (not delete).
 */
DLLEXPORT ServiceBase* getInstance(const char *service)
{
	string svc(service);
	svc = StringUtils::toLower(svc);
	ServiceBase *out = NULL;
	if (svc == "wzip")
		 out = new WZip();
	else if (svc == "wikititleindex")
		out = new WikiTitleIndex();
	else if (svc == "wikifile")
		out = new WikiFile();
	else if (svc == "wikicontent") 
		out = new WikiContent();
	
	return out;
}

/** Get available services.
 * @return Constant pointer to services string.
 */
DLLEXPORT const char* getServices() 
{
	return services;
}

/** Get ID.
 * @return Constant pointer to id string.
 */
DLLEXPORT const char* getId() 
{
	return id;
}
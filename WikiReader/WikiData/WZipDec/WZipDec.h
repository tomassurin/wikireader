#ifndef WZIPDEC_H
#define WZIPDEC_H

/**
 * @file
 * @brief This file contains implementation of WZip service.
 *
 * This file is part of module WikiData.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <typeinfo>

#pragma comment(lib, "../Shared/shared.lib")
#include "../Shared/StringUtils.h"
#include "../Shared/Types.h"

#include "Alloc.h"
#include "7zFile.h"
#include "LzmaDec.h"

/// Compression format magic number.
const UInt32 magic = 0x88022022;
/// Compression format major version.
const UInt16 majorVer = 1;
/// Compression format minor version.
const UInt16 minorVer = 0;

// Sizedefs
static const size_t t8 = sizeof(UInt64);
static const size_t tt = sizeof(size_t);
static const size_t t4 = sizeof(UInt32);
static const size_t t2 = sizeof(UInt16);

// Alocators
static void *SzAlloc(void *p, size_t size) { p = p; return MyAlloc(size); }
static void SzFree(void *p, void *address) { p = p; MyFree(address); }
static ISzAlloc g_Alloc = { SzAlloc, SzFree };

#define IN_BUF_SIZE (1 << 16)
#define OUT_BUF_SIZE (1 << 16)

/** Implementation of decompression service for format WZip. It's very simple
 * format that use LZMA compression. It was designed specially for WikiReader.*/
class WZip : public ServiceDecompress 
{
private:
	/// Stream representing archive file
	CFileSeqInStream archStream;
	/// Archive file
	CSzFile arch;
	/// Offset of blocks directory
	Int64 indexOffset;
	/// Number of blocks in file
	UInt32 offsetsCount;
	/// Indicates if file is opened. 
	bool opened;

public:

	/** Constructor. */
	WZip() {
		opened = false;
		indexOffset = 0;
		offsetsCount = 0;
	}

	/** Destructor. */
	~WZip() {
		close();
	}

	/** Release of memory denoted by ptr. */
	void releaseResource(void *ptr) 
	{
		if (ptr == NULL)
			return;
		free(ptr);
	}
	
	/** Opens compressed file.
	 * @param archFile Full path to compressed file.
	 * @return True if everything went ok.
	 */
	virtual bool open(const std::wstring &archFile) 
	{
		WRes res;
		size_t sz;
		UInt32 pos;

		// create virtual tables for input stream
		FileSeqInStream_CreateVTable(&archStream);
		File_Construct(&archStream.file);

		// open archive file
		if (InFile_OpenW(&archStream.file, archFile.c_str()) != 0)
			return false;
		arch = archStream.file;	

		// get format version
		pos = 0;
		sz = t4;
		File_Read(&arch, &pos, &sz);
		if (pos != magic)  {
			close();
			return false;
		}
		pos = 0;
		sz = t2;
		File_Read(&arch, &pos, &sz);
		if (pos != majorVer) {
			close();
			return false;
		}
		pos = 0;
		sz = t2;
		File_Read(&arch, &pos, &sz);
		if (pos != minorVer) {
			close();
			return false;
		}

		// read index offset
		indexOffset = 0;
		sz = t4;
		res = File_Read(&arch,&indexOffset,&sz);
		if (res != 0 || sz != t4) 
			return false;

		// read index size
		res = File_Seek(&arch,&indexOffset,SZ_SEEK_SET);
		if (res != 0) 
			return false;
		sz = t4;
		res = File_Read(&arch,&offsetsCount,&sz);
		if (res != 0 || sz != t4) 
			return false;
		indexOffset += t4;
		opened = true;

		return true;
	}

	/** Close opened file. */
	virtual void close() 
	{
		opened = false;
		File_Close(&arch);
	}

	/** Extracts block part from compressed file. 
	 * @param block Logical number of block to decompress.
	 * @param start Start offset (in block) from which data is returned.
	 * @param end End offset (in block) to which data is returned
	 * @param outdata ByteBuffer which will be filled with wanted data.
	 * @return True if everything went ok.
	 */
	bool extract(size_t blockIndex, size_t start, size_t end, ByteBuffer &out) 
	{		
		if (!opened) {
			return false;
		}

		size_t i = 0;
		Int64 offset;
		size_t sz;
		SRes res;
		
		if (blockIndex != 0 && blockIndex <= offsetsCount) {
			// get offset of wanted block 
			offset = indexOffset+(blockIndex-1)*t4;
			File_Seek(&arch,&offset,SZ_SEEK_SET);
			offset = 0;
			sz = t4;
			File_Read(&arch,&offset,&sz);

			// seek to start of block
			File_Seek(&arch, &offset,SZ_SEEK_SET);

			// extract data to buffer
			res = Decode(&archStream.s,&out.data,&out.length, &start, &end);
			if (res != SZ_OK) {
				// cleanup if something failed
				if (out.data != NULL)
					releaseResource(out.data);
			    out.data = NULL;
				out.length = 0;
				return false;
			}
			out.allocBy = this;
			return true;
		}	
		return false;
	}

private:

	/** Internal decode utility. */
	static SRes Decode(ISeqInStream *inStream, Byte **data, size_t *bufsize, size_t *start, size_t *end)
	{
		UInt64 unpackSize;
		int i;
		SRes res = 0;

		CLzmaDec *state = (CLzmaDec*) malloc(sizeof(CLzmaDec));

		/* header: 5 bytes of LZMA properties and 8 bytes of uncompressed size */
		unsigned char header[LZMA_PROPS_SIZE + 8];

		/* Read and parse header */
		RINOK(SeqInStream_Read(inStream, header, sizeof(header)));

		unpackSize = 0;
		for (i = 0; i < 8; i++)
			unpackSize += (UInt64)header[LZMA_PROPS_SIZE + i] << (i * 8);

		if (*start > unpackSize) 
			return SZ_ERROR_PARAM;

		if (*end > unpackSize) 
			*end = (size_t)unpackSize;

		*bufsize = (*end)*sizeof(Byte);
		*data = (Byte*) malloc(*bufsize);
		if (*data == NULL)
			return SZ_ERROR_MEM;

		LzmaDec_Construct(state);
		RINOK(LzmaDec_Allocate(state, header, LZMA_PROPS_SIZE, &g_Alloc));
		res = Decode2(state, inStream, *end, *data, *bufsize);
		LzmaDec_Free(state, &g_Alloc);
		free(state);

		if (*start != 0) {
			*bufsize = (*end-*start)*sizeof(Byte);
			memmove(*data,*data+*start,*bufsize);
			*data = (Byte*)realloc(*data,*bufsize);
			if (*data == NULL)
				return SZ_ERROR_MEM;
		}

		return res;
	}

	/** Another internal decoding utility. */
	static SRes Decode2(CLzmaDec *state, ISeqInStream *inStream, 
		UInt64 unpackSize, Byte *buf, size_t bufsize)
	{
		// warning! vc compiler generated bad code if i used array inBuf (WTF?)
		int thereIsSize = (unpackSize != (UInt64)(Int64)-1);
		Byte *inBuf;
		Byte *outBuf = buf;
		size_t inPos = 0, inSize = 0, outPos = 0;

		inBuf = (Byte*)malloc(IN_BUF_SIZE);
		if (inBuf == NULL) {
			return SZ_ERROR_MEM;
		}

		LzmaDec_Init(state);
		for (;;)
		{
			if (inPos == inSize)
			{
				inSize = IN_BUF_SIZE;
				RINOK(inStream->Read(inStream, inBuf, &inSize));
				inPos = 0;
			}
			{
				SRes res;
				size_t inProcessed = inSize - inPos;
				size_t outProcessed = bufsize - outPos;
				ELzmaFinishMode finishMode = LZMA_FINISH_ANY;
				ELzmaStatus status;
				if (thereIsSize && outProcessed > unpackSize)
				{
					outProcessed = (SizeT)unpackSize;
					finishMode = LZMA_FINISH_END;
				}

				res = LzmaDec_DecodeToBuf(state, outBuf + outPos, &outProcessed,
					inBuf + inPos, &inProcessed, finishMode, &status);
				inPos += inProcessed;
				outPos += outProcessed;
				unpackSize -= outProcessed;

				if (res != SZ_OK || thereIsSize && unpackSize == 0) {
					free(inBuf);
					return res;
				}

				if (inProcessed == 0 && outProcessed == 0)
				{
					if (thereIsSize || status != LZMA_STATUS_FINISHED_WITH_MARK) {
						free(inBuf);
						return SZ_ERROR_DATA;
			  }
					free(inBuf);
					return res;
				}
			}
		}
	}		
};
#endif

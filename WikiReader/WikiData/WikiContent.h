#ifndef WIKICONTENT_H
#define WIKICONTENT_H

/**
 * @file
 * @brief This file contains implementation of WikiContent service.
 *
 * This file is part of module WikiData.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include <stdio.h>
#include <cctype>
#include <string>

#include "WikiHelper.h"

using namespace std;

/// Identifier of template namespace.
static const int templateId = 10;
/// Identifier of category namespace.
static const int categoryId = 14;
/// Helper identifier of unavailable links namespace.
#define UNAVAIL_LINKS_NS -1000

/// Capacity of sending buffer (used to store converted data before they are send to client).
#define OUTBUF_LEN 8192
/// Limit of sending buffer size after which its contents are sent to client.
#define OUTBUF_LIMIT 8000

/** Implementation of wiki to HTML conversion service. */
class WikiContent : public ServiceContent {
private:

	/// File format service that is calling this converter. 
	ServiceFileFormat *fileformat;
	/// Preferences object of data file. 
	Preferences *pref;
	/// URL to main page of original wiki project (e.g. "http://en.wikipedia.org/Main_page").
	string basename;
	/// Namespace definitions.
	vector<WikiNamespace> namespaces;
	/// Localized name of template namespace. 
	string templateNamespace;

public:

	/** Constructor. */
	WikiContent() 
	{
 		fileformat = NULL;
		pref = NULL;
		addStandardNamespaces();
	}

	/** Destructor. */
	~WikiContent() 
	{
		namespaces.clear();
	}

	/** Pass pointer of parent service to this service.
     * @param plug Pointer to service.
	 * @param name Some kind of service identification (implementation dependend).
	 */
	void addService(ServiceBase *plug, const std::string &name) 
	{
		if (plug == NULL)
			return;

		if (name == "fileformat") {
			fileformat = dynamic_cast<ServiceFileFormat*>(plug);
			if (fileformat != NULL) {
				setPreferences(fileformat->getPreferences());
			}
		}
	}

	/** Add namespace definition of unavailable links namespace to namespaces vector. */
	void addStandardNamespaces()
	{
		WikiNamespace ns;
		ns.id= UNAVAIL_LINKS_NS;
		ns.included = true;
		ns.name = "@";
		namespaces.push_back(ns);
	}

	/** Set service preference.
	 * @param name Name of preference.
	 * @param data Value of preference.
	 */
	void setPreferences(Preferences *preferences) 
	{
		vector<string> elements;
		int nsid;
		string name;
		int ind;
		WikiNamespace ns;

		this->pref = preferences;
		namespaces.clear();
		basename.clear();
		templateNamespace.clear();

		if (pref == NULL)
			return;

		addStandardNamespaces();

		// load wiki basename
		basename = pref->getString("basename");
		if (basename == pref->error) 
			basename.clear();
		else {		
			basename = basename.substr(0,basename.find_last_of("/"));
			basename += '/';
		}

		// load namespaces info
		pref->getSectionElements("namespaces",elements);
		for(size_t i=0; i<elements.size(); ++i) {
			if (elements[i] == "ns0") 
				continue;

			ns.name = pref->getString("namespaces/"+elements[i]);
			ns.id = StringUtils::toLong(elements[i].substr(2,10));
			namespaces.push_back(ns);
		}
		
		// load included namespaces info
		name = pref->getString("contains");
		elements.clear();
		StringUtils::splitString(name," ",elements);
		for(size_t i=0; i < elements.size();++i) {
			nsid = StringUtils::toLong(elements[i].substr(2,2));
			ind = getNamespace(nsid);
			if (ind == -1)
				continue;
			namespaces[ind].included = true;
		}

		// get template namespace localized name
		ind = getNamespace(templateId);
		if (ind == -1)
			return;
		templateNamespace = namespaces[ind].name;
	}
	
	/** Convert data from input and write data with calls to Request::writeChunk().
	 * @param input ByteBuffer with input data.
	 * @param req Request object which is used to write data to output.
	 * @return True if everything went ok.
	 */
	bool convert(const ByteBuffer &inputBuffer, Request &req) 
	{
		// reserve enought memory for sending buffer
		string outdata;
		outdata.reserve(OUTBUF_LEN);

		// create WorkData instance, reserve enough data for its internal buffer
		WorkData work;
		work.reserve(inputBuffer.length);
		work.setg(0);
		work.req = &req;
		// we are starting on newline
		work.setNewline();

		// create PreprocessWorkData instance with input data
		PreprocessWorkData prework;
		prework.s = inputBuffer;
		prework.i = inputBuffer.startOffset;
		
		while (true) {
			// preprocess some portion of input
			if (!preprocessStep(prework,work))
				break;

			// process preprocessed data
			if (!processStep(work,outdata))
				return false;
		}

		// process final preprocessed data
		processStep(work,outdata);

		// finalize conversion
		finalize(work,outdata);

		return true;
	}

private:

	/** @return Index of namespace with id "id" in "namespaces" vector 
	 * or -1 if such namespace does not exist. 
	 */
	int getNamespace(int id) 
	{
		for(size_t i=0;i<namespaces.size();++i) {
			if (namespaces[i].id == id )
				return i;
		}
		return -1;
	}

	/** @return Index of namespace with localized name "name" in "namespaces" vector
	 * or -1 if such namespace does not exist.
	 */
	int getNamespace(const std::string &name) 
	{
		for(size_t i=0;i<namespaces.size();++i) {
			if (namespaces[i].name == name)
				return i;
		}
		return -1;
	}

	/** Find if namespace is included by providing its id.
	 * @return True if namespace with name "name" is included. 
	 */
	bool isIncluded(const std::string &name) 
	{
		if (name.empty())
			return true;

		int i = getNamespace(name);

		if (i == -1)
			return false;

		return namespaces[i].included;
	}

	/** Find if namespace is included by providing its id.
	 * @return True if namespace with id "id" is included.
	 */
	bool isIncluded(int id) 
	{
		if (id == 0)
			return true;

		int i = getNamespace(id);

		if (i == -1)
			return false;

		return namespaces[i].included;
	}

	/** Separate article name from its namespace.
	 * @param inp String reference to full article name (with namespace).
	 * @param name Reference to only article name (without namespace) is returned in this param. 
	 * @param ns Article namespace id is returned in this param.
	 */
	void separatePageName(stringRef &inp, stringRef &name, int &ns )
	{
		stringRef nstemp;
		size_t i;

		name = inp;
		ns = 0;
		inp.trimPageName();

		// not containing namespace prefix
		if (!inp.contains(':')) 
			return;

		// separate namespace and article name from input
		nstemp.setString(inp.getBuffer(), inp.getBufferLength());
		nstemp.setStart(inp.getStart());
		for(i=0;i<inp.size();++i) {
			if (inp[i] == ':')
				break;
		}
		nstemp.setEnd(nstemp.getStart()+i);
		name.setStart(nstemp.getEnd()+1);
		nstemp.trimPageName();
		name.trimPageName();

		// no namespace prefix 
		if (nstemp.isEmpty())
			return;

		// find namespace in namespaces
		for(i=0;i<namespaces.size();++i) {
			if (nstemp == namespaces[i].name) {
				ns = namespaces[i].id;
				return;
			}
		}

		name.setStart(nstemp.getStart());
	}

	/** This method will be used in future versions to process templates. */
	void processTemplate(const ByteBuffer &input, size_t i, WorkData &out)
	{

	}

	/** Decode HTML entity at current position in preprocessing data.
	 * @return Decoded HTML entity or 0 if not such entity exists in input.
	 */
	char decodeHtmlEntity(PreprocessWorkData &s) 
	{
		if (s.match("lt;") ) {
			s.seekg(3);
			return '<';
		}
		else if (s.match("gt;")) {
			s.seekg(3);
			return '>';
		}
		else if (s.match("quot;")) {
			s.seekg(5);
			return '\"';
		}
		else if (s.match("amp;")) {
			s.seekg(4);
			return '&';
		}
		return 0;
	}

	/** Encode character "c" to HTML entity and append this entity to "outdata". */
	void encodeHtmlEntity(char c, string &outdata)
	{
		switch (c) {
			case '<':
				outdata.append("&lt;");
				break;
			case '>':
				outdata.append("&gt;");
				break;
			case '\"':
				outdata.append("&quot;");
				break;
			case '&':
				outdata.append("&amp;");
				break;
			default:
				outdata.push_back(c);
				break;
		}
	}

	/** Processing step.
	 * @param work WorkData object, with preprocessed data to process.
	 * @param outdata Sending buffer (used to store converted data after some 
	 *				  size limit its contents are send to client).
	 * @return True if everything went ok. 
	 */
	bool processStep(WorkData &work, string &outdata)
	{
		char c;

		while (work.isOk()) {
			// write output buffer to socket
			if (outdata.size() > OUTBUF_LIMIT) {
				if (!work.writeChunk(outdata))
					return false;
				outdata.clear();
			}

			// peek next character
			c = work.peek();

			// process tags
			if (c=='<') {
				doTags(work,outdata);
				continue;
			}
			// skip text
			else if (work.skip) {
				++work;
				continue;
			}
			// nowiki markup
			else if (work.nowiki || work.preformated) {
				work.newline = false;
				encodeHtmlEntity(c,outdata);
				++work;
				continue;
			}
			// begining of line
			else if (work.newline) {
				work.newline = false;

				// hack - skip spaces at the start of line if in table
				if (c == ' ' && work.isInTable()) {
					++work;
					while (work.isOk()) {
						c = work.peek();
						if (c == ' ') {
							++work;
							continue;
						}
						break;
					}
				}
				// process pre block (whitespaces at start of the line)
				if (c == ' ') {
					++work;
					int cpos = work.tellg();
					while(work.isOk()) {
						c = work.peek();
						if (c == '\n') {
							break;;
						}
						if (c != ' ' && c != '\t')
							break;
						++work;
					}
					if (c == '\n' && work.next() != ' ') 
						continue;
						
					work.setg(cpos);
					if (!work.preblock) {
						endParagraph(work,outdata);
						work.preblock = true;
						outdata.append("<pre>");
					}
					outdata.push_back('\n');
				}
				// end pre block
				else if (work.preblock) {
					work.preblock = false;
					outdata.append("</pre>");
					work.setNewline();
				}
				// process lists
				else if (c=='*' || c=='#' || c==':' || c==';') {
					endParagraph(work,outdata);
					doLists(work,outdata);
				}
				// process table
				else if (work.isInTable() && (c=='|' || c=='!')) {
					doTable(work,outdata);
				}
				// end list
				else if (work.isInList()) {
					endList(work,outdata);
					work.setNewline();
				}
				// process headings
				else if (c=='=') {
					doHeadings(work,outdata);
				}
				// process horizontal line
				else if (c=='-') {
					doHorizontalLine(work,outdata);
				}
				// process anchors on separate lines (ie. line == anchor)
				else if (c == '[') {
					startParagraph(work,outdata);
					work.seekg(1);
					work.anchorline = true;
					doAnchors(work,outdata);
					work.anchorline = false;
				}
				// process blank line -> new paragraph
				else if (c == '\n') {
					if (work.inparagraph) {
						endParagraph(work,outdata);
					}
					else {
						work.inparagraph = true;
						outdata.append("<p><br/>");
					}
					// end table
					//if (work.isInTable()) {
					//	endTable(work,outdata);
					//}
					++work;
					work.setNewline();
				}
				continue;
			}
			// end of line
			else if (c == '\n') {
				// end opened elements that can be only at one line (error handling for better output)
				if (work.italics) {
					work.italics = false;
					outdata.append("</i>");
				}
				if (work.bold) {
					work.bold = false;
					outdata.append("</b>");
				}

				// if we are at paragraph append space 
				if (work.inparagraph)
					outdata.push_back(' ');

				// if we are in definition list stop seeking for definitions of term
				if (work.deflist == DEF_TERM)
					work.deflist = DEF_NO;

				++work;
				work.setNewline();
				continue;
			}
			// start new table
			else if (c=='{' && work.next() == '|') {
				work.seekg(2);
				endParagraph(work,outdata);
				startTable(work,outdata);
				continue;
			}
			// process anchors
			else if (c == '[') {
				startParagraph(work,outdata);
				work.seekg(1);
				doAnchors(work,outdata);
				continue;
			}
			// process apostrophes section (bold & italics formating)
			else if (c == '\'') {
				startParagraph(work,outdata);
				doApostrophes(work,outdata);
				continue;
			}
			// process definition to term on single line
			else if (work.deflist == DEF_TERM && c==':') {
				outdata.append("</dt><dd>");
				work.deflist = DEF_DESCR;
				++work;
				continue;
			}
			// process inline table cells
			else if (work.isInTable()) {
				if (c=='|' && work.next() == '|') {
					work.seekg(2);
					if (work.linestart=='!') 
						doTableCell(work,outdata,TH);
					else
						doTableCell(work,outdata,TD);
					continue;
				}
				else if (c=='!' && work.next() == '!') {
					work.seekg(2);
					doTableCell(work,outdata,TH);
					continue;
				}				
			}
			
			// append other characters to send buffer
			startParagraph(work,outdata);
			outdata.push_back(c);
			++work;
		}
		return true;
	}

	/** Finalize conversion. */
	void finalize(WorkData &work, std::string &outdata) 
	{
		// end opened tables and paragraphs
		endTable(work,outdata);
		endParagraph(work,outdata);

		// write data remaining in send buffer
		if (!outdata.empty()) 
			work.writeChunk(outdata);
	}

	
	/** Preprocessing step. This step ends if limit of processed paragraphs is exceeded.
	 * @param s Preprocessing data.
	 * @param out WorkData instance. It's used as output for preprocessed data.
	 * @return True if processing can continue, false if we are at the end of input. 
	 */
	bool preprocessStep(PreprocessWorkData &s, WorkData &out) 
	{
		out.clearBuffer();

		if (!s.isOk())
			return false;

		char tc;
		stringRef sref;
		int paragraph = 0;

		sref.setString((const char*)s.s.data, s.s.length);

		while (s.isOk()) {
			// skip comments
			if (s.match("&lt;!--")) 
			{
				s.seekg(7);
				while (s.isOk()) {
					if (s.match("--&gt;")) {
						s.seekg(6);
						break;
					}
					++s;
				}
				continue;
			}
			// process html entities
			if (s.peek() == '&')	{
				++s;
				tc = decodeHtmlEntity(s);
				if (tc == 0) { 
					out.append('&');
				}
				else
					out.append(tc);
				continue;
			}
			// process templates - todo in future
			/*if (i+1 < len && s[i] == '{' && s[i+1]=='{')	
			{
				//i += 2;
				//processTemplate(input, i, out);
				continue;
			}
			*/
			// new paragraph -> count them and end if limit exceeded
 			if (s.match("\n\n")) {
				out.append("\n\n");
				s.seekg(2);

				++paragraph;
				if (paragraph > 5)
 					return true;

				continue;
 			}

			// write other data to the output
			out.append(s.get());
		}
		return false;
	}

	/** End opened paragraph or paragraph like elements (e.g. formatting, preformatted). */
	void endParagraph(WorkData &work, std::string &out) 
	{
		if (work.bold) {
			work.bold = false;
			out.append("</b>");
		}

		if (work.italics) {
			work.italics = false;
			out.append("</i>");
		}

		if (work.preblock) {
			work.preblock = false;
			out.append("</pre>");
			return;
		}

		if (work.preformated) {
			work.preformated = false;
			out.append("</pre>");
			return;
		}

		if (work.nowiki) {
			work.nowiki = false;
		}

		if (work.isInList() || work.isInTable()) {
			return;
		}

		if (work.inparagraph) {
			work.inparagraph = false;
			out.append("</p>");
		}
	}

	/** Start new paragraph if applicable (not in list, table or preblock). */
	void startParagraph(WorkData &work, std::string &out)   
	{
		if (work.isInList() || work.isInTable() || work.preblock) {
			return;
		}
	   
		if (!work.inparagraph) {
			work.inparagraph = true;
			out.append("<p>");
		}
	}

	/** Do inline formatting and anchors (for heading formatting). */
	void doInline(WorkData &work, string &out) 
	{
		char c; 
		while (work.isOk()) {
			c = work.peek();
			// anchors
			if (c == '[') {
				work.seekg(1);
				doAnchors(work,out);
				continue;
			}
			// apostrophes section
			else if (c == '\'') {
				doApostrophes(work,out);
				continue;
			}
			out.push_back(c);
			++work;
		}
	}

	/** Process apostrophes - bold and italics formatting. */
	void doApostrophes(WorkData &work, string &out) 
	{
		// number of apostrophes
		int apostrophes = 0;

		// find starting apostrophes
		while( work.isOk() && work.peek() == '\'') {
			++apostrophes;
			++work;
		}

		// create elements according to found apostrophes
		switch (apostrophes) {
			case 0:
				return;
			case 1:
				out.push_back('\'');
				return;
			case 2:
				if (work.italics) {
					work.italics = false;
					out.append("</i>");
				}
				else {
					work.italics = true;
					out.append("<i>");
				}
				break;
			case 3:
				if (work.bold) {
					work.bold = false;
					out.append("</b>");
				}
				else {
					work.bold = true;
					out.append("<b>");
				}
				break;
			case 4:
				if (work.bold) {
					work.bold = false;
					out.append("</b>'");
					return;
				}
				else
					out.push_back('\'');
				work.bold = true;
				out.append("<b>");
				break;
			default:
				if (work.bold) {
					work.bold = false;
					out.append("</b>");
				}
				else { 
					work.bold= true;
					out.append("<b>");
				}
				
				if (work.italics) {
					work.italics = false;
					out.append("</i>");
				}
				else {
					work.italics = true;
					out.append("<i>");
				}
				
				out.append(apostrophes-5,'\'');
		}
	}

	/** Process headings (==...== at the begining of the line). */
	void doHeadings(WorkData &work, std::string &out)  
	{
		char c = work.get();
		size_t level = 0;
		size_t level2 = 0;
		stringRef sref;
		sref.setString(work.getBuffer());;

		// find heading level indicated by opening equals
		while (work.isOk() && c == '=') {
			++level;
			c = work.get();
		}

		// set start of heading text reference
		sref.setStart(work.tellg()-1);

		// skip through heading text
		while (work.isOk() && c != '\n' && c != '=') {
			c = work.get();
		}

		if (c == '\n') {
			// newline, no matching equals -> append unformatted text and return
			work.unget();
			out.append(level,'=');
			return;
		}
		else if (c == '=') {
			// set end of heading text reference
			sref.setEnd(work.tellg()-1);

			// find heading level indicated by closing equals
			while (work.isOk() && c =='=') {
				++level2;
				c = work.get();
			}
			work.unget();

			// find if number of opening and closing equals matches and if heading level is permitted
			if (level == level2 && level < 7) {
				// end current paragraph
				endParagraph(work,out);

				// output heading data start
				out.append("<a name=\"");
				stringRef sref2 = sref;
				sref2.trim();
				sref2.appendToUrlEncoded(out);
				out.append("\"></a>");
				itoa(level,work.buf.data,10);
				out.append("<h");
				out.append(work.buf.data);
				out.push_back('>');

				// process heading text
				work.setg(sref.getStart());
				work.setTempEnd(sref.getEnd());
				doInline(work,out);
				work.setTempEnd(-1);
				work.seekg(level2);

				// output heading data end
				out.append("</h");
				out.append(work.buf.data);
				out.push_back('>');
				out.push_back('\n');
				return;
			}
		}

		// append unformatted if something went wrong
		out.append(level,'=');
		work.setg(sref.getStart());
	}

	/** Process anchors - in [ ] or [[ ]]. */
	void doAnchors(WorkData &work, std::string &out) 
	{
		stringRef anchorRef;
		
		char c = work.peek();

		// process wiki links
		if (c == '[') {
			++work;
			anchorRef.setString(work.getBuffer());
			anchorRef.setStart(work.tellg());

			stringRef pipeRef;
			stringRef props;

			// process start of the link
			while (work.isOk()) {
				c  = work.get();
				if (c == '|') {
					anchorRef.setEnd(work.tellg()-1);
					break;
				}
				if (c == '[' || c == '\n') {
					out.append("[[");
					work.setg(anchorRef.getStart());
					return;
				}
				if (c == ']') {
					anchorRef.setEnd(work.tellg()-1);						
					break;
				}
			}

			// process pipes
			if (c == '|') {
				pipeRef.setString(work.getBuffer());
				pipeRef.setStart(work.tellg());
				props.setString(work.getBuffer());
				props.setStart(work.tellg());
				
				// number of opened [[ brackets
				int z = 1;

				while(work.isOk()) {
					c = work.get();
					if (c == '|') {
						props.setEnd(work.tellg()-1);
						pipeRef.setStart(work.tellg());
						continue;
					}
					if (c == '[' && work.peek() == '[') {
						++z;
						++work;
						continue;
					}
					if (c == ']' && work.peek() == ']') {
						if (z == 1) {
							pipeRef.setEnd(work.tellg()-1);
							break;
						}
						else {
							++work;
							--z;
							continue;
						}
					}
					if (c == '[' || c == '\n') {
						out.append("[[");
						work.setg(anchorRef.getStart());
						return;
					}
				}									 
			}

			// final link processing (find closing bracket)
			c = work.get();
			if (c != ']') {
				// no closing bracket -> append unformatted
				out.append("[[");
				work.setg(anchorRef.getStart());
				return;
			}
			else {
				bool newline = false;
			    stringRef nameRef;
				int nsid;

				// separate page name and namespace
				separatePageName(anchorRef,nameRef,nsid);

				// remove category links (TODO: categories)
				if (nsid == categoryId) {
					return;	
				}

				// process anchorline (archor is only text on current line)
				if (work.anchorline && work.peek() == '\n') {
					newline = true;
					endParagraph(work,out);
					startParagraph(work,out);
				}

				// find if link namespace is included
				if (!isIncluded(nsid)) {
					// namespace not included -> create link to original wiki
					int ind = getNamespace(nsid);
					out.push_back('[');
					out.append("<a href=\"");
					out.append(basename);
					out.append(namespaces[ind].name);
					out.push_back(':');
					nameRef.appendToUrlEncoded(out);
					out.append("\">");
					out.append(namespaces[ind].name);
					out.push_back(':');
					nameRef.appendTo(out);
					out.append("</a>");
					if (!pipeRef.isEmpty()) {
						out.append(" (");
						// process link caption text
						work.setg(pipeRef.getStart());
						work.setTempEnd(pipeRef.getEnd());
						doInline(work,out);
						work.setTempEnd(-1);
						work.seekg(2);
						out.push_back(')');
					}
					out.push_back(']');

					if (newline) {
						endParagraph(work,out);
					}
					return;
				}

				// create interwiki link

				// get trailing text concatenade to link text
				stringRef trailing;	
				trailing.setString(work.getBuffer());
				trailing.setStart(work.tellg());
				while(work.isOk()) {
					c = work.get();
					if (isspace(c) || isMetaCharacter(c) || ispunct(c)) {
						work.unget();
						trailing.setEnd(work.tellg());
						break;
					}
				}

				// find if link is available
				if (nsid == UNAVAIL_LINKS_NS) {
					// link is unavailable
					out.append("<span class=\"new\">");
					if (!pipeRef.isEmpty()) {
						pipeRef.appendTo(out);
						trailing.appendTo(out);
					}
					else {
						anchorRef.setStart(anchorRef.getStart()+2);
						anchorRef.appendTo(out);
						trailing.appendTo(out);
					}
					out.append("</span>");
				}
				else {
					// create available interwiki link
					out.append("<a href=\"");
					anchorRef.appendToUrlEncoded(out);
					out.push_back('\"');
					out.push_back('>');

					if (!pipeRef.isEmpty()) {
						pipeRef.appendTo(out);
						trailing.appendTo(out);
						out.append("</a>");
					}
					else {
						if (nsid == UNAVAIL_LINKS_NS) {
							anchorRef.setStart(anchorRef.getStart()+2);
						}
						anchorRef.appendTo(out);
						trailing.appendTo(out);
						out.append("</a>");
					}
				}
				if (newline) {
					endParagraph(work,out);
				}
			}
		}
		// process external links
		else {
			stringRef titleRef;
			anchorRef.setString(work.getBuffer());
			anchorRef.setStart(work.tellg());

			// process start of the link
			while (work.isOk()) {
				c  = work.get();
				if (c == ' ') {
					anchorRef.setEnd(work.tellg()-1);
					break;
				}
				if (c == '[' || c == '\n') {
					out.append("[");
					work.setg(anchorRef.getStart());
					return;
				}
				if (c == ']') {
					anchorRef.setEnd(work.tellg()-1);						
					break;
				}
			}

			// create link only if it starts with valid schema
			if (!anchorRef.startsWith("http://") && !anchorRef.startsWith("ftp://") &&
				!anchorRef.startsWith("mailto:") && !anchorRef.startsWith("https://") && 
				!anchorRef.startsWith("news://") &&	!anchorRef.startsWith("irc://") && 
				!anchorRef.startsWith("gopher://")) {

				out.append("[");
				work.setg(anchorRef.getStart());
				return;
			}

			// process optional display text
			if (c == ' ') {
				titleRef.setString(work.getBuffer());
				titleRef.setStart(work.tellg());
				while (work.isOk()) {
					c  = work.get();
					if (c == '[' || c == '\n') {
						out.append("[");
						work.setg(anchorRef.getStart());
						return;
					}
					if (c == ']') {
						titleRef.setEnd(work.tellg()-1);
						break;
					}
				}
			}

			// output link data
			out.append("<a class=\"external\" href=\"");
			anchorRef.appendTo(out);
			out.append("\">");
			if (!titleRef.isEmpty()) {
				// with title
				titleRef.appendTo(out);
				out.append("</a>");
			}
			else {
				// no title -> automatic numbering
				out.push_back('[');
				++work.extlinknum;
				itoa(work.extlinknum,work.buf.data,10);
				out.append(work.buf.data);
				out.push_back(']');
				out.append("</a>");
			}
		}
	}


	/** Process horizontal line (4 or more hyphens at start of the line). */
	void doHorizontalLine(WorkData &work, std::string &out) 
	{
		int n = 0;

		while(work.isOk() && work.peek() == '-' && !isspace(work.peek())) {
			++n;
			++work;
		}
		if (n >= 4) {
			endParagraph(work,out);
			out.append("<hr/>");
		}
		else {
			startParagraph(work,out);
			out.append(n,'-');
		}
	}

	/** Process lists. */
	void doLists(WorkData &work, std::string &out) 
	{
		stringRef list;
		int len;
		int i;
		int lastlistsz;
		int listsz;
		char c;

		list.setString(work.getBuffer());
		list.setStart(work.tellg());

		// find list prefix
		while(work.isOk()) {
			c = work.get();
			if (c != '*' && c!='#' && c!=':' && c!=';') {
				break;
			}
		}
		work.unget();
		list.setEnd(work.tellg());
			   
		// find common prefix to current lis prefix and last line list prefix
		len = commonPrefixLength(work.lastList,list);

		lastlistsz = work.lastList.size();
		listsz = list.size();

		string last = work.lastList.getString();
		string newlist = list.getString();

		// process same level of list
		if (len == lastlistsz && len == listsz) {
			switch(list[listsz-1]) {
				case '*':
				case '#':
					out.append("</li><li>");
					break;
				case ':':
					if (work.deflist == DEF_TERM) 
						out.append("</dt><dd>");
					else 
						out.append("</dd><dd>");
					work.deflist = DEF_NO;
					break;
				case ';':
					if (work.deflist == DEF_DESCR)
						out.append("</dd><dt>");
					else
						out.append("</dt><dt>");
					work.deflist = DEF_TERM;
					break;
				default:
					break;
			}
			return;
		}

		// process last prefix
		if (lastlistsz !=0) {
			for(i=lastlistsz-1;i >= len;--i) {	
				switch(work.lastList[i]) {
					case '*':
						out.append("</li></ul>");
						break;
					case '#':
						out.append("</li></ol>");
						break;
					case ':':
						if (work.deflist == DEF_TERM) {
							out.append("</dt></dl>");
						}
						else 
							out.append("</dd></dl>");
						work.deflist = DEF_NO;
						break;
					case ';':
						if (listsz == 1 && list[0] == ':') {
							if (work.deflist != DEF_DESCR)
								work.deflist = DEF_TERM;
							break;
						}
						if (work.deflist == DEF_DESCR) {
							out.append("</dd></dl>");
						}
						else 
							out.append("</dt></dl>");
						work.deflist = DEF_NO;
						break;
					default:
						break;
				}
			}
		}

		// continue last list
		if (len > 0 && len >= listsz) {
				switch(list[len-1]) {
					case '*':
					case '#':
						out.append("</li><li>");
						break;
					case ':':
						if (work.deflist == DEF_TERM) {
							out.append("</dt><dd>");
						}
						else
							out.append("</dd><dd>");
						work.deflist = DEF_NO;
						break;
					case ';':
						if (work.deflist == DEF_TERM)
							out.append("</dt><dt>");
						else
							out.append("</dd><dt>");
						work.deflist = DEF_TERM;
					default:
						break;
				}
		}
		
		// open list
		for(i=len;i < listsz;++i) {	
			switch(list[i]) {
				case '*':
					out.append("<ul><li>");
					break;
				case '#':
					out.append("<ol><li>");
					break;
				case ':':
					if (work.deflist == DEF_DESCR) 
						out.append("</dd><dd>");
					else if (work.deflist == DEF_TERM) {
						out.append("</dt><dd>");
					}
					else
						out.append("<dl><dd>");
					work.deflist = DEF_NO;
					break;
				case ';':
					out.append("<dl><dt>");
					work.deflist = DEF_TERM;
					break;
				default:
					break;
			}
		}
		work.lastList = list;
	}

	/** Finalization of lists. */
	void endList(WorkData& work, string &out) 
	{
		for(int i=work.lastList.size()-1;i>=0;--i) {
			switch(work.lastList[i]) {
					case '*':
						out.append("</li></ul>");
						break;
					case '#':
						out.append("</li></ol>");
						break;
					case ':':
						if (work.deflist == DEF_TERM)
							out.append("</dt></dl>");
						else 
							out.append("</dd></dl>");
						work.deflist = DEF_NO;
						break;
					case ';':
						if (work.deflist == DEF_TERM) {
							out.append("</dt></dl>");
						} else
							out.append("</dd></dl>");
						work.deflist = DEF_NO;
						break;
					default:
						break;
			}
		}
		work.lastList.setRegion(0,0);
	}

	/** Get line from input ("work" object). 
	 * @return String reference to line string.
	 */
    stringRef getLine(WorkData &work) 
	{
		stringRef sref;
		sref.setString(work.getBuffer());
		sref.setStart(work.tellg());
		char c;
		while (work.isOk()) {
			c = work.get();
			if (c == '\n') {
				break;
			}
		}
		sref.setEnd(work.tellg()-1);
		work.setNewline();
		return sref;
	}

	/** Close tag "tag" and append data to "out" buffer. */
	void closeTag(Tag tag, string &out)
	{
		switch(tag) {
			case TABLE: out.append("</table>\n");break;
			case TD: out.append("</td>\n");break;
			case TR: out.append("</tr>\n");break;
			case TH: out.append("</th>\n");break;
			case CAPTION: out.append("</caption>\n");break;
			default: break;
		}
	}

	/** Start table. */
	void startTable(WorkData &work, string &out) 
	{
		stringRef tmp = getLine(work);

		// create row and table cell if not in cell (i.e. td, th, caption)
		Tag t = work.tabletags.peek();
		if (work.isInTable() && t != TD && t != TH && t != CAPTION ) {
			closeTag(t,out);
			out.append("<tr><td>");
			work.tabletags.push(TR);
			work.tabletags.push(TD);
		}

		// create table tag
		out.append("\n<table ");
		tmp.appendTo(out);
		out.append(">");
		work.tabletags.push(TABLE);
	}

	/** Find cell attributes. 
	 * @return String reference to cell attributes.
	 */
	stringRef getTableCellAttributes(WorkData &work) 
	{
		stringRef out;
		char c;
		char p;
		out.setString(work.getBuffer());
		out.setStart(work.tellg());
		while(work.isOk()) {
			 c = work.get();
			 p = work.peek();

			 // we found newline, link or cell delimiter -> end and return empty 
			 if ( c == '\n' || (c == '|' && p == '|') 
				 || (c == '!' && p == '!') || (c == '[' && p == '[')) {
				work.setg(out.getStart());
				out.clear();
				return out;
			 }	

			 // we found end of attributes block
			 if (c == '|') {
				 out.setEnd(work.tellg()-1);
				 return out;
			 }
		}
		out.clear();
		return out;
	}

	/** Process table cell. */
	void doTableCell(WorkData &work, string &out, Tag tag)
	{
		if (tag != TH && tag != TD) return;

		Tag	t = work.tabletags.pop();

		// close other tags until last row tag || table tag (i.e. td, caption, th)
		while (work.isInTable() && t != TABLE && t != TR ) {
			closeTag(t,out);
			t = work.tabletags.pop();
		}
		
		if (t == TABLE) {
			// create row
			work.tabletags.push(t);
			out.append("<tr>");
			work.tabletags.push(TR);
		}
		else if (t == TR) {
			work.tabletags.push(t);
		}
		else return;

		char delim = tag == TH?'!':'|';

		// get attributes of cell
		stringRef tmp = getTableCellAttributes(work);

		if (tag == TH) {
			// create heading cell 
			work.tabletags.push(TH);
			if (tmp.isEmpty())
				out.append("<th>");
			else {
				out.append("<th ");
				tmp.appendTo(out);
				out.push_back('>');
			}
		}
		else {
			// create normal cell
			work.tabletags.push(TD);
			if (tmp.isEmpty())
				out.append("<td>");
			else {
				out.append("<td ");
				tmp.appendTo(out);
				out.push_back('>');
			}
		}
	}

	/** Process table. */
	void doTable(WorkData &work, string &out) 
	{
		stringRef tmp;
		Tag t;
		char c = work.get();
		if (c =='|') {
			c = work.get();

			// table end
			if (c == '}') {
				// close other tags until last table tag (i.e. td, tr, caption, th)
				while (work.isInTable()) {
					t = work.tabletags.pop();
					closeTag(t,out);
					if (t == TABLE)
						break;
				}
			}
			// caption
			else if (c=='+') {
				t = work.tabletags.pop();
				// close other tags until last table tag (i.e. td, tr, caption, th)
				while (work.isInTable() && t != TABLE) {
					closeTag(t,out);
					t = work.tabletags.pop();
				}
				if (t == TABLE) {
					work.tabletags.push(t);
				}
				else return;

				out.append("<caption ");
				out.append(">");
				work.tabletags.push(CAPTION);
			}
			// table row
			else if (c=='-') {
				t = work.tabletags.pop();
				// close other tags until last table tag (i.e. td, tr, caption, th)
				while (work.isInTable() && t != TABLE) {
					closeTag(t,out);
					t = work.tabletags.pop();
				}
				if (t == TABLE) {
					work.tabletags.push(t);
					// get row attributes
					tmp = getLine(work);
					if (tmp.isEmpty()) 
						out.append("<tr>");
					else {
						out.append("<tr ");
						tmp.appendTo(out);
						out.push_back('>');
					}
					work.tabletags.push(TR);
				}
				else {
					work.unget();
					work.unget();
					return;
				}
			}
			// table cell
			else {
				work.unget();
				doTableCell(work,out,TD);
			}
		}
		// heading cell
		else if (c == '!') {
			doTableCell(work,out,TH);
		}
		else 
			work.unget();
	}

	/** End table -> close open td, tr and table tags. */
	void endTable(WorkData &work, string &out) 
	{
		Tag t;
		while(work.isInTable())	{
			t = work.tabletags.pop();
			closeTag(t,out);
		}
	}

	/** Get tag text.
	 * @return String reference to tag text (without '>' '<').
	 */
	stringRef getTag(WorkData &work) 
	{
		stringRef sref;
		char c;

		c = work.peek();
		if (c!='<') {
			return sref;			
		}

		++work;

		if ((work.nowiki || work.preformated) && work.peek() != '/') {
			work.unget();
			return sref;
		}

		sref.setString(work.getBuffer());;
		sref.setStart(work.tellg());

		while(work.isOk()) {
			c = work.get();
			if (c == '\n')
				break;
			if (c == '>') {
				break;
			}
		}
		if (c == '\n') {
			work.setg(sref.getStart()-1);
			sref.setStart(0);
			sref.setEnd(0);
			return sref;
		}
		sref.setEnd(work.tellg()-1);
		return sref;		
	}

	/** Process HTML tags. */
	void doTags(WorkData &work, string &out)
	{
		stringRef tag = getTag(work);
		tag.trim();

		// empty tag
		if (tag.isEmpty()) {
			if (work.peek() == '<') {
				out.append("&lt;");
				++work;
			}
			return;
		}

		// nowiki -> escape wiki formatting
		if (!work.nowiki && tag == "nowiki" ) {
			work.nowiki = true;
		}
		// end of nowiki
		else if (work.nowiki && tag == "/nowiki") {
			work.nowiki = false;
		}   
		// preformated text
		else if (!work.preformated && tag == "pre") {
			endParagraph(work,out);
			work.skiptype = PRE;
			work.preformated = true;
			out.append("<pre>");
		}
		// end of preformated text
		else if (work.preformated && tag == "/pre") {
			if (work.skiptype == PRE) {
				work.preformated = false;
				out.append("</pre>");
			}
		}
		// timeline 
		else if (tag.startsWith("timeline")) {
			if (!work.skip) {
				work.skiptype = TIMELINE;
				work.skip = true;
			}
			//out.append("&lt;timeline&gt;");
			//work.preformated = true;
		}
		else if (tag == "/timeline") {
			if (work.skiptype == TIMELINE)
				work.skip = false;
			//out.append("&lt;/timeline&gt;");
			//work.preformated = false;
		}
		// gallery
		else if (tag.startsWith("gallery")) {
			if (!work.skip) {
				work.skiptype = GALLERY;
				work.skip = true;
			}
		}
		else if (tag == "/gallery") {
			if (work.skiptype == GALLERY)
				work.skip = false;
		}
		// math -> interpret as nowiki
		else if (tag == "math") {
			work.nowiki = true;
		}
		else if (tag == "/math") {
			work.nowiki = false;
		}
		// reference
		else if (tag.startsWith("ref")) {
			if (tag[tag.size()-1] == '/') {
				return;
			}

			if (!work.skip) {
				work.skiptype = REF;
				work.skip = true;
			}
		}
		else if (tag == "/ref") {
			if (work.skiptype == REF)
				work.skip = false;
		}
		// comments
		else if (tag.size() >= 5 &&tag.startsWith("!--") && tag[tag.size()-1] == '-' && tag[tag.size()-2] == '-') {
					 // just ignore 
		}
		// source code -> preformatted
		else if (!work.preformated && tag.startsWith("source")) {
			endParagraph(work,out);
			work.preformated = true;
			work.skiptype = SOURCE;
			out.append("<pre>");
		}
		else if (work.preformated && tag == "/source") {
			if (work.skiptype = SOURCE) {
				work.preformated = false;
				out.append("</pre>");
			}
		}
		else {
			// output tag to output
			if (work.skip) {
				// just ignore
			}
			else if (work.nowiki || work.preformated) {
				// we want tag preformatted
				out.append("&lt;");
				tag.appendTo(out);
				out.append("&gt;");
			}
			else {
				out.push_back('<');
				tag.appendTo(out);
				out.push_back('>');
			}
		}
	}
};


#endif
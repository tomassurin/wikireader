#ifndef WikiHelper_H__
#define WikiHelper_H__

/**
 * @file
 * @brief This file contains helper functions and classes for wiki data processing.
 *
 * This file is part of module WikiData.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include <stdio.h>
#include <cctype>
#include <string>

#define itoa _itoa

#pragma comment(lib, "../Shared/shared.lib")
#include "../Shared/StringUtils.h"
#include"../Shared/Types.h"

using namespace std;

/// HTML tags used mainly to store table structure.
enum Tag { NOTAG=0, TABLE, TR, TD, TH, CAPTION, NOWIKI, PRE, REF, TIMELINE, GALLERY, MATH, SOURCE };


/** Find if given character is wiki meta character */
bool isMetaCharacter(char c) 
{
	return c == '[' || c=='{' || c=='\'' || c=='<';
}

/// Definition list types.
enum DefList { DEF_NO = 0, DEF_TERM, DEF_DESCR};

/** Canonize name of wiki article.
 * @param input Non canonized article name.
 * @param firstLetterCase Indicates if first character of article name is case insensitive.
 * @return Canonized name of article with non canonized name "input".
 */
std::string canonizeName(const std::string &input, bool firstLetterCase = false) 
{
	if (input.empty())
		return input;

	string inp = input;

	// replace _ -> space
	for(size_t i=0;i<inp.size();++i) {
		if (inp[i] == '_') {
			inp[i] = ' ';
		}
	}
	// capitalize first character
	if (!firstLetterCase) {
		// if character is from US-ASCII (<127 (7F))
		if (inp[0] < 0x7F) {
			inp[0] = toupper(inp[0]);
			return inp;
		}

		wstring winp = StringUtils::utf8To16(inp);		
		winp[0] = towupper(winp[0]);
		return StringUtils::utf16To8(winp);
	}
	return inp;
}


/** Simple buffer used in wiki article processing. */
struct BufWorkData 
{
	/// Pointer to buffer data.
	char *data;
	/// Buffer length.
	size_t len;
	/// Alocated capacity of buffer.
	size_t capacity;
	/// Actual buffer index (while processing).
	size_t i;
};

/** Read only string reference class. */
class stringRef 
{
public:

	/** Constructor. */
	stringRef() 
	{
		clear();
	}

	/** Assignment operator. */
	stringRef& operator=(stringRef &ref) 
	{
		this->s = ref.s;
		this->len = ref.len;
		this->start = ref.start;
		this->end = ref.end;
		return *this;
	}

	/** Set reference to string portion. 
	 * @param str Referenced string (c_str).
	 * @param length Length of referenced string.
	 * @param start Start offset of reference in string "str".
	 * @param end End offset of reference in string "str" (position after last character of reference).
	 */
	void setRef(const char *str, size_t length, size_t start, size_t end ) 
	{
		clear();
		if (start < length && end <= length) {
			s = str;
			len = length;
			this->start = start;
			this->end = end;
		}
	}

	/** Set reference to string portion. 
	 * @param str Pointer to referenced string.
	 * @param length Length of referenced string.
	 * @param start Start offset of reference in string "str".
	 * @param end End offset of reference in string "str" (position after last character of reference).
	 */
	void setRef(std::string *str, size_t start, size_t end)
	{
		if (str == NULL)
			return;
		setRef(str->c_str(), str->size(), start, end);
	}

	/** Set start offset of reference (referenced string must be already set). */
	void setStart(size_t start) 
	{
		if (s == NULL)
			return;
		this->start = 0;
		if (start < len) {
			this->start = start;
		}
	}

	/** Set end offset of reference (referenced string must be already set). */
	void setEnd(size_t end) 
	{
		if (s == NULL)
			return;
		this->end = 0;
		if (end <= len) {
			this->end = end;
		}
	}

	/** Set reference region (referenced string must be already set). */
	void setRegion(size_t start, size_t end) 
	{
		if (s == NULL)
			return;

		this->start = 0;
		this->end = 0;
		if (start < len && end <= len) {
			this->start = start;
			this->end = end;
		}
	}

	/** Set referenced string. 
	 * @param str Referenced string (c_str).
	 * @param len Length of referenced string.
	 */
	void setString(const char *str, size_t len) 
	{
		if (str != NULL) {
			s = str;
			this->len = len;
			this->start = 0;
			this->end = 0;
		}
	}

	/** Set referenced string. 
	 * @param str Pointer to referenced string.
	 */
	void setString(std::string *str) {
		if (str == NULL)
			return;
		setString(str->c_str(), str->size());
	}

	/** Apped referenced portion of string denoted by "start" and "end" offsets to output string.
	 * @param str Output string.
	 */
	void appendTo(std::string &str) 
	{
		if (!isOk())
			return;

		for(size_t i=start;i<end;++i) {
			str.push_back(s[i]);
		}
	}

	/** Same as appendTo() but appended string is url encoded. */
	void appendToUrlEncoded(std::string &str) 
	{
		if (!isOk())
			return;

		unsigned int c;
		unsigned char cc;
		char digit1;
		char digit2;

		for(size_t i=start;i<end;++i) {
			if (isurlchar(s[i])) {
				str.push_back(s[i]);
			}
			else if (s[i] == '<') {
				str.append("%26lt%3B");
			}
			else if (s[i] == '>') {
				str.append("%26gt%3B");
			}
			else if (s[i] == '\"') {
				str.append("%26quot%3B");
			}
			else if (s[i] == '&') {
				str.append("%26amp%3B");
			}
			else {
				cc = s[i];
				str.push_back('%');
				c = cc % 16;
				if (c < 10)
					digit2 = '0'+c;
				else {
					digit2 = 'A'+ c - 10;
				}
				c = (cc / 16) % 16;
				if (c < 10)
					digit1 = '0'+c;
				else {
					digit1 = 'A'+ c - 10;
				}
				str.push_back(digit1);
				str.push_back(digit2);
			}
		}
	}

	/** Trim string reference (get rid of whitespaces at start and end of string). */
	void trim() 
	{
		if (!isOk())
			return;

		while ( start < end && (s[start] == ' ' || s[start] == '\t')) 
			++start;

		while ( end > start && (s[end-1] == ' ' || s[end-1] == '\t'))
			--end;
	}

	/** Trim string reference that contains article name */
	void trimPageName() 
	{
		if (!isOk())
			return;

		while ( start < end && (s[start] == ' ' || s[start] == '_')) 
			++start;

		while ( end > start && (s[end-1] == ' ' || s[end-1] == '_'))
			--end;
	}

	/** @return True if reference is without error. */
	bool isOk() 
	{
		if (s == NULL)
			return false;

		if (start >= len)
			return false;
		if (end > len)
			return false;

		return true;
	}

	/** Initialize object to its default state. */
	void clear() 
	{
		s = NULL;
		len = 0;
		start = 0;
		end = 0;
	}

	/** @return True if length of string reference is 0. */
	bool isEmpty() const 
	{
		if (s == NULL)
			return true;
		if (size() == 0)
			return true;
		return false;
	}

	/** @param off Offset which we want to append.
	 * @return Character at position start+off of referenced string or 0 if position is out of range. 
	 */
	char operator[](size_t off)	
	{
		if (s==NULL || start+off >= end) return 0;
		return s[start+off];
	}

	/** @return Length of string reference. */
	size_t size() const 
	{
		if (end <= start)
			return 0;
		return end-start;
	}

	/** @return True if string reference contains character c. */
	bool contains(char c)
	{
		for(size_t i=0;i<size();++i) {
			if (operator[](i) == c)
				return true;
		}
		return false;
	}

	/** Compare this string reference to other string.
	 * @param str String which we are comparing to (c_str).
	 * @param bsz Size of string "str".
	 * @return -1 if reference < str
	 *          0 if reference == str
	 *          1 if reference > str
	 */
	int compare(const char *str, size_t bsz) 
	{
		int min;
		size_t asz = size();
		if (asz < bsz)
			min = asz;
		else 
			min = bsz;

		//	out = ::memcmp(s+start,str,min);

		//	return ( min != 0 ? min : asz < bsz ? -1
		//				 : asz == bsz ? 0 : +1);

		for(int i=0;i<min;++i) {
			if (operator[](i) < str[i])
				return -1;
			if (operator[](i) > str[i])
				return 1;
		}

		if (asz == bsz) {
			return 0;
		}
		if (asz < bsz)
			return -1;
		else
			return 1;
	}

	/** Equality operator. */
	bool operator==(const char* str) 
	{
		if (!isOk())
			return false;

		return compare(str, strlen(str)) == 0;
	}

	/** Equality operator. */
	bool operator==( const std::string &str) 
	{
		if (!isOk())
			return false;

		return compare(str.c_str(),str.size()) == 0;
	}

	/** Compare operator. */
	bool operator<(const std::string &str) 
	{
		if (!isOk())
			return false;

		return compare(str.c_str(), str.size()) < 0;
	}

	/** Compare operator. */
	bool operator>(const std::string &str) 
	{
		if (!isOk())
			return false;

		return compare(str.c_str(),str.size()) > 0;
	}

	/** Compare operator. */
	bool operator>=(const std::string &str) 
	{
		if (!isOk())
			return false;

		return compare(str.c_str(), str.size()) >= 0;
	}

	/** Compare operator. */
	bool  operator<=(const std::string &str) 
	{
		if (!isOk())
			return false;

		return compare(str.c_str(), str.size()) <=0;
	}

	/** @return String that reference is referencing to. */
	std::string getString() 
	{
		if (!isOk())
			return "";

		
		return string(s+start, end-start );
	}

	/** @return c_str referenced string. */
	const char *getBuffer() 
	{
		return s;
	}

	/** @return length of referenced string. */
	size_t getBufferLength() 
	{
		return len;
	}

	/** @return Start offset of reference. */
	size_t getStart() 
	{
		return start;
	}

	/** @return End offset of reference. */
	size_t getEnd() 
	{
		return end;
	}

	/** @param str String we are comparing to (c_str).
	 * @return True if string reference starts with "str".
	 */
	bool startsWith(char *str) 
	{
		if (str == NULL)
			return false;

		size_t j = 0;
		while(str[j] != 0) {
			if (start+j >= end) 
				return false;
			if (s[start+j] != str[j])
				return false;
			++j;
		}
		return true;
	}

private:
	/// Referenced string.
	const char *s;
	/// Length of referenced string.
	size_t len;
	/// Start offset of reference in referenced string.
	size_t start;
	/// End offset of reference in referenced string.
	size_t end;
};

/** Simple stack for Tag elements with static capacity. */
class TagStack {
public:
	TagStack(size_t capacity) 
	{
		arr = NULL;
		setCapacity(capacity);
	}

	TagStack() 
	{
		arr = NULL;
		clear();
	}

	void setCapacity(size_t capacity) 
	{
		clear();
		arr = new Tag[capacity];
		this->cap = capacity;
	}

	~TagStack() 
	{
		clear();
	}

	void clear() 
	{
		if (arr != NULL)
			delete arr;
		arr = NULL;
		cap = 0;
		pos = 0;
	}

	void push(Tag it) 
	{
		if (isOk())
			arr[pos++] = it;
		else if (pos > cap) {
			Tag *arr2 = new Tag[pos+32];
			memcpy(arr,arr2,sizeof(Tag)*cap);
			cap = pos+32;
			delete arr;
			arr = arr2;
		}
	}

	Tag pop() 
	{
		if (isOk())
			return arr[--pos];
		return NOTAG;
	}

	Tag peek() 
	{
		if (isOk()) 
			return arr[pos-1];
		return NOTAG;
	}

	bool isOk() 
	{
		return pos < cap && pos >= 0;
	}

	bool isEmpty() 
	{
		return pos == 0;
	}

	void setEmpty() 
	{
		pos = 0;
	}

private:
	/// Internal array of stack elements.
	Tag *arr;
	/// Stack capacity.
	size_t cap;
	/// Index of stack top in internal array.
	size_t pos;
};

/** Class used to store wiki article conversion progress. Also mimics 
 * iostream API for easy access to article data.
 */
class WorkData {
public:

	/** Constructor. */
	WorkData() : tabletags(32) 
	{
		buf.data = NULL;
		buf.capacity = 0;		
		clear();
		buf.data = new char[512];
		buf.capacity = 512;
	}

	/** Destructor. */
	~WorkData() 
	{
		clear();
	}

	/** Initialize object to its default state. */
	void clear() 
	{
		s.clear();
		len = 0;
		origend = 0;
		i = 0;
		linestart = 0;

		req = NULL;

		if (buf.capacity != 0) {
			delete buf.data;
			buf.capacity = 0;
		}

		inparagraph = false;
		italics = false;
		bold = false;
		newline = false;
		preblock = false;
		nowiki = false;
		preformated = false;
		skip = false;
		skiptype = NOTAG;
		anchorline = false;

		lastList.clear();
		deflist = DEF_NO;

		extlinknum = 0;

	}

	/** Clear article data. */
	void clearBuffer() 
	{
		s.clear();
		len = 0;
		origend = 0;
		i = 0;
		lastList.clear();
	}

	/** Subscript operator. 
	 * @param off Offset in article data. 
	 * @return Character at index off in article data. 
	 */
	char operator[](size_t off) 
	{
		return s[off];
	}

	/** Increment operator (prefix) */
	void operator++() 
	{
		++i;
	}

	/** Increment operator (postfix) */
	void operator++(int) 
	{
		++i;
	}

	/** @return True if position in article data is valid (we are not at the end). */
	bool isOk() 
	{
		return i < len;
	}

	/** Get character from actual position and increment position. 
     * @return Char from actual position in article data.
	 */
	char get() 
	{
		if (isOk())
			return s[i++];
		else return 0;
	}

	/** Same as get() but it's not incrementing position. 
	 * @return Char from actual position in article data.
	 */
	char peek() 
	{
		if (isOk())
			return s[i];
		else return 0;
	}

	/** Change actual position to one character less. */
	void unget() 
	{
		if (i>0)
			--i;
	}

	/** @return Chara at next position to actual in article data. */
	char next() 
	{
		if (i+1 < len)
			return s[i+1];
		else return 0;
	}

	/** @return Actual position in article data. */
	size_t tellg() 
	{
		if (isOk())
			return i;
		else return 0;
	}

	/** Sets actual postion in article data.
	 * @param off Position to set. 
	 */
	void setg(size_t off) 
	{
		if (off < len)
			i = off;
	}

	/** Change actual position.
	 * @param off Amount that we are changing position for. 
	 */
	void seekg(size_t off) 
	{
		if (i+off < len)
			i+=off;
		else i = len-1;
	}

	/** Append string to article data. */
	void append(const string &str) 
	{
		s.append(str);
		len = s.size();
		origend = len;
	}

	/** Append character to article data. */
	void append(const char c) 
	{
		s.push_back(c);
		len = s.size();
		origend = len;
	}

	/** Reserve capacity for article data buffer. */
	void reserve(size_t res) 
	{
		s.reserve(res);
	}

	/** @return Pointer to article data buffer. */
	string *getBuffer() 
	{
		return &s;
	}

	/** @return True if we are actually parsing list. */
	bool isInList() 
	{
		return !lastList.isEmpty();
	}

	/** @return True if we are actually parsing table. */
	bool isInTable() 
	{
		return !tabletags.isEmpty();
	}

	/** Sets newline state. */
	void setNewline() 
	{
		newline = true;
		linestart = 0;
		if (i < s.length())
			linestart = s[i];
	}

	/** Set temporary end offset. */
	void setTempEnd(size_t end)
	{
		if (end == -1) {
			len = origend;
			return;
		}
		origend = len;
		len = end;
	}

	/** Write chunk of data to article request source. */
	bool writeChunk(string &chunk) 
	{
		if (req == NULL) 
			return false;

		if (!req->writeChunk(chunk))
			return false;
		
		return true;
	}

	/** Find if string from actual position starts with "str".
	 * @param str Input string. 
	 * @return True if string from actual position in article data starts with "str".
	 */
	bool match(char *str) {
		if (str == NULL)
			return false;

		size_t j = 0;
		while(str[j] != 0) {
			if (i+j >= len) 
				return false;
			if (s[i+j] != str[j])
				return false;
			++j;
		}
		return true;
	}

	/// Article data buffer.
	string s;
	/// Length of article data.
	size_t len;
	/// Actual position in article data.
	size_t i;
	/// Used to store original end of article data.
	size_t origend;
	/// Pointer to request object (request for article we are processing).
	Request *req;
	/// Character at last line start.
	char linestart;
	/// Helper buffer.
	BufWorkData buf;

// state variables
		/// In paragraph.
		bool inparagraph;
		/// At newline (after \n character).
		bool newline;
		/// In nowiki element.
		bool nowiki;
		/// Inside skipped element.
		bool skip;
		/// Type of skipped element.
		Tag skiptype;
		/// Inside preformated element.
		bool preformated;
		// Inside pre block (space at line start).
		bool preblock;

		/// Inside italics formatting.
		bool italics;
		/// Inside bold formatting.
		bool bold;

		/// Definition list properties.
		DefList deflist;
		/// Reference to last list data.
		stringRef lastList;

		/// Stack of table tags.
		TagStack tabletags;

		/// Current number of external link.
		unsigned int extlinknum;

		/// Links on separate lines
		bool anchorline;
};

/** Class used to store wiki article preprocessing progress. Also mimics iostream
 * API for easy access to article data. It has roughly same API sa WorkData.
 */
class PreprocessWorkData {
public:

	PreprocessWorkData() 
	{
		clear();
	}

	~PreprocessWorkData() 
	{
		clear();
	}

	void clear() 
	{
		i = 0;
		s.data = NULL;
		s.length = 0;
	}

	char operator[](size_t off) 
	{
		return s.data[off];
	}

	void operator++() 
	{
		++i;
	}

	void operator++(int) 
	{
		++i;
	}

	bool isOk() 
	{
		if (s.data == NULL)
			return false;

		if (i < s.length)
			return true;

		return false;
	}

	char get() 
	{
		if (isOk())
			return s.data[i++];
		else return 0;
	}

	char peek() 
	{
		if (isOk())
			return s.data[i];
		else return 0;
	}

	void unget() 
	{
		if (i>0)
			--i;
	}

	char next() 
	{
		if (i+1 < s.length)
			return s.data[i+1];
		else return 0;
	}

	size_t tellg() 
	{
		if (isOk())
			return i;
		else return 0;
	}

	void setg(size_t off) 
	{
		if (off < s.length)
			i = off;
	}

	void seekg(size_t off) 
	{
		if (i+off < s.length)
			i+=off;
		else i = s.length-1;
	}

	size_t size()
	{
		return  s.length;
	}

	bool match(char *str) 
	{
		if (str == NULL)
			return false;

		size_t j = 0;
		while(str[j] != 0) {
			if (i+j >= s.length) 
				return false;
			if (s.data[i+j] != str[j])
				return false;
			++j;
		}
		return true;
	}

	/// Buffer with article data.
	ByteBuffer s;
	/// Actual position in article data.
	size_t i;
};


/** Find length of largest common prefix for 2 string references.
 * @param a, b String references.
 * @return Length of largest common prefix for a and b.
 */
size_t commonPrefixLength(stringRef &a, stringRef &b)
{
	size_t i = 0;
	while( i< a.size() && i < b.size()) {
		if (a[i] == b[i]) 
			++i;
		else break;
	}
	return i;
}

/** Class that holds information about wiki namespace. */
class WikiNamespace {
public:

	/** Constructor. */
	WikiNamespace() 
	{
		id = 0;
		included = false;
	}

	/** Equality operator. */
	bool operator==(const WikiNamespace &n) 
	{
		return name == n.name && id == n.id && included == n.included;
	}

	/// Localized name of namespace.
	string name;
	/// Id of namespace.
	int id;
	/// Indicates if namespace is included. 
	bool included;
};

#endif
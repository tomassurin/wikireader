#ifndef __WIKITITLEINDEX_H
#define __WIKITITLEINDEX_H

/**
 * @file
 * @brief This file contains implementation of WikiTitleIndex service.
 *
 * This file is part of module WikiData.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <ios>
#include <algorithm>
#include <vector>

#pragma comment(lib, "../Shared/shared.lib")
#include "../Shared/Types.h"
#include "../Shared/StringUtils.h"

using namespace std;


/// Last block item's ID.
static const char *levelend = "##BLOCKEND##";

/** Structure used to save temporary index data. */
struct IndexNode 
{
	/// Name of article.
	string id;
	/// Offset in index file at which next index level with wanted article can be found.
	size_t offset;
};

/** Implementation of index service. This service is used to access wikipedia article names index.
 * Searching in this index is case-sensitive. 
 */
class WikiTitleIndex : public ServiceIndex {
private:

	/// Index file's input stream.
	ifstream index;
	/// Auxiliary array which is used to store number conversion results or input.
	char num[5];
	/// Indicates if index is opened.
	bool opened;
	/// Number of index levels.
	int levels;
	/// Offset in index file at which actual data begins.
	ifstream::pos_type startoffset;
	/// Index nodes of master index (it's stored in memory).
	vector<IndexNode> ind;
	/// Indicates if index is indexing multivolume archive.
	bool multivolume;

	/** Converts big endian unsigned number given as array of bytes to built in type.
	 * @param data Input array. Byte representation of input bigendian unsigned number.
	 * @param length Length of input array (number of number's bytes).
	 * @return Converted number or -1 if error occured.
	 */
	size_t convertNumber(char data[], int length)
	{
		if (length < 1)
			return -1;

		unsigned long out = data[0];
		for(int i=1;i<length;++i) {
			out =out*256+(unsigned char)data[i];
		}
		return out;
	}

	/** Close index and return false.
	 * @return False. 
	 */
	bool closeF() 
	{
		index.close();
		return false;
	}

	/** Reads id from actual position in index file.
	 * @param buf String which is used to store read data.
	 * @param primary Indicates if index level from which we are reading is primary index (level 0).
	 */
	void getIdFromFile(string &buf, bool primary = false) 
	{
		char c;
		buf.clear();
		if (primary)	{
			c = index.get();
			if (c!='\0')
				return;
		}
		while(index.good()) {
			c = index.get();			
			if (c=='\0') 
				break;
			buf.push_back(c);
		}

		if (c!='\0')
			buf.clear();
	}

	/** Same as getIdFromFile bud string is read backwards and then reversed 
	 * (used while reading index items backwards - from higher items to lower). 
	 * This method can be used only in primary index(level 0).
	 * @param buf String which is used to store read data.
	 */
	void getIdFromFileRev(string &buf) 
	{
		char c;
		buf.clear();

		c = index.peek();
		index.seekg(-1,ios::cur);
		if (c!='\0') 
			return;

		while(index.good() && index.tellg() >= startoffset) {
			c = index.peek();			
			index.seekg(-1,ios::cur);
			if (c=='\0') 
				break;
			buf.push_back(c);
		}

		if (c!='\0') {
			buf.clear();
			return;
		}

		reverse(buf.begin(),buf.end());
	}

	/** Read number from actual position in index file.
	 * @param length Number of bytes to read.
	 * @return Number from file of size length bytes.
	 */
	size_t getNumberFromFile(unsigned int length) 
	{		
		if (length == 0)
			return -1;
		
		index.read(num,length);
		return convertNumber(num,length);
	}

	/** Same as getNumberFromFile but this time we are reading file 
	 * backwards (used while reading index items backwards - from 
	 * items with higher id's).
	 * @param length Number of bytes to read.
	 * @return Number from file of size length bytes read in reverese.
	 */
	size_t getNumberFromFileRev(unsigned int length)
	{
		if (!index.good())
			return -1;

		if (length == 0)
			return -1;

		for(int i=length-1;i>=0;--i) {
			if (index.good() && index.tellg() >= startoffset) {
				num[i] = index.peek();
				index.seekg(-1, ios::cur);
			}
			else return -1;
		}
		return convertNumber(num,length);
	}

public:

	/** Constructor. */
	WikiTitleIndex() 
	{
		opened = false;
		multivolume = false;
	}

	/** Destructor. */
	~WikiTitleIndex() 
	{
		close();
	}

	/** Close opened file. */
	void close() 
	{
		closeF();
	}

	/** Open index file.
	 * @param indexFile Full path to index file.
	 * @return True if everything went ok.
	 */
	bool open(const wstring &filename) 
	{
		// open index file
		index.open(filename.c_str(),ios::binary | ios::in);
		if (!index.is_open())
			return false;

		// read index data
		char num[4];

		// read number of volumes
		index.read(num,1);
		multivolume = convertNumber(num,1) == 1;

		// read number of levels
		index.read(num, 1);
		levels = convertNumber(num, 1);

		// read offset of master index
		index.read(num, 4);
		size_t lastblockoffset = convertNumber(num, 4);

		// starting offset of index
		startoffset = index.tellg();

		// read last level of index (we will keep this in memory)
		IndexNode addr;
		addr.id.reserve(256);
		index.seekg(lastblockoffset);
		while(index.good()) {	
			getIdFromFile(addr.id);
			index.read(num,4);
			addr.offset = convertNumber(num,4);
			if (addr.id == levelend || addr.id.empty()) 
				break;
			ind.push_back(addr);
			addr.id.clear();
		}

		// set opened flag to true
		opened = true;
		return true;
	}

	/** Exact match request.
	 * @param req Request object (Request::id must be set to requested resource id).
	 * @return Address instance of requested resource. 
	 *         Address::valid is not set to true if returned Address object is invalid.
	 */
	Address exactMatch(Request &req) 
	{
		Address out;

		if (!opened)
			return out;

		const string &what = req.getId();
		if (what.empty())
			return out;

		index.clear(ios::goodbit);
		if (!index)
			return out;
		
		// find offset of lower bound of item with id "what" in index
		size_t offset = lowerBound(what);

		// seek to lower bound offset
		index.seekg(offset,ios::beg);

		if(!index.good())
			return out;
		
		// read item's id
		getIdFromFile(out.id,true);

		// if read id is not equal to "what" return (not valid adress)
		if (out.id == what) {
			// read item's volume (if multivolume)
			if (multivolume) 
				out.volume = getNumberFromFile(1);
			// read item's block
			out.block = getNumberFromFile(4);
			// read item's start offset
			out.start = getNumberFromFile(4);
			// read item's end offset
			out.end = getNumberFromFile(4);

			// set address to valid if it's valid
			out.valid = true;
			if (out.block == -1 || out.start == -1 || out.end == -1)
				out.valid = false;
			if (multivolume && out.volume == -1)
				out.valid = false;
		}

		return out;
	}

	/** Partial match request.
	 * @param req Request object (id must be set to requested resource id).
	 *            Optional parameters can be set through this object:
	 *               - matches - number of returned matches
	 *               - direction - have value "back" if we want to search backwards.
	 * @return Vector of Address instances that contains found resources.
	 */
	void partialMatch(Request &req, vector<Address> &out) 
	{
		// get parameters of search
		const string &what = req.getId();
		int n = StringUtils::toInt(req.getParam("matches"));
		bool back = req.getParam("direction") == "back";

		if (!opened || n < 1 || what.empty())
			return;

		index.clear(ios::goodbit);

		Address addr;
		string tmp;
		tmp.reserve(512);

	    // find offset of lower bound of item with id "what" in index
		size_t offset = lowerBound(what);
		if (offset == -1)
			return;

		if (!back) {
			// read n items starting from lower bound item
			int i = 0;

			index.seekg(offset,ios::beg);
			
			while(index.good() && i < n) {
				getIdFromFile(tmp,true);
				if (tmp == levelend)
					return;

				addr.id.swap(tmp);
				if (multivolume) 
					addr.volume = getNumberFromFile(1);
				addr.block = getNumberFromFile(4);
				addr.start = getNumberFromFile(4);
				addr.end = getNumberFromFile(4);

				addr.valid = true;
				if (addr.block == -1 || addr.start == -1 || addr.end == -1)
					addr.valid = false;
				if (multivolume && addr.volume == -1)
					addr.valid = false;

				if (addr.isValid())
					out.push_back(addr);
				++i;
			}
		}
		else {
			// read n items starting from lower bound item backwards
			int i = 0;

			// are we at the begining?
			if (offset == startoffset)
				return;

			if (offset > 0)
				index.seekg(offset,ios::beg);
			
			if (index.good()) {
				getIdFromFile(tmp,true);
				if (tmp != levelend) {
					addr.id.swap(tmp);
					if (multivolume) 
						addr.volume = getNumberFromFile(1);
					addr.block = getNumberFromFile(4);
					addr.start = getNumberFromFile(4);
					addr.end = getNumberFromFile(4);

					addr.valid = true;
					if (addr.block == -1 || addr.start == -1 || addr.end == -1)
						addr.valid = false;
					if (multivolume && addr.volume == -1)
						addr.valid = false;

					if (addr.isValid())
						out.push_back(addr);
				}
			}
			else return;

			if (offset > 0)
				index.seekg(offset-1,ios::beg);

			while(index.good() && index.tellg() >= startoffset && i < n-1) {
				addr.end = getNumberFromFileRev(4);
				addr.start = getNumberFromFileRev(4);
				addr.block = getNumberFromFileRev(4);
				if (multivolume)
					addr.volume = getNumberFromFileRev(1);

				getIdFromFileRev(tmp);

				if (tmp == levelend)
					return;

				addr.id.swap(tmp);
				addr.valid = true;
				if (addr.block == -1 || addr.start == -1 || addr.end == -1)
					addr.valid = false;
				if (multivolume && addr.volume == -1)
					addr.valid = false;

				if (addr.isValid())
					out.push_back(addr);
				++i;
			}
		}
		
	}
private:
	
	/** Find lower bound of item. 
	 * @param what Id of item for which we are finding lower bound.
	 * @return Offset of lower bound of item with id "what" in primary index.
	 */
	int lowerBound(const string &what) 
	{
		string tmp;
		size_t lastoffset;
		tmp.reserve(512);

		if (ind.size() < 1 )
			return -1;

		lastoffset = ind[ind.size()-1].offset;

		// search in memory (master index)
		for(size_t i=0;i<ind.size();++i) {
			if (ind[i].id < what) 
				continue;
			if (ind[i].id == what)
				lastoffset = ind[i].offset;
			else
				lastoffset = ind[i-1].offset;
			break;
		}

		// search in other index levels
		for(int i=0;i<levels-1;++i) {

			if (lastoffset == -1)
				return -1;

			// seek to offset which we found in last level of index
			index.seekg(lastoffset, ios::beg);
			lastoffset = -1;
			while(index.good()) {
				
				// read current item
				getIdFromFile(tmp);	

				if (tmp < what && tmp != levelend) {
					lastoffset = getNumberFromFile(4);
					continue;
				}

				// are we at end of block?
				if (tmp == levelend)
					return -1;

				// we found what we want
				if (tmp == what) 
					lastoffset = getNumberFromFile(4);

				break;
			}		
		}

		if (lastoffset == -1)
			return -1;

		index.seekg(lastoffset,ios::beg);

		// search in primary index
		while(index.good()) {
			lastoffset = index.tellg();
			getIdFromFile(tmp,true);
			if (tmp < what && tmp != levelend) {
				if (multivolume) 
					index.ignore(1);
				index.ignore(12);
				continue;
			}

			if (tmp == levelend)
				return -1;

			return lastoffset;
		}

		return -1;
	}
};

#endif

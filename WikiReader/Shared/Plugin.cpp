/**
 * @file
 * @brief This file contains implementation for Plugin.h.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include "Plugin.h"
#include "StringUtils.h"

#include <iostream>
#include <algorithm>

using namespace std;

/** Find if vector contains given data. */
template<class T> bool vectorContains(vector<T> &vect, const T &data) 
{
	vector<T>::iterator it = find(vect.begin(),vect.end(),data);
	if (it == vect.end())
		return false;
	return true;
}

/* Class Plugin. */
Plugin::Plugin() 
{
	hinstLib = NULL;
	loaded = false;
}

Plugin::Plugin(const std::wstring &filename) 
{
	loaded = loadPlugin(filename);
}

Plugin::~Plugin() 
{
	close();
}

bool Plugin::loadPlugin(const wstring &filename) 
{
	// Load DLL file
	hinstLib = LoadLibraryW(filename.c_str());
	if (hinstLib == NULL) 
		return false;

	// Get plugin ID
	GetIdFunction getId = (GetIdFunction)GetProcAddressW(hinstLib, L"getId");
	if (getId == NULL) {
		close();
		return false;
	}
	id = getId();

	// Get getInstance() function
	dllFactory = (DllFactoryFunction)GetProcAddressW(hinstLib, L"getInstance");
	if (dllFactory == NULL) {
		close();
		return false;
	}

	// Get available services IDs and make them case insensitive
	GetServicesFunction getServices = (GetServicesFunction)GetProcAddressW(hinstLib,L"getServices");
	if (getServices == NULL) {
		close();
		return false;
	}
	string str = getServices();
	StringUtils::toLower(str);
	StringUtils::splitString(str,";",services);

	loaded = true;
	return true;
}

std::string Plugin::getId(const wstring &filename) 
{
	// Load DLL file
	HINSTANCE hinstLib = LoadLibraryW(filename.c_str());
	if (hinstLib == NULL) 
		return "";

	// Get plugin ID
	GetIdFunction getId = (GetIdFunction)GetProcAddressW(hinstLib, L"getId");
	if (getId == NULL) {
		FreeLibrary(hinstLib);
		return "";
	}
	string id = getId();

	// Close DLL library
	FreeLibrary(hinstLib);

	return id;
}

const std::string& Plugin::getId() const 
{
	return this->id;
}

bool Plugin::isLoaded() const 
{
	return loaded;
}

ServiceBase* Plugin::getImplementation(const string &service) 
{
	if (!loaded)
		return NULL;

	// create instance of service and return pointer to it
	ServiceBase *base = dllFactory(service.c_str());
	return base;
}

bool Plugin::containsService(const string &service) 
{
	if (!loaded)
		return NULL;

	string tmp = service;
	return vectorContains<string>(services,StringUtils::toLower(tmp));
}

vector<string> Plugin::getServices() const 
{
	return services;
}

void Plugin::close() 
{
	loaded = false;
	services.clear();

	FreeLibrary(hinstLib);
	hinstLib = NULL;

	id.clear();
}

/* class PluginManager */

// create global instance
PluginManager *PluginManager::_instance = new PluginManager();

PluginManager* PluginManager::instance() 
{
	if (_instance == NULL) 
		_instance = new PluginManager();

	return _instance;
}

void PluginManager::deleteInstance() 
{
	if (_instance != NULL)
		delete _instance;
}

bool PluginManager::plugRefContains(const string &key) 
{
	return mapContains<string,pair<int,Plugin*>>(plugRef,key);
}

bool PluginManager::registerService(const string &name, const wstring &filename, const string &service) 
{
	// find if given service name isn't already registered -> unregister it
	if (mapContains<std::string,ServiceInfo*>(plugins,name)) {
		unregisterService(name);
	}

	// get plugin name
	string libid = Plugin::getId(filename);
	if (libid.empty())
		return false;

	// find if plugRef contains plugin with ID libid
	if (!plugRefContains(libid)) {
		// it does not contain plugin with name libid => load new plugin
		Plugin *plug = new Plugin(filename);

		// if plugin does not contain service with ID service return false
		if (!plug->isLoaded() || !plug->containsService(service)) { 
			delete plug;
			return false;
		}
		if (_getPlug(name) != NULL) {
			delete plug;
			return false;
		}

		// save service registration info
		ServiceInfo *svc = new ServiceInfo;
		svc->plugin = plug;
		svc->service = service;
		plugins[name] = svc;

		// add plugin to plugin reference count structure
		pair<int,Plugin*> p;
		p.first = 1;		// reference count set to 1 (only 1 registration use this plugin)
		p.second = plug;
		plugRef[libid] = p; 

		return true;
	}

	pair<int,Plugin*> p = plugRef[libid];
	// find if plugin contains service with ID service
	if (!p.second->containsService(service))
		return false;

	// increment plugin reference count
	p.first = p.first+1;
	plugRef[libid] = p;

	// save service registration info
	ServiceInfo *svc = new ServiceInfo;
	svc->plugin = p.second;
	svc->service = service;

	// save updated data to plugRef structure
	plugins[name] = svc;

	return true;
}

void PluginManager::unregisterService(const std::string &name) 
{
	// retrieve pointer to plugin with given service
	Plugin *plug = _getPlug(name);
	if (plug == NULL) 
		return;

	// decrement reference count for this plugin
	string libid = plug->getId();
	pair<int,Plugin*> p = plugRef[libid];
	p.first--;

	// remove info about service
	delete plugins[name];
	plugins.erase(name);

	if (p.first <= 0) {
		// reference count for this plugin is 0 => close this plugin
		if (p.second != NULL)
			delete p.second;
		plugRef.erase(libid);
		return;
	}

	// save updated data to plugRef structure
	plugRef[libid] = p;
}

Plugin* PluginManager::_getPlug(const std::string &name) 
{
	ServiceInfo *svc = getServiceInfo(name);
	if (svc == NULL)
		return NULL;

	return svc->plugin;
}

ServiceInfo* PluginManager::getServiceInfo(const std::string &name) 
{
	map<string,ServiceInfo*>::iterator it = plugins.lower_bound(name);

	if (it == plugins.end() || it->first != name)
		return NULL;

	return plugins[name];
}

ServiceBase* PluginManager::getService(const std::string &service) 
{
	// find if service is registered
	ServiceInfo *svc = getServiceInfo(service);
	if (svc == NULL)
		return NULL;

	// request service instance from Plugin class
	return svc->plugin->getImplementation(svc->service);
}

vector<string> PluginManager::getRegisteredServices()
{
	map<string,ServiceInfo*>::iterator it;
	vector<string> out;

	for(it = plugins.begin();it != plugins.end();++it) {
		if (it->second != NULL)
			out.push_back(it->first);
	}

	return out;
}

PluginManager::~PluginManager() 
{
	map<string,ServiceInfo*>::iterator it;
	for(it = plugins.begin();it != plugins.end();++it) {
		if (it->second != NULL) {
			if (it->second->plugin != NULL)
				delete it->second->plugin;
			delete it->second;
		}
	}
}

/*
vector<string> PluginManager::getPluginsByInterface(const std::string &interf) {
	map<string,Plugin*>::iterator it;
	vector<string> out;
	for(plugins.begin();it != plugins.end();++it) {
		if (it->second != NULL && it->second->implementsInterface(interf))
			out.push_back(it->first);
	}
	return out;
}
*/
#ifndef STRINGUTILITIES_H
#define STRINGUTILITIES_H

/**
 * @file
 * @brief This file contains string manipulation functions.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#ifdef DLL_EXPORT
	#define DLLAPI __declspec(dllexport)
#else
	#define DLLAPI __declspec(dllimport)
#endif

#pragma warning (disable:4251)

#include <string>
#include <vector>

/// Path separator used in current OS.
#define PATH_SEPARATOR L'\\'

/** Class encapsulating various string related utilities. */
class DLLAPI StringUtils 
{
public:
	/** Replace all occurences of string.
	 * @param in Input string.
	 * @param search_for String we are searching for.
	 * @param repace_with What we are replacing searched string for.
	 * @return String in which has all occurences of substring search_for replaced with replace_with.
	 * @author Ren� Nyffenegger (rene.nyffenegger@adp-gmbh.ch)
	 */
	static std::string replaceInStr(const std::string &in, 
							 const std::string &search_for, 
							 const std::string &replace_with);

	/** This method is similiar to replaceInStr() but replaces only one occurence of search_for. */
	static std::string replaceInStrOne(const std::string &in, 
		const std::string &search_for, 
		const std::string &replace_with);

	/** Split strings.
	 * @param str Input string.
	 * @param delim String used as delimiter.
	 * @param results Vector in which results are saved.
	 */
	static void splitString(const std::string &str, 
					 const std::string &delim, 
					 std::vector<std::string> &results);

	/** Convert string to upper case.
	 * @param s Input string.
	 * @return Upper cased string.
	 */
	static std::string& toUpper(std::string &s);

	/** Convert string to lower case.
	 * @param s Input string.
	 * @return Lower cased string.
	 */
	static std::string& toLower(std::string &s);

	/** Remove whitespaces from start and end
	 * @param s Input string.
	 * @return Trimmed string.
	 */
	static std::string& trim(std::string &s);

	/** UTF8 to UTF16 conversion
	 * @param s Input string.
	 * @return Wide string in UTF16 encoding.
	 */
	static std::wstring utf8To16(const std::string &s);

	/** UTF16 to UTF8 conversion
	 * @param s Input wide string.
	 * @return String in UTF8 encoding.
	 */
	static std::string utf16To8(const std::wstring &s);
	
	/** string to long conversion.
	 * @param s Input string.
	 * @return Long value of string s or 0.
	 */
	static long toLong(const std::string &s);

	/** long to string conversion.
	 * @param l Input long value.
	 * @return String representing number l.
	 */
	static std::string fromLong(long l);

	/** long to wide string conversion.
	 * @param l Input long value.
	 * @return Wide string representing number l.
	 */
	static std::wstring fromLongW(long l);

	/** string to int conversion.
	 * @param s Input string.
	 * @return Integer value of string s or 0.
	 */
	static int toInt(const std::string &s);

	/** int to string conversion.
	 * @param i Input int value.
	 * @return String representing number i.
	 */
	static std::string fromInt(int i);

	/** int to wide string conversion.
	 * @param i Input int value.
	 * @return Wide string representing number i.
	 */
	static std::wstring fromIntW(int i);


	/** Test if string stars with other string.
	 * @param what Input string.
	 * @param with Starting prefix.
	 * @return True if what starts with prefix with.
	 */
	static bool startsWith(const std::string &what, const std::string &with);

	/** Url encode string.
	 * @param str Input string.
	 * @return Url encoded string. 
	 */
	static std::string encodeUrl(const std::string &str);

	/** 
	 * @param src Path relative to cwd.
	 * @param cwd Working directory.
	 * @return Full path with respect to parameter cwd.
	 */
	static std::wstring &getFullPath(std::wstring &src, const std::wstring &cwd);

	/** Same as getFullPath(std::wstring&, const std::wstring&) buf cwd is set to path to WikiReader.exe */
	static std::wstring &getFullPath(std::wstring &src);
};

/** @return True if c is legal URL character */
inline bool isurlchar(char c) 
{
	return (c >= 0x30 && c <= 0x39) || (c >= 0x41 && c <= 0x5A) 
		|| (c >= 0x61 && c <= 0x7A) || (c == 0x5F) || (c == 0x2E);
}

#endif
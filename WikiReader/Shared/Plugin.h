#ifndef WIKIREADER_PLUGIN_H
#define WIKIREADER_PLUGIN_H

/**
 * @file
 * @brief This file contains declaration of Plugin and PluginManager classes.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#ifdef DLL_EXPORT
	#define DLLAPI __declspec(dllexport)
#else
	#define DLLAPI __declspec(dllimport)
#endif
#pragma warning (disable:4251)

#include <windows.h>
#include <string>
#include <vector>
#include <map>
#include <set>
#include "../Shared/Types.h"

/** Class representing plugin. */
class DLLAPI Plugin {
private:
	
	/// Typedef for pointer to getInstance() function. 
	/// Purpose of this function is creation of services that plugin file contains.
	typedef ServiceBase* (*DllFactoryFunction)(const char *);
	/// Typedef for pointer to getServices() function. 
	/// This function is used to retrieve list of service ID's that plugin file contains.
	typedef char* (*GetServicesFunction)();				 
	/// Typedef for pointer to getId() function. 
	/// This function is used to retrieve unicate id of plugin file.
	typedef char* (*GetIdFunction)();

	/// Handle to dll library.
	HINSTANCE hinstLib;
	/// Pointer to plugin getInstance() function.
	DllFactoryFunction dllFactory;
	/// List of plugin service ID's.
	std::vector<std::string> services;
	/// Indicates if plugin file is loaded.
	bool loaded;
	/// Unicate ID of plugin file.
	std::string id;

public:

	/** Constructor. */
	Plugin();

	/** Constructor that loads plugin dll file.
	 * @param filename Full path to plugin file.
	 */
	Plugin(const std::wstring &filename);

	/** Destructor. */
	~Plugin();

	/** Load plugin dll file - retrieve plugin services and ID.
	 * @param filename Full path to plugin file.
	 * @return True if everything went ok.
	 */
	bool loadPlugin(const std::wstring &filename);

	/** @return True if plugin file is loaded. */
	bool isLoaded() const;

	/** 
	 * @param filename Full path to plugin file.
	 * @return ID of plugin file given by path filename.
	 */
	static std::string getId(const std::wstring &filename);

	/** @return Plugin file ID. */
	const std::string& getId() const; 

	/** Create instance of service that plugin contains. 
	 * @param service ID of service which we want to create.
	 * @return Pointer to created instance of service with ID service. Or NULL if error occurs 
	 *		   (service ID is not supported ...).
	 */
	ServiceBase* getImplementation(const std::string &service);

	/** 
	 * @param service ID of service.
	 * @return True if service with ID service exists in plugin file. 
	 */
	bool containsService(const std::string &service);

	/** @return List of available services. */
	std::vector<std::string> getServices() const;

	/** Close plugin dll file. */
	void close();
};

/** Structure that holds info about service specification. */
struct ServiceInfo {
	/// Service ID in plugin.
	std::string service;
	/// Pointer to instance of plugin class that contains service with ID service.
	Plugin* plugin;
};

/** Singleton class PluginManager. This class is used to register plugins
 *  and their services and to retrieve instances of these services. */
class DLLAPI PluginManager {
private:

	/// Typedef for reference count structure (we don't want to load same plugin for every 
	/// service if plugin contains many services). Key of this map is plugin ID. 
	/// Integer value indicate number of references to given plugin.
	typedef std::map<std::string,std::pair<int,Plugin*>> PluginRefCount;

	/// Table of registered service names and their ServiceInfo structures.
	std::map<std::string,ServiceInfo*> plugins;
	/// Reference count structure.
	PluginRefCount plugRef;

	/// Global instance of PluginManager.
	static PluginManager *_instance;

	/** Find if reference count structure contains plugin with given id.
	 * @param in Plugin ID.
	 * @return True if plugRef contains plugin with given id.
	 */
	bool plugRefContains(const std::string &id);

	/**
	 * @param name Registered service name.
	 * @return Pointer to Plugin object that is registered to service with given name. 
			   Returns NULL if requested plugin does not exist.
	 */
	Plugin *_getPlug(const std::string &name);

	/**
	 * @param name Registered service name.
	 * @return Pointer to ServiceInfo object that contains specification for service with given name.
			   Return NULL if requested object does not exist.
	 */
	ServiceInfo* getServiceInfo(const std::string &name);

protected:
	
	/** Constructor. - protected because this class is singleton. */
	PluginManager() {};

	/** Destructor. */
	~PluginManager();

public:
	
	/** @return Pointer to global PluginManager instance. */
	static PluginManager *instance();

	/** Delete global instance. */
	static void deleteInstance();

	/** Register service in plugin manager.
	 * @param name Service name. This will be the service identifier in whole program.
	 * @param filename Full path to plugin dll file.
	 * @param service Service ID in plugin file.
	 * @return True if everything went ok.
	 */
	bool registerService(const std::string &name, const std::wstring &filename, const std::string &service);

	/** Unregister service with given name. 
	 * @param name Registered service name.
	 */
	void unregisterService(const std::string &name);
	
	/** Retrieve implementation of registered service. 
	 * @param name Registered name of requested service.
	 * @return Pointer to instance of requested service.
	 */
	ServiceBase* getService(const std::string &name);

	/* @return List of names of registered services. */
	std::vector<std::string> getRegisteredServices();
};
#endif

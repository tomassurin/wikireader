#ifndef WIKIREADER_TYPES_H
#define WIKIREADER_TYPES_H

/**
 * @file
 * @brief This file contains definitions of basic types used in program.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include <string>
#include <vector>
#include <map>
#include "Preferences.h"
#include "Request.h"

#ifdef DLL_EXPORT
#define DLLAPI __declspec(dllexport)
#else
#define DLLAPI __declspec(dllimport)
#endif

/// Byte type definition.
typedef unsigned char Byte;

class ByteBuffer;

/** Class is used to store information about position of resource in compressed archive. */
class Address 
{
public:

	/** Constructor. */
	Address() 
	{
		clear();
	}

	/** Initialize object to its default state. */
	void clear() 
	{
		id.clear();
		volume = 0;
		block = -1;
		start = -1;
		end = -1;
		valid = false;
	}

	/** Find if this Address object is valid (valid field was set).
	 * @return True if valid field is set.
	 */
	bool isValid() const 
	{
		return valid;
	}

	/// Resource identifier (e.g. wiki article name).
	std::string id;
	/// Identifies if address is valid.
	bool valid;
	/// Volume of archive that contains article.
	size_t volume;
	/// Block in archive that contains article.
	size_t block;
	/// Start offset of article in block.
	size_t start;
	/// End offset of article in block.
	size_t end;
};

/** Definition of services interfaces. */

	/** Base service interface. */
	class ServiceBase 
	{
	public:

		/** Virtual destructor. */
		virtual ~ServiceBase() {};

		/** Release this object - used to release object (to handle various memory allocation methods). */
		virtual void release() { delete this; }

		/** Release of memory denoted by ptr. */
		virtual void releaseResource(void *ptr) { };

		/** Pass pointer of parent service to this service.
		 * @param plug Pointer to service.
		 * @param name Some kind of service identification (implementation dependend).
		 */
		virtual void addService(ServiceBase *plug, const std::string &name) { };

		/** Set service preference.
		 * @param name Name of preference.
		 * @param data Value of preference.
		 */
		virtual void setPreference(const std::string &name, const std::string &data) { };

		/** Same as setPreference() but accepts unicode data. */
		virtual void setPreferenceW(const std::string &name, const std::wstring &data) { };
	};    

	/** Decompression service interface. Used to decompress data from archives.*/
	class ServiceDecompress : public ServiceBase 
	{
	public:
	   
	   /** Opens compressed file.
	    * @param archFile Full path to compressed file.
		  * @return True if everything went ok.
		  */
	   virtual bool open(const std::wstring &archFile) = 0;

	   /** Close opened file. */
	   virtual void close() = 0;

	   /** Extracts block part from compressed file. 
	    * @param block Logical number of block to decompress.
		  * @param start Start offset (in block) from which data is returned.
		  * @param end End offset (in block) to which data is returned
		  * @param outdata ByteBuffer which will be filled with wanted data.
		  * @return True if everything went ok.
		  */
	   virtual bool extract(size_t block, size_t start, size_t end, ByteBuffer &outdata) = 0;
	   
	   /** Extract resource from compresed file.
	    * @param id Identifier of resource in compressed file.
		  * @param start Start offset (of resource) from which data is returned.
		  * @param end End offset (of resource) to which data is returned
		  * @param outdata ByteBuffer which will be filled with wanted data.
		  * @return True if everything went ok.
		  */
	   virtual bool extract(const std::string &id, size_t start, size_t end, ByteBuffer &outdata) { return false;};
	};

	/** Index service interface. Used to retrieve Address structure to various requests.*/
	class ServiceIndex : public ServiceBase 
	{
	public:

		/** Open index file.
		 * @param indexFile Full path to index file.
		 * @return True if everything went ok.
		 */
		virtual bool open(const std::wstring &indexFile) = 0;

		/** Close opened file. */
		virtual void close() = 0;

		/** Exact match request.
		 * @param req Request object (Request::id must be set to requested resource id; 
		 *            also optional parameters can be set through this object).
		 * @return Address instance of requested resource. 
		 *         Address::valid is not set to true if returned Address object is invalid.
		 */
		virtual Address exactMatch(Request &req) = 0;

		/** Partial match request.
		 * @param req Request object (Request::id must be set to requested resource id; 
		 *            also optional parameters can be set through this object).
		 * @return Vector of Address instances that contains found resources.
		 */
		virtual void partialMatch(Request &req, std::vector<Address> &out) = 0;
	};

	/** File format service interface. Used to access data files. */
	class ServiceFileFormat : public ServiceBase 
	{
	public:

		/** Open data file.
		 * @param filename Full path to data file.
		 * @return True if everything went ok.
		 */
		virtual bool open(const std::wstring &filename) = 0;

		/** Close opened file. */
		virtual void close() = 0;

		/** Process request.
		 * @param req Request object (id must be set to requested resource id; 
		 *            also optional parameters can be set through this object).
		 * @return True if everything went ok.
		 */
		virtual bool process(Request &req) = 0;

		/** Retrieve resource data.
		 * @param id Resource id.
		 * @param outdata ByteBuffer which will be filled with wanted data.
		 * @return True if everything went ok.
		 */
		virtual bool getData(const std::string &id, ByteBuffer &outdata) { return false; };

		/** Get pointer to Preferences object which contains configuration of data file. 
		 * @return NULL if Preferences object can't be returned or this method is not implemented.
		 */
		virtual Preferences* getPreferences() { return NULL; };

		/** Get some information about data file (this information is show on Home page).
		 * @return String with information about data file.
		 */
		virtual std::string getInfo() { return ""; };
	};

	/** Content Plugin interface. Used to convert resource data to other formats.*/
	class ServiceContent : public ServiceBase 
	{
	public:
		
		/** Convert data from input and write data with calls to Request::writeChunk().
		 * @param input ByteBuffer with input data.
		 * @param req Request object which is used to write data to output.
		 * @return True if everything went ok.
		 */
		virtual bool convert(const ByteBuffer &input, Request &req) = 0;
	};

/// This definition is used to export symbols from plugin dll files.
#define DLLEXPORT extern "C" __declspec(dllexport)

/** Class used for transporting large amounts of data. */
class ByteBuffer 
{
public:

	/// Pointer to allocated memory block.
	Byte *data;
	/// Length of allocated memory block.
	size_t length;
	/// Pointer to service that allocated this buffer.
	ServiceBase *allocBy;
	/// Offset of our data start.
	size_t startOffset;

	/** Constructor. */
	ByteBuffer() 
	{
		data = NULL;
		allocBy = NULL;
		length = 0;
		startOffset = 0;
	}

	/** Copy constructor. */
	ByteBuffer(const ByteBuffer &buf) 
	{
		operator=(buf);
	}

	/** Assignment. */
	ByteBuffer& operator=(const ByteBuffer &buf) 
	{
		data = buf.data;
		allocBy = buf.allocBy;
		length = buf.length;
		startOffset = buf.startOffset;
		return *this;
	}
	
	/** Find if this buffer is empty.
	 * @return True if this buffer is empty, false otherwise.
	 */
	bool isEmpty() 
	{
		if (length == 0)
			return true;

		return false;
	}

	/** Release this buffer. */
	void releaseBuffer() 
	{
		if (data == NULL)
			return;

		if (allocBy == NULL)
			delete data;
		else {
			// release buffer by other service
			allocBy->releaseResource(data);
		}

		length = 0;
		startOffset = 0;
		data = NULL;
	}
};

#endif

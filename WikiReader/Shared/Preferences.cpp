/**
 * @file
 * @brief This file contains implementation for Preferences.h.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include "Preferences.h"

#include <fstream>
#include <algorithm>
#include <string>
#include <windows.h>
#include <fstream>

#include "StringUtils.h"
#include "wce/wce_time.h"

using namespace std;

/// Path to log file.
static const char *logfilepath = "WikiReader.log";

static wstring getExePath() 
{
	LPWSTR szPath = new TCHAR[MAX_PATH];
	GetModuleFileName((HMODULE)GetCurrentProcessId(),szPath,MAX_PATH);
	wstring path = szPath;
	delete szPath;
	wstring::size_type sz = path.find_last_of(L"\\");
	path = path.substr(0,sz) +L"\\";
	return path;
}

/** Find if input map contains element key. */
template<class K,class T>
bool mapContains(const std::map<K,T> &input, const K &key) 
{
	map<K,T>::const_iterator it = input.lower_bound(key);
	if (it == input.end() || it->first != key )
		return false;
	return true;
}

/// Pointer to global instance of Preferences class. 
/// This instance can be accessed from whole program.
/// It allows access to main program configuration file.
Preferences *_globalInstance = NULL;

// Some definitions.
const std::string Preferences::error = "PREFERENCE_NOT_FOUND";
const std::wstring Preferences::werror = L"PREFERENCE_NOT_FOUND";
const std::wstring Preferences::path = getExePath();

Preferences::Preferences() : hRoot(0), loaded(false) 
{

}

Preferences* Preferences::instance() 
{
	if (_globalInstance == NULL) {
		// clear log file contents
		ofstream logfile;
		logfile.open(logfilepath, ios::out | ios::trunc);
		logfile.close();

		// create new instance
		_globalInstance = new Preferences();
	}
	return _globalInstance;
}

bool Preferences::loadPreferences(const wstring &filename) 
{
	loaded = false;

	// convert filename to ordinary string (TinyXml does not support unicode loadFile function).
	string file = StringUtils::utf16To8(filename);
	
	// load configuration file
	if (!doc.LoadFile(file))
		return false;

	// get handle to root element
	TiXmlElement *pElem = doc.RootElement();
	if (pElem == NULL)
		return false;
	hRoot = TiXmlHandle(pElem);
	loaded = true;

	return true;
}

Preferences::~Preferences()
{

}

const std::wstring &Preferences::exePath() 
{
	return path;
}

string Preferences::getString(const string &key) 
{
	if (!loaded)
		return error;

	if (key.empty())
		return error;

	if (key[0] == localId) {
		// local preference
		if (mapContains<string,string>(data, key)) {
			return data[key];
		}
		else return error;
	}
	
	// preference in config file
	vector<string> parts;
	StringUtils::splitString(key,"/",parts);
	TiXmlHandle tmp(hRoot);
	// go through xml tree until we find our wanted preference
	for(size_t i=0;i<parts.size();++i) 
		tmp = tmp.FirstChild(parts[i]);

	// get current element
	TiXmlElement* pElem = tmp.ToElement();
	if (!pElem) 
		return error;

	// get text of current element
	const char *out = pElem->GetText();
	if (!out) 
		return "";

	return out;
}

wstring Preferences::getStringW(const string &key) 
{
	return StringUtils::utf8To16(getString(key));
}

long Preferences::getInteger(const string &key) 
{
	return StringUtils::toLong(getString(key));
}

bool Preferences::getBool(const string &key) 
{
	string s = getString(key);
	if (StringUtils::toLower(s) == "true")
		return true;
	return false;
}

void Preferences::getSectionElements(const string &key, vector<string> &elems) 
{
	elems.clear();

	if (!loaded)
		return;

	vector<string> parts;
	StringUtils::splitString(key,"/",parts);

	// go through xml tree until we find our wanted preference
	TiXmlHandle tmp(hRoot);
	for(size_t i=0;i<parts.size();++i) 
		tmp = tmp.FirstChild(parts[i]);

	// get first child of current element
	TiXmlElement* pElem = tmp.FirstChildElement().ToElement();
	if (!pElem)
		return;

	// find all siblings of pElem and save them to elems vector
	for( pElem; pElem; pElem = pElem->NextSiblingElement() ) 
		elems.push_back(pElem->ValueStr());
}

void Preferences::setString(const std::string &key, const std::string &val) 
{
	if (key.empty() || key[0] != localId)
		return;

	data[key] = val;
}
void Preferences::setStringW(const std::string &key, const std::wstring &val) 
{
	if (key.empty() || key[0] != localId)
		return;

	data[key] = StringUtils::utf16To8(val);
}

void Preferences::setInteger(const std::string &key, long val)
{
	if (key.empty() || key[0] != localId)
		return;

	data[key] = StringUtils::fromLong(val);
}

void Preferences::setBool(const std::string &key, bool val) 
{
	if (key.empty() || key[0] != localId)
		return;

	data[key] = val == true? "true": "false";
}

void Preferences::appendToLog(const std::string &str)
{
	// open log file
	std::ofstream logfile;
	logfile.open(logfilepath, ios::out | ios::app);
	
	// create current time
	time_t ltime;
	wceex_time(&ltime);
	tm* local = wceex_localtime(&ltime);
	char* asctime_remove_nl = wceex_asctime(local);
	asctime_remove_nl[24] = 0;

	// append data to log
	logfile << asctime_remove_nl << "\t" << str << "\n";

	// close log
	logfile.close();
}
#ifndef WIKIREADER_PREFERENCES_H
#define WIKIREADER_PREFERENCES_H

/**
 * @file
 * @brief This file contains declaration of Preferences class.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#ifdef DLL_EXPORT
	#define DLLAPI __declspec(dllexport)
#else
	#define DLLAPI __declspec(dllimport)
#endif

#pragma warning (disable:4251)

#include <string>
#include <vector>
#include <map>
#include "TinyXML/tinyxml.h"

/// Prefix local (ie. not in config file) preference id.
const static char localId = ':';

/* Class for storing program preferences. */
class DLLAPI Preferences 
{
private:

	/// XML document class.
	TiXmlDocument doc;
	/// Handle to root element of XML document.
	TiXmlHandle hRoot;
	/// Indicate if this object is initialized ok (if config file is loaded).
	bool loaded;
	/// Path to main exe file.
	static const std::wstring path;
	/// Local preferences storage (user must have set this). 
	std::map<std::string,std::string> data;

public:

	/** Constructor. */
	Preferences();

	/** Destructor. */
	~Preferences();

	/** This class is not singleton because we want to create instance of this class for every data file.
	 * @return Pointer to global Preferences instance. 
	 */
	static Preferences *instance();

	/// Error constant. This value is returned if requested string preference is not found.
	static const std::string error;
	/// Wide string error constant. This value is returned if requested wide string preference is not found.
	static const std::wstring werror;

	/** @return Path to main exe file. */
	static const std::wstring &exePath();

	/** Initialize preferences object -> Load preferences from configuration file.
	 * @param file Full path to configuration file.
	 * @return True if everything went ok.
	 */
	bool loadPreferences(const std::wstring &file);

	/** Append string to application log.
	 * @param str String to append.
	 */
	static void appendToLog(const std::string &str);

	/** Get string value of preference.
	 * @param key Preference id.
	 * @return String value of preference with id key or 
	           Preferences::error if preference does not exist. 
	 */
	std::string getString(const std::string &key);

	/** Get wide string value of preference.
	 * @param key Preference id.
	 * @return Wide string value of preference with id key or 
			   Preferences::werror if preference does not exist. 
	 */
	std::wstring getStringW(const std::string &key);

	/** Get long value of preference.
	 * @param key Preference id.
	 * @return Long value of preference with id key or 0 if 
	           preference does not exist. 
	 */
	long getInteger(const std::string &key);

	/** Get boolean value of preference.
	 * @param key Preference id.
	 * @return Boolean value of preference with id key or false if 
	           preference does not exist. 
	 */
	bool getBool(const std::string &key);

	/** Get ids of preferences in preferences section 
	 *  (child elements that key element contains).
	 * @param key Id of requested preferences section.
	 * @param elems Vector in which results will be returned.
	 */
	void getSectionElements(const std::string &key, std::vector<std::string>& elems);

	/** Set local string preference.
	 * @param key Preference key. Must start with localId character.
	 * @param val Preference value.
	 */
	void setString(const std::string &key, const std::string &val);

	/** Set local wide string preference.
	 * @param key Preference key. Must start with localId character.
	 * @param val Preference value.
	 */
	void setStringW(const std::string &key, const std::wstring &val);

	/** Set local long preference.
	 * @param key Preference key. Must start with localId character.
	 * @param val Preference value.
	 */
	void setInteger(const std::string &key, long val);

	/** Set local boolean preference.
	 * @param key Preference key. Must start with localId character.
	 * @param val Preference value.
	 */
	void setBool(const std::string &key, bool val);
};

#endif
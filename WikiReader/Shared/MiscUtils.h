#ifndef MiscUtils_h__
#define MiscUtils_h__

/**
 * @file
 * @brief This file contains some utility functions.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#ifdef DLL_EXPORT
#define DLLAPI __declspec(dllexport)
#else
#define DLLAPI __declspec(dllimport)
#endif

#pragma warning (disable:4251)

#include <string>

/** Read file into string which serves as buffer.
 * @param path Full path to file.
 * @param str Output buffer in which read data are stored.
 * @return True if everything went ok.
 */
bool DLLAPI ReadFile(const std::wstring &path, std::string &str);

#endif

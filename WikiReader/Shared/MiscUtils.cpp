/**
 * @file
 * @brief This file contains implementation for MiscUtils.h.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include "MiscUtils.h"

#include <fstream>

bool ReadFile(const std::wstring &path, std::string &str) 
{
	ifstream input;
	input.open(path.c_str());
	if (!input)
		return false;

	input.seekg(0,ios::end);
	size_t size = input.tellg();
	input.seekg(0,ios::beg);

	char *buf = new char[4096];
	while(input.good()) {
		input.read(buf,4096);
		str.append(buf, input.gcount());
	}

	input.close();
	delete buf;
	return true;
}
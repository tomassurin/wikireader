#ifndef REQUEST_H
#define REQUEST_H

/**
 * @file
 * @brief This file contains implementation of class Request.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include <string>
#include <algorithm>

#pragma warning( disable: 4251 )

#ifdef DLL_EXPORT
#define DLLAPI __declspec(dllexport)
#else
#define DLLAPI __declspec(dllimport)
#endif

const std::string empty = "";

/** Interface for classes representing requests. All methods are optional. */
class DLLAPI Request {
public:

	/** Constructor. */
	Request() {}
	/** Consturtor. Added for convenience. */
	Request(const std::string &s) : id(s) {	}

	/** Writes part of response to client (implemented in childs of this class - source of request, string ...).
	 * @param ch String to write.
	 * @return True if everything went ok.
	 */
	virtual bool writeChunk(const std::string &ch) { return false; }

	/** Same as writeChunk(const std::string&).
	 * @param buf Buffer of characters.
	 * @param length Length of input buffer.
	 * @return True if everything went ok.
	 */
	virtual bool writeChunk(char *buf, size_t length) { return false; }

	/** Writes start of response to client.
	 * @param status Status code of response.
	 * @return True if everything went ok.
	 */
	virtual bool startResponse(int status) { return false;}

	/** Add header to processing.
	 * @param header Input header.
	 */
	virtual void addHeader(const std::string &header) { return; } ;

	/** Set identifier of requested resource.
	 * @param st Requested resource id.
	 */
	virtual void setId(const std::string &st)
	{
		id = st;
	}
	
	/** @return Identifier of requested resource. */
	virtual const std::string &getId() const
	{
		return id;
	}

	/** @return Parameter with name "name".
	 * @param name Name of wanted parameter. 
	 */
	virtual const std::string &getParam(const std::string &name) { return empty; }

	/** Set parameter of request.
	 * @param name Name of parameter.
	 * @param val Value of parameter.
	 */
	virtual void setParam(const std::string &name, const std::string &val) { }

private:
	std::string id;
};

#endif
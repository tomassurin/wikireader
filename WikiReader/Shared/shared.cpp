/**
 * @file
 * @brief This file includes many *.cpp files in module shared.dll. 
 *
 * This way exportation of symbols is simplified.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#define DLL_EXPORT

#pragma warning (disable:4251)

#include "wce/wce_time.c"
#include "wce/wce_ctime.c"
#include "wce/wce_gettimeofday.c"
#include "wce/wce_localtime.c"
#include "wce/wce_mktime.c"
#include "wce/wce_asctime.c"
#include "Preferences.cpp"
#include "StringUtils.cpp"
#include "MiscUtils.cpp"
#include "Plugin.cpp"
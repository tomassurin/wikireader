/**
 * @file
 * @brief This file contains implementation for StringUtils.h.
 *
 * This file is part of module Shared.dll.
 *
 * @author Tom� �ur�n
 * @version 1.0
 *
 */

#include "StringUtils.h"
#include "utf8.h"
#include <algorithm>
#include <cctype>
#include <sstream>

using namespace std;

wstring &StringUtils::getFullPath(std::wstring &src, const std::wstring &cwd)
{
	if (src.empty())
		return src;

	if (src[0] == PATH_SEPARATOR) {
		return src;
	}
	else if (src.size() > 1 && src[0] == L'.' && src[1] == PATH_SEPARATOR) {
		if (!cwd.empty() || cwd[cwd.size()-1] == PATH_SEPARATOR)
			src = cwd+src.substr(2);
		else
			src = cwd+PATH_SEPARATOR+src.substr(2);
	}
	else {
		if (!cwd.empty() && cwd[cwd.size()-1] == PATH_SEPARATOR) {
			src = cwd+src;
		}
		else 
			src = cwd+PATH_SEPARATOR+src;
	}
	return src;
}

wstring &StringUtils::getFullPath(std::wstring &src )
{
	return StringUtils::getFullPath(src,Preferences::instance()->exePath());
}

string StringUtils::replaceInStr(const string &in, const string &search_for, const string &replace_with) 
{
  string ret = in;

  string::size_type pos = ret.find(search_for);

  while (pos != string::npos) {
    ret = ret.replace(pos, search_for.size(), replace_with);

    pos =  pos - search_for.size() + replace_with.size() + 1;

    pos = ret.find(search_for, pos);
  }

  return ret;
}

string StringUtils::replaceInStrOne(const string &in, const string &search_for, const string &replace_with) 
{
	string ret = in;

	string::size_type pos = ret.find(search_for);

	if (pos != string::npos) {
		ret = ret.replace(pos, search_for.size(), replace_with);

		pos =  pos - search_for.size() + replace_with.size() + 1;

		pos = ret.find(search_for, pos);
	}

	return ret;
}

void StringUtils::splitString(const string &str, const string &delimiters, vector<string> &tokens)
{
    // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}

string& StringUtils::toUpper(string &s) {
  transform(s.begin(), s.end(), s.begin(), toupper);
  return s;
}

string& StringUtils::toLower(string &s) {
  transform(s.begin(), s.end(), s.begin(), tolower);
  return s;
}

static string whitespaces(" \t\n\v\f\r");

string& StringUtils::trim(string &str) 
{
	string::size_type start = str.find_first_not_of(whitespaces);
	if (start == str.npos) {
		str.clear();
		return str;
	}
	string::size_type end = str.size()-1;
	while( end != start && isspace(str[end]))
		--end;
	str = str.substr(start,end+1);
	return str;
}

long StringUtils::toLong(const std::string &s) 
{
	return atol(s.c_str());
}

string StringUtils::fromLong(long l) 
{
	ostringstream out;
	out << l;
	return out.str();
}

wstring StringUtils::fromLongW(long i)
{
	return utf8To16(fromLong(i));
}

int StringUtils::toInt(const std::string &s) 
{
	return atoi(s.c_str());
}

string StringUtils::fromInt(int i) 
{
	ostringstream out;
	out << i;
	return out.str();
}

wstring StringUtils::fromIntW(int i)
{
	return utf8To16(fromInt(i));
}

wstring StringUtils::utf8To16(const string &s) 
{
	wstring out;
	utf8::unchecked::utf8to16(s.begin(),s.end(),back_inserter(out));
	return out;
}

string StringUtils::utf16To8(const std::wstring &s) 
{
	string out;
	utf8::unchecked::utf16to8(s.begin(),s.end(),back_inserter(out));
	return out;
}

bool StringUtils::startsWith(const string &what, const string &with) 
{
	return what.substr(0,with.length()) == with;
}

std::string StringUtils::encodeUrl(const std::string &str) 
{
	std::string out;
	unsigned int c;
	unsigned char cc;
	char digit1;
	char digit2;
	for(size_t i=0;i < str.size();++i) {
		cc = (unsigned char) str[i];
		if (isurlchar(cc)) {
			// is url char -> output
			out.push_back(cc);
		}
		else {
			// not url char	-> encode into % form
			out.push_back('%');
			c = cc % 16;
			if (c < 10)
				digit2 = '0'+c;
			else {
				digit2 = 'A'+ c - 10;
			}
			c = (cc / 16) % 16;
			if (c < 10)
				digit1 = '0'+c;
			else {
				digit1 = 'A'+ c - 10;
			}
			out.push_back(digit1);
			out.push_back(digit2);
		}
	}
	return out;
}
/* LzmaUtil.c -- Test application for LZMA compression
2009-08-14 : Igor Pavlov : Public domain */

/* This modified version: Implementation of WZip format compression utility by Tom� �ur�n. */

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../Alloc.h"
#include "../../7zFile.h"
#include "../../LzmaDec.h"
#include "../../LzmaEnc.h"

const char *kCantReadMessage = "Can not read input file";
const char *kCantWriteMessage = "Can not write output file";
const char *kCantAllocateMessage = "Can not allocate memory";
const char *kDataErrorMessage = "Data error";
char rs[800] = { 0 };
const size_t t8 = sizeof(UInt64);
const size_t tt = sizeof(size_t);
const size_t t4 = sizeof(UInt32);
const size_t t2 = sizeof(UInt16);

#define UINT32_MAX 4294967296

static void *SzAlloc(void *p, size_t size) { p = p; return MyAlloc(size); }
static void SzFree(void *p, void *address) { p = p; MyFree(address); }
static ISzAlloc g_Alloc = { SzAlloc, SzFree };

int algo = 1;
int dictSize = 1 << 23;
int fb = 128;
int lc = 3;
int lp = 0;
int pb = 2;
int mt = 2;
int mc = 32;
int level = 5;

const UInt32 magic = 0x88022022;
const UInt16 majorVer = 1;
const UInt16 minorVer = 0;

void PrintHelp(char *buffer)
{
  strcat(buffer, "\nWikiZip - Simple wiki file compression format\n"
	  "\nUsage: \n"
	  "\twzip a [<switches>] archive filenames : add files to archive\n"
	  "\twzip e archive fileindex startbyte endbyte : extract filepart \n"
	  "<Switches>:\n"
	  "-a {N}:  set compression mode - [0, 1], default: 1 (max)\n"
	  "-d {N}:  set dictionary size - [12, 30], default: 23 (8MB)\n"
	  "-l (N):  set level of compression - [0, 9], default: 5 (normal)\n"
	  "-fb {N}: set number of fast bytes - [5, 273], default: 128\n"
	  "-mc {N}: set number of cycles for match finder\n"
	  "-lc {N}: set number of literal context bits - [0, 8], default: 3\n"
	  "-lp {N}: set number of literal pos bits - [0, 4], default: 0\n"
	  "-pb {N}: set number of pos bits - [0, 4], default: 2\n"
	  );
}

int PrintError(char *buffer, const char *message)
{
  strcat(buffer, "\nError: ");
  strcat(buffer, message);
  strcat(buffer, "\n");
  return 1;
}

void PrintErrorNoExit(char *buffer, const char *message)
{
  strcat(buffer, "\nError: ");
  strcat(buffer, message);
  strcat(buffer, "\n");
}

int PrintErrorNumber(char *buffer, SRes val)
{
  sprintf(buffer + strlen(buffer), "\nError code: %x\n", (unsigned)val);
  return 1;
}

int PrintUserError(char *buffer)
{
  return PrintError(buffer, "Incorrect command");
}

static WRes File_OpenEx(CSzFile *p, const char *name, int writeMode)
{
  p->handle = CreateFile(name,
	  writeMode == 1? GENERIC_WRITE | GENERIC_READ : GENERIC_READ,
	  writeMode == 1? FILE_SHARE_READ: 0, NULL,
	  writeMode == 1? OPEN_ALWAYS: OPEN_EXISTING,
      FILE_ATTRIBUTE_NORMAL, NULL);
  return (p->handle != INVALID_HANDLE_VALUE) ? 0 : GetLastError();
}
WRes InFile_OpenEx(CSzFile *p, const char *name) { return File_OpenEx(p, name, 0); }
WRes OutFile_OpenEx(CSzFile *p, const char *name) { return File_OpenEx(p, name, 1); }

#define IN_BUF_SIZE (1 << 16)
#define OUT_BUF_SIZE (1 << 16)

static SRes Encode(ISeqOutStream *outStream, ISeqInStream *inStream, UInt64 fileSize)
{
  CLzmaEncHandle enc;
  SRes res;
  CLzmaEncProps props;

  enc = LzmaEnc_Create(&g_Alloc);
  if (enc == 0)
    return SZ_ERROR_MEM;

  LzmaEncProps_Init(&props);
  props.algo = algo;
  props.fb = fb;
  props.lc = lc;
  props.lp = lp;
  props.level = level;
  props.mc = mc;
  props.numThreads = mt;
  props.pb = pb;
  props.dictSize = dictSize;
  res = LzmaEnc_SetProps(enc, &props);

  if (res == SZ_OK)
  {
    Byte header[LZMA_PROPS_SIZE + 8];
    size_t headerSize = LZMA_PROPS_SIZE;
    int i;

    res = LzmaEnc_WriteProperties(enc, header, &headerSize);
    for (i = 0; i < 8; i++)
      header[headerSize++] = (Byte)(fileSize >> (8 * i));
    if (outStream->Write(outStream, header, headerSize) != headerSize)
      res = SZ_ERROR_WRITE;
    else
    {
      if (res == SZ_OK)
        res = LzmaEnc_Encode(enc, outStream, inStream, NULL, &g_Alloc, &g_Alloc);
    }
  }
  LzmaEnc_Destroy(enc, &g_Alloc, &g_Alloc);
  return res;
}

static SRes Decode2(CLzmaDec *state, ISeqInStream *inStream,
    UInt64 unpackSize, Byte *buf, size_t bufsize)
{
  int thereIsSize = (unpackSize != (UInt64)(Int64)-1);
  Byte inBuf[IN_BUF_SIZE];
  Byte *outBuf = buf;
  size_t inPos = 0, inSize = 0, outPos = 0;
  LzmaDec_Init(state);
  for (;;)
  {
    if (inPos == inSize)
    {
      inSize = IN_BUF_SIZE;
      RINOK(inStream->Read(inStream, inBuf, &inSize));
      inPos = 0;
    }
    {
      SRes res;
      size_t inProcessed = inSize - inPos;
      size_t outProcessed = bufsize - outPos;
      ELzmaFinishMode finishMode = LZMA_FINISH_ANY;
      ELzmaStatus status;
      if (thereIsSize && outProcessed > unpackSize)
      {
        outProcessed = (SizeT)unpackSize;
        finishMode = LZMA_FINISH_END;
      }
      
      res = LzmaDec_DecodeToBuf(state, outBuf + outPos, &outProcessed,
        inBuf + inPos, &inProcessed, finishMode, &status);
      inPos += inProcessed;
      outPos += outProcessed;
      unpackSize -= outProcessed;
      
      //outPos = 0;
      
      if (res != SZ_OK || thereIsSize && unpackSize == 0)
        return res;
      
      if (inProcessed == 0 && outProcessed == 0)
      {
        if (thereIsSize || status != LZMA_STATUS_FINISHED_WITH_MARK)
          return SZ_ERROR_DATA;
        return res;
      }
    }
  }
}

static SRes Decode(ISeqInStream *inStream, Byte **data, size_t *bufsize, size_t *start, size_t *end)
{
  UInt64 unpackSize;
  int i;
  SRes res = 0;

  CLzmaDec state;

  /* header: 5 bytes of LZMA properties and 8 bytes of uncompressed size */
  unsigned char header[LZMA_PROPS_SIZE + 8];

  /* Read and parse header */
  RINOK(SeqInStream_Read(inStream, header, sizeof(header)));

  unpackSize = 0;
  for (i = 0; i < 8; i++)
    unpackSize += (UInt64)header[LZMA_PROPS_SIZE + i] << (i * 8);

  if (*start > unpackSize) 
	  return SZ_ERROR_PARAM;
  
  if (*end > unpackSize) 
	  *end = (size_t)unpackSize;

  *bufsize = (*end)*sizeof(Byte);
  *data = (Byte*) malloc(*bufsize);
  if (*data == NULL)
	  return SZ_ERROR_MEM;
  
  LzmaDec_Construct(&state);
  RINOK(LzmaDec_Allocate(&state, header, LZMA_PROPS_SIZE, &g_Alloc));
  res = Decode2(&state, inStream, *end, *data, *bufsize);
  LzmaDec_Free(&state, &g_Alloc);

  if (*start != 0) {
	  *bufsize = (*end-*start)*sizeof(Byte);
	  memmove(*data,*data+*start,*bufsize);
	  *data = (Byte*)realloc(*data,*bufsize);
	  if (*data == NULL)
		 return SZ_ERROR_MEM;
  }
  
  return res;
}

// extract files from archive
static SRes Extract(const char *archiveFile, size_t blockIndex, size_t start, size_t end) 
{
	CFileOutStream outStream;
	CFileSeqInStream archStream;
	CSzFile arch;
	UInt64 indexOffset = 0;
	UInt32 offsetsCount;
	size_t i = 0;
	UInt64 offset;
	SRes res = SZ_OK;
	size_t sz;
	UInt64 pos;
	Byte *buf = NULL;
	size_t bufsize = 0;

	// create vtables of input and output streams
	FileOutStream_CreateVTable(&outStream);
	File_Construct(&outStream.file);
	FileSeqInStream_CreateVTable(&archStream);
	File_Construct(&archStream.file);

	// open archive file
	if (InFile_OpenEx(&archStream.file, archiveFile) != 0)
	  return PrintError(rs, "Can not open archive file");
	arch = archStream.file;

	// get format version
	pos = 0;
	sz = t4;
	File_Read(&arch, &pos, &sz);
	if (pos != magic)  {
		return PrintError(rs, "Bad archive format");
	}
	pos = 0;
	sz = t2;
	File_Read(&arch, &pos, &sz);
	if (pos != majorVer) {
		return PrintError(rs, "Bad archive format - unsupported version");
	}
	pos = 0;
	sz = t2;
	File_Read(&arch, &pos, &sz);
	if (pos != minorVer) {
		return PrintError(rs, "Bad archive format - unsupported version");
	}

	// read index offset
	indexOffset = 0;
	sz = t4;
	File_Read(&arch,&indexOffset,&sz);
	// read index size
	File_Seek(&arch,&indexOffset,SEEK_SET);
	sz = t4;
	File_Read(&arch,&offsetsCount,&sz);
	indexOffset += t4;

	printf("Extracting...\n");

	if (blockIndex != 0) {
		if (blockIndex > offsetsCount) {
			return PrintError(rs, "Wrong block index specified");
		}
		// get offset of wanted block index
		offset = indexOffset+(blockIndex-1)*t4;
		// seek to offset of index
		File_Seek(&arch,&offset,SEEK_SET);
		// read index
		offset = 0;
		sz = t4;
		File_Read(&arch,&offset,&sz);
		// seek to start of block (find in index)
		File_Seek(&arch, &offset,SEEK_SET);

		// create output stream
		sprintf(rs,"%d",blockIndex);
	    if (OutFile_Open(&outStream.file, rs ) != 0)
			return PrintError(rs, "Can not open archive file");

		// extract
		res = Decode(&archStream.s,&buf,&bufsize, &start, &end);
		// write file
		File_Write(&outStream.file,buf,&bufsize);

		// destruct allocated stuff
		free(buf);
		File_Close(&outStream.file);
		File_Close(&archStream.file);
		printf("Done\n");
		return res;
	}
	
	// extract all data
	for( i=0; i< offsetsCount; ++i) {
		start = 0;
		end = INT_MAX;
		offset = indexOffset+i*t4;
		File_Seek(&arch,&offset,SEEK_SET);
		offset = 0;
		sz = t4;
		File_Read(&arch,&offset,&sz);
		File_Seek(&arch, &offset,SEEK_SET);

		sprintf(rs,"%d",i+1);
	    if (OutFile_Open(&outStream.file, rs ) != 0)
			return PrintError(rs, "Can not open archive file");

		res = Decode(&archStream.s,&buf, &bufsize, &start, &end);

		File_Write(&outStream.file,buf,&bufsize);
		File_Close(&outStream.file);
		free(buf);
	}
	
	File_Close(&archStream.file);
	printf("Done\n");
	return res;
}

SRes File_Restart(CSzFile *p, const char *name)
{
	Int64 pos = 0;
	SRes res = SZ_OK;
	FlushFileBuffers(p->handle);

	File_Close(p);
	if ( (res = OutFile_OpenEx(p, name)) != 0)
		return res;

	File_Seek(p,&pos,SEEK_END);
	return 0;
}

// append files to archive
static SRes AppendFiles(const char *archiveFile, const char *addFiles[], size_t outLength) 
{
	CFileSeqInStream inStream;
	CFileOutStream archStream;
	CSzFile out;
	UInt32 *offsets = NULL;
	UInt32 *oldOffsets = NULL;
	UInt32 oldOffsetsCount = 0;
	UInt64 pos = 0;
	UInt64 indexOffset = 0;
	size_t i;
	SRes res = SZ_OK;
	int create = 0;
	size_t sz;

	// create vtables of input and output streams
	FileSeqInStream_CreateVTable(&inStream);
	File_Construct(&inStream.file);
	FileOutStream_CreateVTable(&archStream);
	File_Construct(&archStream.file);
	
	// allocate memory to save offsets of blocks
	offsets = (UInt32*)malloc(outLength*t4);
	if (offsets == NULL)
		return PrintError(rs, kCantAllocateMessage);

	// open archive file
	if (OutFile_OpenEx(&archStream.file, archiveFile) != 0)
	  return PrintError(rs, "Can not open archive file");
	out = archStream.file;

	// get length of archive file
 	File_GetLength(&out,&pos);
	if (pos >= UINT32_MAX)
		return PrintError(rs, "Too long file (>4GB)");

	// find if we want to create file (ie. length == 0)
	create = pos==0?1:0;
 	if (pos < t4*2+2*t2 && pos!= 0) {
 		create = 1;
 		pos = 0;
 		File_Seek(&out,&pos,SEEK_SET);
 	}
	if (!create) {
		// get format version
		pos = 0;
		sz = t4;
		File_Read(&out, &pos, &sz);
		if (pos != magic)  {
			return PrintError(rs, "Bad archive format");
		}
		pos = 0;
		sz = t2;
		File_Read(&out, &pos, &sz);
		if (pos != majorVer) {
		   return PrintError(rs, "Bad archive format version");
		}
		pos = 0;
		sz = t2;
		File_Read(&out, &pos, &sz);
		if (pos != minorVer) {
			return PrintError(rs, "Bad archive format version");
		}

		// get offset of index structure
		indexOffset = 0;
		sz = t4;
		File_Read(&out,&indexOffset,&sz);
		// read offsets
		if (indexOffset != 0) {
			File_Seek(&out,&indexOffset,SEEK_SET);
			sz = t4;
			File_Read(&out,&oldOffsetsCount,&sz);
			oldOffsets = (UInt32 *)malloc(oldOffsetsCount*t4);
			if (oldOffsets == NULL)
				return PrintError(rs, kCantAllocateMessage);
			for(i=0;i<oldOffsetsCount;++i) {
				sz = t4;
				File_Read(&out,oldOffsets+i,&sz);
			}
		}
	}

	if (create) {
		// write header + reserve 4B for index offset
		sz = t4;
		File_Write(&out, &magic, &sz);
		sz = t2;
		File_Write(&out, &majorVer, &sz);
		sz = t2;
		File_Write(&out, &minorVer, &sz);
		sz = t4;
		File_Write(&out,&indexOffset,&sz);

		SetEndOfFile(out.handle);
		File_GetLength(&out,&pos);
		offsets[0] = (UInt32)pos;
	}
	else {
		// set file offset to end of last compressed block
		offsets[0] = (UInt32)indexOffset;
		File_Seek(&out,&indexOffset,SEEK_SET);
	}

	// append input files to archive
	for(i=0;i<outLength;++i) {
		UInt64 fileSize;
		 
		// open input file
		if (InFile_Open(&inStream.file, addFiles[i]) != 0) {
			PrintErrorNoExit(rs, "Can not open input file");
			outLength = i;
			break;
		}

		// compress input file
		File_GetLength(&inStream.file, &fileSize);
		res = Encode(&archStream.s, &inStream.s, fileSize);
		File_Close(&inStream.file);

		if (res != SZ_OK) {
			printf("Error encoding file %s\n",addFiles[i]);
			break;
		}

		// set EOF marker - because of problems with longer files
		SetEndOfFile(out.handle);

		// align data to 4bytes
		File_GetLength(&out,&indexOffset);
		if (indexOffset % 4 != 0) {
			int m = indexOffset % 4;
			int j;
			Byte data[4];
			j = 4-m;
			memset(data, 0,j);
			File_Write(&out,&data,&j);
			SetEndOfFile(out.handle);
			indexOffset +=4-m;
		}

		if (res != SZ_OK) {
			printf("Error encoding file %s\n",addFiles[i]);
			break;
		}

		if (res == SZ_OK) 
			printf("File %s added as number %d\n",addFiles[i], oldOffsetsCount+i+1); 

		if (i % 10 == 0)
			fflush(stdout);

		// save block offset
		if (i != outLength-1) {
			File_GetLength(&out,&indexOffset);
			offsets[i+1] = (UInt32)indexOffset;
		}
	}

	if (indexOffset >= UINT32_MAX)
		return PrintError(rs, "Too long file (>4GB)");

	// write index offset to the header
	pos = 8;
	File_Seek(&out,&pos,SEEK_SET);
	sz = t4;
	File_Write(&out,&indexOffset,&sz);
	File_Seek(&out,&indexOffset,SEEK_SET);

	// save block index
	pos = outLength+oldOffsetsCount;
	sz = t4;
	File_Write(&out,&pos,&sz);
	for(i=0;i<oldOffsetsCount;++i) {
		sz = t4;
		File_Write(&out,&oldOffsets[i],&sz);
	}
	for(i=0;i<outLength;++i) {
		sz = t4;
		File_Write(&out,&offsets[i],&sz);
	}

	File_Close(&out);

	if (offsets != NULL)
		free(offsets);
	if (oldOffsets != NULL)
		free(oldOffsets);

	return res;
}

int main2(int numArgs, const char *args[])
{
  char c;
  int res;
  int extractMode = 0;
  const char** inFiles;
  const char* outfile;
  int i,j;
  int numfiles;

  if (numArgs == 1)
  {
    PrintHelp(rs);
    return 1;
  }

  if (numArgs < 3 || strlen(args[1]) != 1)
    return PrintUserError(rs);

  c = args[1][0];
  extractMode = (c == 'e' || c == 'E');
  if (!extractMode && c != 'a' && c != 'A')
    return PrintUserError(rs);

  {
    if (t4 != 4 || t8 != 8)
      return PrintError(rs, "Incorrect UInt32 or UInt64");
  }

  if (numArgs > 3)
  {
	for(i=2;i<numArgs;++i) {
		if (strlen(args[i])>0 && args[i][0]=='-') {
			if (i+1 >= numArgs)	{
				PrintHelp(rs);
				return 1;
			}
			if (strcmp(args[i],"-a") == 0) {
				++i;
				algo = atoi(args[i]);
				continue;
			}
			if (strcmp(args[i],"-d") == 0) {
				++i;
				dictSize = 1 << atoi(args[i]);
				continue;
			}
			if (strcmp(args[i],"-fb") == 0) {
				++i;
				fb = atoi(args[i]);
				continue;
			}
			if (strcmp(args[i],"-lc") == 0) {
				++i;
				lc = atoi(args[i]);
				continue;
			}
			if (strcmp(args[i],"-lp") == 0) {
				++i;
				lp = atoi(args[i]);
				continue;
			}
			if (strcmp(args[i],"-pb") == 0) {
				++i;
				pb = atoi(args[i]);
				continue;
			}
			if (strcmp(args[i],"-mc") == 0) {
				++i;
				mc = atoi(args[i]);
				continue;
			}
			if (strcmp(args[i],"-l") == 0) {
				++i;
				level = atoi(args[i]);
				continue;
			}			
		}
		else break;
	}
	// get input files names
	if (i >= numArgs) {
		PrintHelp(rs);
		return 1;
	}
	
	outfile = args[i];
	
	++i;
	inFiles = (char**)malloc((numArgs-i)*sizeof(char*));
	if (inFiles == NULL) 
		return PrintError(rs, kCantAllocateMessage);
	for(j=i; j<numArgs; ++j) {
		inFiles[j-i] = args[j];
	}
	numfiles = numArgs-i;
  }
  else if (!extractMode)
    PrintUserError(rs);

  if (!extractMode)
  {
		res = AppendFiles(outfile,inFiles,numfiles);
  }
  else
  {
	  int ind = 0;
	  int start = 0;
	  int end = INT_MAX;
	  if (numArgs > 3) {
		ind = atoi(args[3]);
		if (numArgs > 4) {
			start = atoi(args[4]);
			if (numArgs > 5) {
				end = atoi(args[5]);
			}
		}
	  }
	  res = Extract(args[2],ind,start,end);
  }

  if (res != SZ_OK)
  {
    if (res == SZ_ERROR_MEM)
      return PrintError(rs, kCantAllocateMessage);
    else if (res == SZ_ERROR_DATA)
      return PrintError(rs, kDataErrorMessage);
    else if (res == SZ_ERROR_WRITE)
      return PrintError(rs, kCantWriteMessage);
    else if (res == SZ_ERROR_READ)
      return PrintError(rs, kCantReadMessage);
    return PrintErrorNumber(rs, res);
  }
  return 0;
}

int MY_CDECL main(int numArgs, const char *args[])
{

  int res = main2(numArgs, args);
  if (res != SZ_OK)
	printf(rs);
  return res;
}

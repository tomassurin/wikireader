/**
 * @file
 * This file contains IndexSort class that is used to compress raw title index file.
 *
 * @author Sam Larbi - Author of original n-way sort implementation.
 * <a href="http://www.codeodor.com/index.cfm/2007/5/14/Re-Sorting-really-BIG-files---the-Java-source-code/1208">Source of his implementation</a>
 * @author Tomáš Šurín
 *
 * @version 1.0
 */

package wikiconvert;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/** Implementation of external mergesort algorithm. */
public class IndexSort {
    /// List of created files.
    private List<File> files = new LinkedList<File>();
    /// Limit of rows from input file that can be sorted in memory.
    private int limit = 100000;

    /** Convert byte input to unsigned value.
     * @param inp Input number.
     * @return Unsigned value of inp.
     */
    private int con(byte inp)
    {
        int out = inp;
        if (out < 0) {
            out = 256+out;
        }
        return out;
    }

    /** Compare byte string representations in UTF-8 encoding.
     * @param a Byte representation of first string.
     * @param b Byte representation of second string.
     * @return 0 if strings are equal. -1 if a < b. 1 if a > b.
     */
    private int compare(byte[] a, byte[] b)
    {
        
        int min;
        int asz = a.length;
        int bsz = b.length;
        if (asz < bsz)
            min = asz;
        else 
            min = bsz;
        for(int i=0;i<min;++i) {
            if (con(a[i]) < con(b[i]))
                return -1;
            if (con(a[i]) > con(b[i]))
                return 1;
        }

        if (asz == bsz) {
            return 0;
        }
        if (asz < bsz)
            return -1;
        else
            return 1;
    }

    /** Sort list of rows from input file with mergesort algorithm.
     * @param a List of rows to be sorted.
     * @param index Index of column which is used to compare rows.
     * @return List of rows sorted according to index column.
     * @throws UnsupportedEncodingException
     */
    private ArrayList<String[]> mergeSort(ArrayList<String[]> a, int index) throws UnsupportedEncodingException
    {
        ArrayList<String[]> left = new ArrayList<String[]>();
        ArrayList<String[]> right = new ArrayList<String[]>();
        if (a.size() <= 1) {
            return a;
        } else {
            int middle = a.size() / 2;
            for (int i = 0; i < middle; i++) {
                left.add(a.get(i));
            }
            for (int j = middle; j < a.size(); j++) {
                right.add(a.get(j));
            }
            left = mergeSort(left, index);
            right = mergeSort(right, index);
            return merge(left, right, index);
        }
    }

    /** Merge 2 lists of rows from input file.
     * @param left First list of rows.
     * @param right Second list of rows.
     * @param index Index of column which is used to compare rows.
     * @return List of rows sorted according to index column.
     * @throws UnsupportedEncodingException
     */
    private ArrayList<String[]> merge(ArrayList<String[]> left, ArrayList<String[]> right, int index) throws UnsupportedEncodingException
    {
        ArrayList<String[]> result = new ArrayList<String[]>();
        while (left.size() > 0 && right.size() > 0) {
            byte[] a = left.get(0)[index].getBytes("UTF8");
            byte[] b = right.get(0)[index].getBytes("UTF8");
            //if (left.get(0)[index].compareTo(right.get(0)[index]) <= 0) {
            if (compare(a, b) <= 0) {
                result.add(left.get(0));
                left.remove(0);
            } else {
                result.add(right.get(0));
                right.remove(0);
            }
        }
        if (left.size() > 0) {
            for (int i = 0; i < left.size(); i++) {
                result.add(left.get(i));
            }
        }
        if (right.size() > 0) {
            for (int i = 0; i < right.size(); i++) {
                result.add(right.get(i));
            }
        }
        return result;
    }

    /** Concatenate strings from input array.
     * @param a Input array of strings.
     * @return String that is concatenation of strings from array a.
     */
    private static String flattenArray(String[] a)
    {
        String separator = "\t";
        String result = "";
        for (int i = 0; i < a.length-1; i++) {
            result += a[i] + separator;
        }
        result+=a[a.length-1];
        return result;
    }

    /** Sort indexFile file according to collumn compareIndex.
     * @param indexFile Filename of input file.
     * @param index Index of column which is used to compare rows.
     */
    public void sort(String indexFile, int compareIndex)
    {
        try {
            BufferedReader in = new BufferedReader(
                                new InputStreamReader(new FileInputStream(indexFile),"UTF8")
            );
            String[] tmp = {};
            ArrayList<String[]> rows = new ArrayList<String[]>();

            int numFiles = 0;
            while (tmp != null) {
                // Read rows until we reach limit of lines
                for (int i = 0; i < limit; i++) {
                    String line = in.readLine();
                    if (line == null) {
                        tmp = null;
                        break;
                    }
                    tmp = line.split("\t");
                    rows.add(tmp);
                }
                // Sort these rows
                rows = mergeSort(rows, compareIndex);

                // Save sorted rows to file
                File f = new File(indexFile+"_chunk"+numFiles);
                files.add(f);
                PrintWriter out = new PrintWriter(f,"UTF8");
                for (int i = 0; i < rows.size(); i++) {
                    out.append(flattenArray(rows.get(i)) + "\n");
                }
                out.close();
                numFiles++;
                rows.clear();
            }

            // Merge files created in previous while cycle.
            mergeFiles(indexFile, numFiles, compareIndex);

            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    /** Merge files created in sort method.
     * @param indexFile Base name of input file that we are sorting.
     * @param numFiles Number of files to merge.
     * @param index Index of column which is used to compare rows.
     * @throws UnsupportedEncodingException
     */
    private void mergeFiles(String indexFile, int numFiles, int index) throws UnsupportedEncodingException
    {
        try {
            ArrayList<BufferedReader> mergefbr = new ArrayList<BufferedReader>();
            InputStreamReader tmpread = null;
            ArrayList<String[]> filerows = new ArrayList<String[]>();
            String name = indexFile;
            if (indexFile.contains(".")) {
               name = indexFile.substring(0,indexFile.lastIndexOf("."));
            }
            PrintWriter out = new PrintWriter(name+Main.sortedindexext,"UTF8");
            String[] tmp = null;

            boolean someFileStillHasRows = false;

            for (int i = 0; i < numFiles; i++) {
                tmpread = new InputStreamReader(new FileInputStream(files.get(i)),"UTF8");
                mergefbr.add(new BufferedReader(tmpread));
                // Get first line
                String line = mergefbr.get(i).readLine();
                if (line != null) {
                    filerows.add(line.split("\t"));
                    someFileStillHasRows = true;
                } else {
                    filerows.add(null);
                }

            }

            String[] row;
            while (someFileStillHasRows) {
                String min;
                int minIndex = 0;

                row = filerows.get(0);
                if (row != null) {
                    min = row[index];
                    minIndex = 0;
                } else {
                    min = null;
                    minIndex = -1;
                }

                // Find minimum
                for (int i = 1; i < filerows.size(); i++) {
                    row = filerows.get(i);
                    if (min != null) {
                        if (row != null) {
                            byte[] a = row[index].getBytes("UTF8");
                            byte[] b = min.getBytes("UTF8");
                            if (compare(a,b) < 0) {
                                minIndex = i;
                                min = filerows.get(i)[index];
                            }
                        }
                    } else {
                        if (row != null) {
                            min = row[index];
                            minIndex = i;
                        }
                    }
                }

                if (minIndex < 0) {
                    someFileStillHasRows = false;
                } else {
                    // Append to sorted file
                    out.append(flattenArray(filerows.get(minIndex)) + "\n");

                    // Get another line from file that had minimum
                    String line = mergefbr.get(minIndex).readLine();
                    if (line != null) {
                        filerows.set(minIndex, line.split("\t"));
                    } else {
                        filerows.set(minIndex, null);
                    }
                }
                // Test if there are still any rows left
                for (int i = 0; i < filerows.size(); i++) {
                    someFileStillHasRows = false;
                    if (filerows.get(i) != null) {
                        if (minIndex < 0) {
                            System.out.println("minIndex lt 0 and found row not null");
                            System.exit(-1);
                        }
                        someFileStillHasRows = true;
                        break;
                    }
                }

                // Again test files for available rows
                if (!someFileStillHasRows) {
                    for (int i = 0; i < filerows.size(); i++) {
                        if (filerows.get(i) == null) {
                            String line = mergefbr.get(i).readLine();
                            if (line != null) {
                                someFileStillHasRows = true;
                                filerows.set(i, line.split("\t"));
                            }
                        }
                    }
                }
            }

            // Cleanup
            out.close();
            for (int i = 0; i < mergefbr.size(); i++) {
                mergefbr.get(i).close();
            }

            for(File f:files) {
                f.delete();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }
}

/**
 * @file
 * This file contains IndexCreate class. This class is used to create final title index.
 * 
 * @author Tomáš Šurín
 *
 * @version 1.0
 */

package wikiconvert;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.LinkedList;

/** Class which create final title index. This index is implementation of index-sequential file. */
public class IndexCreate
{
    /// Limit of index block size.
    public long blocksize = 20*1024;
    /// True if we are indexing multivolume archive.
    public boolean multivolume = false;
    /// Number of levels of index.
    private long levels = 0;

    /// Name of input file without extension.
    private String name = null;
    /// BufferedReader for input file.
    private BufferedReader in = null;

    /** Constructor.
     * @param f Input file name.
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    public IndexCreate(String f) throws FileNotFoundException, UnsupportedEncodingException
    {
         in = new BufferedReader(new InputStreamReader(new FileInputStream(f),"UTF8"));

         if (f.contains(".")) {
            name = f.substring(0,f.lastIndexOf("."));
         }
    }

    /** Converts input number into Bigendian unsigned number of specified length.
     * @param num Input number.
     * @param len Length of output number (in bytes).
     * @return Bigendian unsigned representation of input number.
     */
    private byte[] convertNumber(long num, int len)
    {
        byte[] out = new byte[len];
        Arrays.fill(out, (byte)0);
        int i = len-1;
        while(i >= 0 && num != 0) {
            out[i--] = (byte) (num % 256);
            num /=256;
        }
        return out;
    }

    /** Help structure used to store index element name and offset in index file. */
    private class IndexCreateData
    {
        /// Index element name.
        String str = null;
        /// Index element offset in index file.
        long offset = 0;
    }

    /** Write end block element to output file.
     * @param out Output file.
     * @param masterlevel True if we want end of primary file index else false.
     * @throws IOException
     */
    private void writeBlockEnd(RandomAccessFile out, boolean masterlevel) throws IOException
    {
        byte[] MAXINT4 = { (byte)255, (byte)255, (byte)255, (byte)255 };
        if (masterlevel)
            out.write(0);
        out.write("##BLOCKEND##".getBytes("UTF8"));
        out.write(0);
        out.write(MAXINT4);
    }

    /** This method will create final title index.
     * @throws IOException
     */
    public void create() throws IOException
    {
        levels = 0;
        
        String[] tmp = {};
        // Here we will save name of the first index element of block
        List<IndexCreateData> newnames = new LinkedList<IndexCreateData>();
        List<IndexCreateData> names;
        IndexCreateData tmpdata;
        // last index level starting offset
        long blocklength = 0;
        String line;

        File outfile = new File(name+Main.indexext);
        RandomAccessFile out = new RandomAccessFile(outfile, "rw");
        
        byte[] bytes = null;
        long temp;

        // First we reserve place for header.
        if (multivolume)
            out.write(convertNumber(1, 1));
        else
            out.write(convertNumber(0,1));
        out.write(convertNumber(0,1));
        out.write(convertNumber(0, 4));

        // Create primary index file
            newnames.clear();
            while ((line = in.readLine())!=null) {
                    tmp = line.split("\t");
                    if (tmp.length != 5)
                        continue;

                    // If this line is first of the block save it's name
                    if (blocklength == 0) {
                        tmpdata = new IndexCreateData();
                        tmpdata.str = tmp[0];
                        tmpdata.offset = out.getFilePointer();
                        newnames.add(tmpdata);
                    }

                    // Write name
                    out.write(0);
                    out.write(tmp[0].getBytes("UTF8"));
                    out.write(0);

                    // Write volume (if multi volume data archive)
                    if (multivolume) {
                        blocklength+=tmp[0].length()+1+13;
                        temp = Long.parseLong(tmp[1]);
                        out.write(convertNumber(temp,1));
                    }
                    else
                        blocklength+=tmp[0].length()+1+12;

                    // Write block number
                    temp = Long.parseLong(tmp[2]);
                    out.write(convertNumber(temp,4));
                    // Write start offset
                    temp = Long.parseLong(tmp[3]);
                    out.write(convertNumber(temp,4));
                    // Write end offset
                    temp = Long.parseLong(tmp[4]);
                    out.write(convertNumber(temp,4));

                    // Find if we want to create new block.
                    if (blocklength >= blocksize) {
                        blocklength = 0;
                    }
            }
            blocklength = 0;
            names = newnames;
            writeBlockEnd(out, true);

            // Write index level if index is only 0-leveled
            if (names.size() == 1) {
                out.write(names.get(0).str.getBytes("UTF8"));
                out.write(0);
                out.write(convertNumber(names.get(0).offset,4));
                writeBlockEnd(out, false);
            }

        // Write index levels
        // They contain: "name\0block" (block which we index)
        while(names.size() > 1) {
            ++levels;
            blocklength = 0;
            newnames = new ArrayList<IndexCreateData>();

            for(int i=0;i<names.size();++i) {
                // If this line is first of the block save it's name
                if (blocklength == 0) {
                    tmpdata = new IndexCreateData();
                    tmpdata.str = names.get(i).str;
                    tmpdata.offset = out.getFilePointer();
                    newnames.add(tmpdata);
                }

                blocklength += names.get(i).str.length()+1+4;
                // Write name
                out.write(names.get(i).str.getBytes("UTF8"));
                out.write(0);
                // Write index offset
                out.write(convertNumber(names.get(i).offset,4));
                if (blocklength >= blocksize) {
                    blocklength = 0;
                }
            }
            names = newnames;
            blocklength = 0;
            writeBlockEnd(out, false);
        }

        // Write header (number of levels and index of last level block)
        out.seek(1);
        out.write(convertNumber(levels, 1));
        out.write(convertNumber(names.get(0).offset, 4));
        out.close();
    }
}

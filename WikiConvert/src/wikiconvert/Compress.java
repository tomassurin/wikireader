/**
 * @file
 * This file contains Compress class.
 *
 * @author Tomáš Šurín
 * @version 1.0
 */

package wikiconvert;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

/** Compress class is used to start external compression utility to compress input files. */
public class Compress
{

    /// Filename of output archive.
    private String filename;
    /// Contains command line arguments.
    private String[] cmd;
    /// File object that represents archive file. This object is used to get archive file size.
    private File file;

    /** Constructor.
     * @param f Filename of output archive.
     */
    public Compress(String f)
    {
        filename = f;
        file = new File(filename);
        file.delete();
        file = new File(filename);

        if (Main.compressCmd.isEmpty()) {
            cmd = new String[4];
            cmd[0] = "wzip";
            cmd[1] = "a";
            cmd[2] = "-d";
            cmd[3] = "16";
        }
        else
            cmd = Main.compressCmd.split("\\s+");
    }

    /** Getter method for output archive filename.
     * @return Output archive filename.
     */
    public String getFilename()
    {
        return filename;
    }

    /** Get archive filesize.
     * @return Output archive file size.
     */
    public long getSize()
    {
        return file.length();
    }

    /** Append files to output archive.
     * @param files List of files that will be appended to output archive.
     * @throws IOException
     */
    public synchronized void append(List<File> files) throws IOException
    {
          Runtime run = Runtime.getRuntime();

          // Create command line
          List<String> command = new LinkedList<String>();
          if (cmd.length != 0) {
              for(String s: cmd)
                command.add(s);
          }
          command.add(filename);
          for(File f:files) {
            command.add(f.getName());
          }

          // Run external process
          ProcessBuilder procbuild = new ProcessBuilder(command);
          procbuild.redirectErrorStream(true);
          Process proc = procbuild.start();
          try {
            final BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            // Create thread which will get external program command line.
            (new Thread( new Runnable() {
                @Override
                public void run() {
                    try {
                        String out = null;
                        while ((out = in.readLine()) != null) {
                            System.out.println(out);
                            System.out.flush();
                        }
                    } catch (IOException ex) {
                        
                    }
                }
             })).start();
             // Clean
            proc.waitFor();
            proc.getInputStream().close();
            proc.getOutputStream().close();
            proc.getErrorStream().close();
          } 
          catch (InterruptedException ex) { }
    }
}

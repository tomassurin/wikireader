package wikiconvert;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Arrays;

public class IndexCreateCompressed
{
    private List<File> files = new LinkedList<File>();
    private Compress zip;

    public long filelimit = 100;
    public long blocksize = 20*1024;
    public boolean multivolume = false;

    long block = 1;
    private String localtempext = "index";

    public long getLastBlock() {
        return block;
    }

    private String name = null;
    private BufferedReader in = null;

    IndexCreateCompressed(String f) throws FileNotFoundException, UnsupportedEncodingException
    {
         in = new BufferedReader(new InputStreamReader(new FileInputStream(f),"UTF8"));

         if (f.contains(".")) {
            name = f.substring(0,f.lastIndexOf("."));
         }
         zip = new Compress(name+Main.zipindexext);
    }

    private void compress(List<File> files) throws IOException
    {
        zip.append(files);

        // clean created temporary files
        for(File f:files) {
            f.delete();
        }
        files.clear();
    }

    private byte[] convertNumber(long num, int len) {
        byte[] out = new byte[len];
        Arrays.fill(out, (byte)0);
        int i = len-1;
        while(i > 0 && num != 0) {
            out[i--] = (byte) (num % 256);
            num /=256;
        }
        return out;
    }

    void create() throws IOException
    {
        block = 1;
        files.clear();
        
        String[] tmp = {};
        // here we will save name of the first index of block
        List<String> newnames = new ArrayList<String>();
        List<String> names;
        // last index level starting block
        long lastlevelblock = 1;
        long currentlevelblock;
        String line;

        File f = new File(block+Main.tempext+localtempext);
        DataOutputStream out = new DataOutputStream(new FileOutputStream(f));
        long filelength = 0;
        byte[] bytes = null;
        long temp;

        // write primary index
            newnames.clear();
            currentlevelblock = 1;
            while ((line = in.readLine())!=null) {
                    tmp = line.split("\t");
                    if (tmp.length != 5)
                        continue;

                    // if this line is first of the block save it's name
                    if (filelength == 0) {
                        newnames.add(tmp[0]);
                    }

                    // name
                    out.write(tmp[0].getBytes());
                    out.write(0);

                    // data file (if multi volume data file - TODO!)
                    if (multivolume) {
                        filelength+=tmp[0].length()+1+12;
                        temp = Long.parseLong(tmp[1]);
                        out.write(convertNumber(temp,1));
                    }
                    else
                        filelength+=tmp[0].length()+1+13;
                    
                    // block
                    temp = Long.parseLong(tmp[2]);
                    out.write(convertNumber(temp,4));
                    // start
                    temp = Long.parseLong(tmp[3]);
                    out.write(convertNumber(temp,4));
                    // end
                    temp = Long.parseLong(tmp[4]);
                    out.write(convertNumber(temp,4));

                    if (filelength >= blocksize) {
                        filelength = 0;
                        out.close();
                        files.add(f);
                        block++;
                        f = new File(block+Main.tempext+localtempext);
                        out = new DataOutputStream(new FileOutputStream(f));
                        if (files.size() >= filelimit) {
                            compress(files);
                        }
                    }
            }

            // end last opened file
            if (f.length() > 0) {
                out.close();
                files.add(f);
            }
            else {
                --block;
                f.delete();
            }
            // compress still uncompressed files
            if (!files.isEmpty()) {
                compress(files);
            }
            lastlevelblock = currentlevelblock;
            names = newnames;
            files.clear();

        // write index levels
        // contains: name\0block (block which we index)
        while(names.size() > 1) {
            currentlevelblock = ++block;
            f = new File(block+Main.tempext+localtempext);
            out = new DataOutputStream(new FileOutputStream(f));
            filelength = 0;
            newnames = new ArrayList<String>();

            for(int i=0;i<names.size();++i) {
                // if this line is first of the block save it's name
                if (filelength == 0) {
                    newnames.add(names.get(i));
                }

                filelength += names.get(i).length()+1+4;
                // name
                out.write(names.get(i).getBytes());
                out.write(0);
                // block index
                out.write(convertNumber(lastlevelblock+i,4));
                if (filelength >= blocksize) {
                    filelength = 0;
                    out.close();
                    files.add(f);
                    block++;
                    f = new File(block+Main.tempext+localtempext);
                    out = new DataOutputStream(new FileOutputStream(f));
                    if (files.size() >= filelimit) {
                        compress(files);
                    }
                }
            }
            
            // end last opened file
            if (f.length() > 0) {
                out.close();
                files.add(f);
            }
            else {
                --block;
                f.delete();
            }
            // compress still uncompressed files
            if (!files.isEmpty()) {
                compress(files);
            }
            lastlevelblock = currentlevelblock;
            names = newnames;
            files.clear();
        }   

        System.out.println("Last index block:"+(block));
    }
}

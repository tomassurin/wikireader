/**
 * @file
 * This file contains class XMLRead. This class parses database export and create raw index.
 *
 * @author Tomáš Šurín
 * @version 1.0
 *
 */

package wikiconvert;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/** Class XMLRead parses database export and creates raw index. */
public class XMLRead {
    /// Set of excluded namespaces ids.
    private Set<Integer> exclNames = new HashSet<Integer>();
    /// Map of original namespaces names with their according ids.
    private Map<String,Integer> orignamespaces = new HashMap<String, Integer>();
    /// Map of lowercase namespaces names with their according ids.
    private Map<String,Integer> namespaces = new HashMap<String, Integer>();
    /// List of files which are not compressed yet.
    private List<File> files = null;

    /// Array of language prefixes - used to remove language links.
    private static final String[] languagePrefixes =
    { "ace", "af", "ak", "als", "am", "ang", "ab", "ar", "an", "arc",
    "roa-rup", "frp", "as", "ast", "gn", "av", "ay", "az", "bm", "bn",
    "zh-min-nan", "nan", "map-bms", "ba", "be", "be-x-old", "bh", "bcl",
    "bi", "bar", "bo", "bs", "br", "bg", "bxr", "ca", "cv", "ceb", "cs",
    "ch", "cbk-zam", "ny", "sn", "tum", "cho", "co", "cy", "da", "dk",
    "pdc", "de", "dv", "nv", "dsb", "dz", "mh", "et", "el", "eml", "en",
    "myv", "es", "eo", "ext", "eu", "ee", "fa", "hif", "fo", "fr", "fy",
    "ff", "fur", "ga", "gv", "gd", "gl", "gan", "ki", "glk", "gu",
    "got", "hak", "xal", "ko", "ha", "haw", "hy", "hi", "ho", "hsb",
    "hr", "io", "ig", "ilo", "bpy", "id", "ia", "ie", "iu", "ik", "os",
    "xh", "zu", "is", "it", "he", "jv", "kl", "kn", "kr", "pam", "krc",
    "ka", "ks", "csb", "kk", "kw", "rw", "ky", "rn", "sw", "kv", "kg",
    "ht", "ku", "kj", "lad", "lbe", "lo", "la", "lv", "lb", "lt", "lij",
    "li", "ln", "jbo", "lg", "lmo", "hu", "mk", "mg", "ml", "mt", "mi",
    "mr", "arz", "mzn", "ms", "cdo", "mwl", "mdf", "mo", "mn", "mus",
    "my", "nah", "na", "fj", "nl", "nds-nl", "cr", "ne", "new", "ja",
    "nap", "ce", "pih", "no", "nb", "nn", "nrm", "nov", "ii", "oc",
    "mhr", "or", "om", "ng", "hz", "uz", "pa", "pi", "pag", "pnb",
    "pap", "ps", "km", "pcd", "pms", "tpi", "nds", "pl", "tokipona",
    "tp", "pnt", "pt", "aa", "kaa", "crh", "ty", "ksh", "ro", "rmy",
    "rm", "qu", "ru", "sah", "se", "sm", "sa", "sg", "sc", "sco", "stq",
    "st", "tn", "sq", "scn", "si", "simple", "sd", "ss", "sk", "cu",
    "sl", "szl", "so", "ckb", "srn", "sr", "sh", "su", "fi", "sv", "tl",
    "ta", "kab", "roa-tara", "tt", "te", "tet", "th", "ti", "tg", "to",
    "chr", "chy", "ve", "tr", "tk", "tw", "udm", "bug", "uk", "ur",
    "ug", "za", "vec", "vi", "vo", "fiu-vro", "wa", "zh-classical",
    "vls", "war", "wo", "wuu", "ts", "yi", "yo", "zh-yue", "diq", "zea",
    "bat-smg", "zh", "zh-tw", "zh-cn" };

    /// Set of language prefixes (contains languagePrefixes contents).
    private Set<String> langpr = new HashSet<String>();

    {
        for(String s: languagePrefixes)
            langpr.add(s);
    }

    /// Input stream of input database export.
    private BufferedInputStream in = null;
    /// Compression object.
    private Compress zip = null;
    /// Number of actual data archive volume.
    private long datafile = 0;
    /// Number of actual block in archive volume.
    private long block = 1;
    /// Number of articles.
    private long articleNo;
    /// Local extension for temporary files.
    private String localtempext = "xml";

    /// Name of database export file.
    private String filename = null;
    /// Filename of database export file without extension.
    private String name = null;
    /// Basename of database export project (i.e. "http://en.wikipedia.org/Main_page").
    public String basename = null;
    /// Sitename of database export project (i.e. "Wikipedia").
    public String sitename = null;
    /// Property of article case.
    public String caseprop = null;
    /// First letter case property (true if caseprop == "first-letter").
    public boolean firstlettercase = false;
    /// Setting to process article links.
    public boolean processLinks = true;
    /// Setting to remove language links.
    public boolean removeLanguageLinks = true;
   
    /// Archive file block size limit.
    public long blocksize = 100*1024;
    /// Number of files to compress in one call of Compress.append().
    public long filelimit = 100;
    /// Limit for size of archive file volume (in bytes).
    public long sizelimit = 1073741824;

    /// XML parser state: Outside of plausible elements.
    private final int GARBAGE = 0;
    /// XML parser state: Inside title element.
    private final int TITLE = 1;
    /// XML parser state: Inside article element.
    private final int ARTICLE = 2;
    /// XML parser state: Inside namespaces section.
    private final int NAMESPACES = 3;
    /// XML parser state: Inside namespace element.
    private final int NAMESPACE = 4;
    /// XML parser state: Inside sitename element.
    private final int SITENAME = 5;
    /// XML parser state: Inside basename element.
    private final int BASENAME = 6;
    /// XML parser state: Inside case element.
    private final int CASE = 7;
    /// XML parser state: Inside siteinfo section.
    private final int SITEINFO = 8;
    /// XML parser state: Inside revision section.
    private final int REVISION = 9;
    /// XML parser state: Inside link (this state is not xml parser state but wiki article state).
    private final int LINK = 10;

    /** @return Number of data archive's volumes.  */
    public long getVolumes()
    {
        return datafile+1;
    }

    /** @return Number of articles in data file.  */
    public long getArticles()
    {
        return articleNo;
    }

    /** Add namespace to definition.
     * @param name Namespace localized prefix.
     * @param id Namespace id.
     */
    public void addNamespace(String name, int id)
    {
        orignamespaces.put(name, id);
        namespaces.put(name.toLowerCase(Locale.ENGLISH), id);
    }

    /** Constructor.
     * @param filename Name of input file (database export).
     */
    public XMLRead(String filename)
    {
        try {
            in = new BufferedInputStream(new FileInputStream(filename));
        } catch(FileNotFoundException ex) {
            System.exit(1);
        }

        addNamespace("Image", 500);
        addNamespace("File",501);
        // standard project titles and shortcuts
        /*
        namespaces.put("wikipedia",500);
        namespaces.put("w",500);
        namespaces.put("wiktionary",501);
        namespaces.put("wikt",501);
        namespaces.put("wikinews",502);
        namespaces.put("n",502);
        namespaces.put("wikibooks",503);
        namespaces.put("b",503);
        namespaces.put("wikiquote",504);
        namespaces.put("q",504);
        namespaces.put("wikisource",505);
        namespaces.put("s",505);
        namespaces.put("wikispecies",506);
        namespaces.put("species",506);
        namespaces.put("wikiversity",507);
        namespaces.put("v",507);
        namespaces.put("wikimedia",508);
        namespaces.put("foundation",508);
        namespaces.put("wmf",508);
        namespaces.put("commons",509);
        namespaces.put("chapter",510);
        namespaces.put("metawikipedia",511);
        namespaces.put("meta",511);
        namespaces.put("m",511);
        namespaces.put("incubator",512);
        namespaces.put("mw",513);
         */
        
        readDumpInfo();
        this.filename = filename;
        if (filename.contains(".")) {
            name = filename.substring(0,filename.lastIndexOf("."));
        }
        zip = new Compress(name+Main.zipext);
    }

    /** Exclude namespace with id key.
     * @param key Namespace id to exclude.
     */
    public void excludeNamespace(int key)
    {
        exclNames.add(key);
    }

    /** Include namespace with id key.
     * @param key Namespace id to include.
     */
    public void includeNamespace(int key)
    {
        exclNames.remove(key);
    }

    /** Write excluded namespaces to stdout. */
    public void writeExcluded() {
        if (exclNames.isEmpty())
            return;
        System.out.println("Excluded namespaces:");
        for(String s :namespaces.keySet()) {
            if (exclNames.contains(namespaces.get(s)))
                System.out.println(s);
        }
        System.out.println();
    }

    /** Get excluded namespaces map.
     * @return Map of excluded namespaces.
     */
    public Map<Integer,String> getExcludedNamespaces()
    {
        Map<Integer,String> out = new TreeMap<Integer, String>();
        for(String s:orignamespaces.keySet()) {
            if (exclNames.contains(orignamespaces.get(s)))
                out.put(orignamespaces.get(s), s);
        }
        return out;
    }

    /** Get included namespaces map.
     * @return Map of included namespaces.
     */
    public Map<Integer,String> getIncludedNamespaces()
    {
        Map<Integer,String> out = new TreeMap<Integer, String>();

        for(String s:orignamespaces.keySet()) {
            if (!exclNames.contains(orignamespaces.get(s))) {
                out.put(orignamespaces.get(s), s);
            }
        }
        return out;
    }

    /** Get original namespaces map.
     * @return Map of original namespaces.
     */
    public Map<String,Integer> getNamespaces()
    {
        return orignamespaces;
    }

    /** Reads tag from input stream.
     * @param in Input stream from we are reading.
     * @return Tag contents.
     * @throws IOException
     */
    private static String readTag(InputStream in) throws IOException
    {
        int s ;
        StringBuilder tmp = new StringBuilder();
        while ((s = in.read()) != -1) {
            if (s == '>')
                break;
            tmp.append((char)s);
        }
        return tmp.toString();
    }

    /** Convert list of bytes to string.
     * @param input List of bytes representing string in UTF-8 encoding.
     * @return String that was represented by input list.
     * @throws UnsupportedEncodingException
     */
    private static String byteArrayToString(List<Byte> input) throws UnsupportedEncodingException
    {
            Byte[] t = input.toArray(new Byte[0]);
            byte[] titlestr = new byte[t.length];
            for(int i=0;i<t.length;++i) {
                titlestr[i] = (byte)t[i];
            }
            return new String(titlestr,"UTF8");
    }

    /** Convert string to list of bytes.
     * @param input Input string.
     * @return List of bytes representing input string in UTF-8 encoding.
     */
    private static List<Byte> stringToByteArray(String input)
    {
        List<Byte> out = new LinkedList<Byte>();
        byte[] buf = input.getBytes();
        for(int i=0;i < buf.length;++i)
            out.add(buf[i]);
        return out;
    }

    /** Read database dump info section. */
    private void readDumpInfo()
    {
        int s;
        int state = GARBAGE;
        List<Byte> title = new LinkedList<Byte>();
        Pattern namespaceregexp = Pattern.compile("namespace key=\"(.*)\"");
        int key = 0;
        try {
            while (( s = in.read()) != -1) {
                 if (s == '<') {
                    String tmp = readTag(in);
                    if (state == GARBAGE && tmp.startsWith("siteinfo")) {
                        state = SITEINFO;
                        continue;
                    }
                    if (state == SITEINFO && tmp.startsWith("sitename")) {
                        state = SITENAME;
                        title.clear();
                        continue;
                    }
                    if (state == SITENAME && tmp.startsWith("/sitename")) {
                        state = SITEINFO;
                        sitename = byteArrayToString(title);
                        continue;
                    }
                    if (state == SITEINFO && tmp.startsWith("base")) {
                        state = BASENAME;
                        title.clear();
                        continue;
                    }
                    if (state == BASENAME && tmp.startsWith("/base")) {
                        state = SITEINFO;
                        basename = byteArrayToString(title);
                        continue;
                    }
                    if (state == SITEINFO && tmp.startsWith("case")) {
                        state = CASE;
                        title.clear();
                        continue;
                    }
                    if (state == CASE && tmp.startsWith("/case")) {
                        state = SITEINFO;
                        caseprop = byteArrayToString(title);
                        firstlettercase = !caseprop.equals("first-letter");
                        continue;
                    }
                    if (state == SITEINFO && tmp.startsWith("namespaces")) {
                        state = NAMESPACES;
                        continue;
                    }
                    if (state == NAMESPACES && tmp.startsWith("namespace")) {
                        state = NAMESPACE;
                        Matcher match = namespaceregexp.matcher(tmp);
                        if (match.find()) {
                            try {
                                key = Integer.parseInt(match.group(1));
                            } catch (NumberFormatException ex) {
                                key = Integer.MIN_VALUE;
                            }
                        }
                        title.clear();
                    }
                    if (state == NAMESPACE && (tmp.startsWith("/namespace") || tmp.endsWith("/"))) {
                        state = NAMESPACES;
                        String str = byteArrayToString(title);
                        addNamespace(str, key);
                        /*if (key == 0)
                            namespaces.put("Main",key);
                        else namespaces.put(str,key);
                         */
                        continue;
                    }
                    if (state == NAMESPACES && tmp.startsWith("/namespaces")) {
                        state = SITEINFO;
                        break;
                    }
                    if (state == SITEINFO && tmp.startsWith("/siteinfo")) {
                        break;
                    }
                    if (tmp.startsWith("page")) {
                        break;
                    }
                    continue;
                }
                if (state == NAMESPACE || state == BASENAME || state == SITENAME
                                       || state == CASE) {
                    title.add((byte)s);
                    continue;
                }
            }
        } catch(IOException ex) {
            namespaces.clear();
        }
    }

    /** Convert number to byte list representation. This list represents string containing input number.
     * @param n Input number.
     * @return String representing number n in byte form.
     */
    private List<Byte> getDigits(long n)
    {
        List<Byte> out = new LinkedList<Byte>();
        if (n==0) {
            out.add((byte)48);
            return out;
        }
        while (n > 0) {
            out.add((byte) (48 + (n % 10)));
            n = n / 10;
        }
        Collections.reverse(out);
        return out;
    }

    /** Compress list of files using Compress object.
     * @param files List of files to compress.
     * @throws IOException
     */
    private void compress(List<File> files) throws IOException
    {
        zip.append(files);
        
        // we are at compressed file limit => create new compressed file and ++datafile
        if (zip.getSize() >= sizelimit) {
            ++datafile;
            block = 0;
            zip = new Compress(name+Main.zipext+datafile);
        }

        // clean created temporary files
        for(File f:files) {
           f.delete();
        }
        files.clear();
    }

    /** Canonize name of wiki title.
     * @param input Input title.
     * @return Canonized title.
     */
    private String canonizeName(String input)
    {
	if (input.isEmpty())
	   return input;

	String inp = input;

	// replace _ -> space
        inp = input.replace('_', ' ');

	// capitalize first character
	if (!firstlettercase) {
            return Character.toUpperCase(inp.charAt(0)) + inp.substring(1);
	}
	return inp;
    }

    /** Resolve namespace id of wiki title.
     * @param title Input title.
     * @return Id of title namespace.
     */
    private int namespaceToId(String title)
    {
        String namespace;
        if (title.contains(":")) {
            namespace = title.substring(0,title.indexOf(':'));
        }
        else {
            namespace = "";
        }

        namespace = namespace.toLowerCase(Locale.ENGLISH);
        if (namespaces.containsKey(namespace))
            return namespaces.get(namespace);
        else
            return 0;
    }

    /** Find if wiki title is language link.
     * @param title Input title.
     * @return True if title is language link.
     */
    private boolean isLanguageLink(String title)
    {
        if (title.contains(":")) {
            return langpr.contains(title.substring(0,title.indexOf(':')));
        }
        else {
            return false;
        }
    }

    /** Find if namespace of wiki title is included.
     * @param title Input title.
     * @return True if namespace is included.
     */
    private boolean includedNamespace(String title)
    {
        if (exclNames.contains(namespaceToId(title))) 
            return false;
        else
            return true;
    }

    /** Read database dump. Create temporary files with articles, compress them and create raw index.
     * @throws IOException
     */
    public void read() throws IOException
    {
        datafile = 0;
        block = 1;
        articleNo = 0;

        boolean langRem = false;
        
        BufferedOutputStream out = null;
        // in.available returns file length as 32bit so we needed to use File.length()
        File input = new File(filename);

        PrintWriter index = new PrintWriter(name+Main.rawindexext,"UTF8");

        File file = null;

        files = new LinkedList<File>();

        int s;
        long tmpindex = 0;
        int state = GARBAGE;
        List<Byte> title = new LinkedList<Byte>();
        List<Byte> link = new LinkedList<Byte>();
        List<Byte> article = new LinkedList<Byte>();
        HashSet<String> titles = new HashSet<String>();
        long bytesavail = input.length();
        long read = 0;
        int k = 0;
        String temp;
        String titlestr = "";

        if (processLinks) {
            System.out.println("Reading titles...");
            // read whole file and save titles to database - we need this to find if link is included in data file.
            while (( s = in.read()) != -1) {
                ++read;
                if (s == '<') {
                    String tmp = readTag(in);
                    if (state == GARBAGE && tmp.startsWith("title")) {
                        state = TITLE;
                        title.clear();
                        continue;
                    }
                    if (state == TITLE && tmp.startsWith("/title") ) {
                        state = REVISION;
                        titlestr = byteArrayToString(title);
                        String namespace;

                        if (!includedNamespace(titlestr)) {
                            continue;
                        }

                        titles.add(canonizeName(titlestr));

                        ++k;
                        if (k > 1000) {
                            k = 0;
                            double dat = (read*100.0/bytesavail);
                            System.out.printf("%1$2.3g %% %2$s \n",dat, titlestr);
                        }
                        continue;
                    }
                    if (state == REVISION && tmp.startsWith("/revision")) {
                        state = GARBAGE;
                        continue;
                    }
                }
                if (state == TITLE) {
                    title.add((byte)s);
                    continue;
                }
            }
            System.out.println("DONE: Reading titles...");

            read = 0;
            k = 0;
            in.close();
            in = new BufferedInputStream(new FileInputStream(filename));
        }
        System.out.println("Reading database export...\n");
        // read database export
        while (( s = in.read() )!= -1) {
            ++read;
            // remove newline after language link
            if (langRem && s == '\n') {
                langRem = false;
                continue;
            }

            // end wrong links
            if (state == LINK && s== '[') {
                state = ARTICLE;
                article.add((byte)'[');
                article.add((byte)'[');
                article.addAll(link);
            }

            // process links start
            if (state == ARTICLE && s == '[') {
                s = in.read();
                if (s == -1) {
                    article.add((byte)'[');
                    continue;
                }

                if (s == '[') {
                    state = LINK;
                    link.clear();
                    continue;
                }
                else {
                    article.add((byte)'[');
                }
            }

            // process tags
            if (s == '<') {
                if (state == LINK) {
                    state = ARTICLE;
                    article.add((byte)'[');
                    article.add((byte)'[');
                    article.addAll(link);
                }
                String tmp = readTag(in);
                if ( state == GARBAGE && tmp.startsWith("title")) {
                    state = TITLE;
                    title.clear();
                    continue;
                }
                if (state == TITLE && tmp.startsWith("/title") ) {
                    state = GARBAGE;
                    // decode title
                    titlestr = byteArrayToString(title);
                    continue;
                }
                if (state == GARBAGE && tmp.startsWith("text")) {
                    state = ARTICLE;

                    if (!includedNamespace(titlestr)) {
                        state = GARBAGE;
                        continue;
                    }
                    
                    article.addAll(title);
                    article.add((byte)'\n');
                    article.add((byte)'\n');
                    continue;
                }
                if (state == ARTICLE && tmp.startsWith("/text")) {
                    ++articleNo;
                    state = REVISION;
                    article.add((byte)'\n');

                    if (article.size() >= blocksize ) {
                        // create temporary file with article data
                        file = new File(datafile+"_"+block+Main.tempext+localtempext);
                        out = new BufferedOutputStream(new FileOutputStream(file,false));
                        for(Byte b: article) {
                            out.write(b);
                        }
                        out.close();
                        files.add(file);

                        // append data to index
                        index.append(titlestr+"\t"+datafile+"\t"+block+"\t"+tmpindex+"\t"+article.size()+"\n");

                        if (files.size() >= filelimit || files.size() >= sizelimit) {
                            // compress files
                            compress(files);
                            files = new LinkedList<File>();
                        }
                        
                        article.clear();
                        tmpindex = 0;
                        ++block;
                    }
                    else {
                        // append data to index
                        index.append(titlestr+"\t"+datafile+"\t"+block+"\t"+tmpindex+"\t"+article.size()+"\n");
                        tmpindex = article.size();
                    }

                    ++k;
                    if (k > 100) {
                        double dat = (read*100.0/bytesavail);
                        System.out.printf("%1$2.3g %% %2$s \n",dat, titlestr);
                    }
                    continue;
                }
                if (state == REVISION && tmp.startsWith("/revision")) {
                    state = GARBAGE;
                    continue;
                }
            }
            // add data to title
            if (state == TITLE) {
                title.add((byte)s);
                continue;
            }
            // add data to article
            if (state == ARTICLE) {
                article.add((byte)s);
                continue;
            }
            // process links (and also some wrong links handlers)
            if (state == LINK) {
                if (s == ']') {
                    state = ARTICLE;
                    s = in.read();
                    if (s == -1) {
                        article.add((byte)'[');
                        article.add((byte)'[');
                        article.addAll(link);
                        article.add((byte)']');
                        continue;
                    }

                    if (s == ']') {
                        temp = byteArrayToString(link);
                        
                        if (removeLanguageLinks && isLanguageLink(temp)) {
                            langRem = true;
                            continue;
                        }

                        if (!processLinks || temp.isEmpty() || namespaceToId(temp) != 0) {
                            article.add((byte)'[');
                            article.add((byte)'[');
                            article.addAll(link);
                            article.add((byte)']');
                            article.add((byte)']');
                        }
                        else {
                            String[] parts = temp.trim().split("\\|",2);
                            String tit = "";
                            if (parts.length >= 1 ) {
                                int ind = parts[0].indexOf('#');
                                if (ind == -1) {
                                    tit = canonizeName(parts[0]);
                                }
                                else {
                                    tit = parts[0].substring(0,parts[0].indexOf('#'));
                                    tit = canonizeName(tit);
                                }
                            }

                            if ( parts.length > 2 || titles.contains(tit)) {
                                article.add((byte)'[');
                                article.add((byte)'[');
                                article.addAll(link);
                                article.add((byte)']');
                                article.add((byte)']');
                            }
                            else {
                                // not available link
                                article.add((byte)'[');
                                article.add((byte)'[');
                                article.add((byte)'@');
                                article.add((byte)':');
                                article.addAll(link);
                                article.add((byte)']');
                                article.add((byte)']');
                            }
                        }
                    }
                    else {
                        article.add((byte)'[');
                        article.add((byte)'[');
                        article.addAll(link);
                        article.add((byte)']');
                        article.add((byte)s);
                    }
                    continue;
                }
                else if (s == '\n') {
                    state = ARTICLE;
                    article.add((byte)'[');
                    article.add((byte)'[');
                    article.addAll(link);
                    article.add((byte)'\n');
                }
                else {
                    link.add((byte)s);
                }
            }
        }
        // write last block
        if (article.size() != 0) {
            file = new File(datafile+"_"+block+Main.tempext+localtempext);
            out = new BufferedOutputStream(new FileOutputStream(file,false));
            for(Byte b: article) {
                out.write(b);
            }
            out.close();
            files.add(file);
            article.clear();
        }

        // compress still uncompressed files
        if (files.size() != 0) {
            compress(files);
            files = null;
        }

        file = new File(zip.getFilename());
        if (file.length() == 0) {
            --datafile;
            file.delete();
        }

        System.out.println("DONE: Reading database export\n");

        index.close();
    }
}
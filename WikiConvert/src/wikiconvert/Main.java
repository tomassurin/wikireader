/** @mainpage
<h1> WikiConvert </h1>

WikiConvert's purpose is to convert MediaWiki database exports into format that is
acceptable for use in program WikiReader.
*/

/**
 * @file
 * Main file of program WikiConvert, which is used to create data files from wikipedia
 * dumps for use in program WikiReader.
 *
 * @author Tomáš Šurín
 * @version 1.0
 *
 * @todo Add categories support
 */

package wikiconvert;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/** Main program class. */
public class Main {
    /// Usage string.
    public static String usage = "Usage: java -jar WikiConvert.jar [<switches>] <filename> \n"+
                                 "Switches:\n"+
                                 "\tzip <string>: Specify command line for compression\n"+
                                 "\tnames: Only list namespaces\n"+
                                 "\tinc <j>: Include namespace with index j\n"+
                                 "\tns <name> <id>: Add additional namespace\n"+
                                 "\tindex : only create index\n"+
                                 "\tnoclean: do not remove some of generated temporary files\n"+
                                 "\tlinks: remove unacessible links in main namespace\n"+
                                 "\tnolang: remove language links\n"+
                                 "\tb <size>: Set compressed file size limit in bytes (default: 1024*1024*1024)\n"+
                                 "\tf <size>: Set file count limit per one compression (default: 100)\n"+
                                 "\ts <size>: Set block size limit in bytes (default: 100*1024)\n"+
                                 "\ti <size>: Set index block size limit in bytes (default: 20*1024)\n";

    /// Raw title index file extension.
    public static String rawindexext = ".rawindex";
    /// Final title index file extension.
    public static String indexext = ".index";
    /// Sorted title index file extension.
    public static String sortedindexext = ".sortindex";
    /// Extension of compressed data archive file. For multi-volume archive, 
    /// ordinal number of archive volume is appended to this extension.
    public static String zipext = ".w";
    /// Extension of temporary files.
    public static String tempext = ".wt";
    /// Extension of temporary XML files.
    public static String configext = ".xmlx";
    /// Compression command.
    public static String compressCmd = "";

    /** Entry point of application.
     * @param args
     * @throws IOException
     */
     public static void main(String[] args) throws IOException
     {
        // If there are no arguments print usage string and exit.
        if (args.length == 0) {
            System.out.println(usage);
            System.exit(1);
        }

        boolean listnamespaces = false;
        Set<Integer> includednamespaces = new HashSet<Integer>();
        Map<String, Integer> addNs = new HashMap<String,Integer>();

        String filename = null;
        String tmp = null;
        long blocksize = 100*1024;
        long filelimit = 100;
        long blocklimit = 1073741824;
        long indexblocksize = 20*1024;
        long p = 0;
        boolean index = false;
        boolean noclean = false;
        boolean processLinks = false;
        boolean removeLanguageLinks = false;

        // Command line parameters
        for(int i=0;i<args.length;++i) {
            if (args[i].startsWith("-")&& args[i].length()> 1) {
                if (args[i].equals("-index")) {
                    index = true;
                    continue;
                }
                if (args[i].equals("-noclean")) {
                    noclean = true;
                    continue;
                }
                if (args[i].equals("-links")) {
                    processLinks = true;
                    continue;
                }
                if (args[i].equals("-nolang")) {
                    removeLanguageLinks = true;
                    continue;
                }
                if (args[i].equals("-names")) {
                    listnamespaces = true;
                    continue;
                }
                if (args[i].equals("-zip")) {
                    ++i;
                    if (i >= args.length) {
                        System.err.println("Bad argument to command line switches for compression program\n"+usage);
                        System.exit(1);
                    }
                    compressCmd = args[i];
                    continue;
                }
                if (args[i].equals("-ns")) {
                    ++i;
                    if (i+1 >= args.length) {
                        System.err.println("Bad arguments to add aditional namespaces\n"+usage);
                        System.exit(1);
                    }
                    tmp = args[i];
                    ++i;
                    try {
                         p = Long.parseLong(args[i]);
                    } catch (NumberFormatException ex) {
                        System.err.println("Bad argument to namespace id\n"+usage);
                        System.exit(1);
                    }
                    addNs.put(tmp, (int) p);
                    continue;
                }
                if (args[i].equals("-indexblock")) {
                    ++i;
                    if (i >= args.length) {
                        System.err.println("Bad argument to index block size limit\n"+usage);
                        System.exit(1);
                    }
                    try {
                         p = Long.parseLong(args[i]);
                    } catch (NumberFormatException ex) {
                        System.err.println("Bad argument to index block size limit\n"+usage);
                        System.exit(1);
                    }
                    indexblocksize = p;
                    continue;
                }
                if (args[i].equals("-inc")) {
                    ++i;
                    if (i >= args.length) {
                        System.err.println("Bad argument to include namespaces\n"+usage);
                        System.exit(1);
                    }
                    try {
                        p = Integer.parseInt(args[i]);
                    } catch (NumberFormatException ex) {
                        System.err.println("Bad argument to exclude namespaces\n"+usage);
                        System.exit(1);
                    }
                    includednamespaces.add((int)p);
                    continue;
                }

                switch (args[i].charAt(1)) {
                    case 'b':
                        ++i;
                        if (i >= args.length) {
                            System.err.println("Bad argument to archive size limit\n"+usage);
                            System.exit(1);
                        }
                        try {
                            p = Long.parseLong(args[i]);
                        } catch (NumberFormatException ex) {
                            System.err.println("Bad argument to archive size limit\n"+usage);
                            System.exit(1);
                        }
                        blocklimit = p;
                        break;
                    case 'f':
                        ++i;
                        if (i >= args.length) {
                            System.err.println("Bad argument to file count limit\n"+usage);
                            System.exit(1);
                        }
                        try {
                            p = Long.parseLong(args[i]);
                        } catch (NumberFormatException ex) {
                            System.err.println("Bad argument to file count limit\n"+usage);
                            System.exit(1);
                        }
                        filelimit = p;
                        break;
                    case 's':
                        ++i;
                        if (i >= args.length) {
                            System.err.println("Bad argument to block size limit\n"+usage);
                            System.exit(1);
                        }
                        try {
                             p = Long.parseLong(args[i]);
                        } catch (NumberFormatException ex) {
                            System.err.println("Bad argument to block size limit\n"+usage);
                            System.exit(1);
                        }
                        blocksize = p;
                        break;
                    case 'i':
                        ++i;
                        if (i >= args.length) {
                            System.err.println("Bad argument to index block size limit\n"+usage);
                            System.exit(1);
                        }
                        try {
                             p = Long.parseLong(args[i]);
                        } catch (NumberFormatException ex) {
                            System.err.println("Bad argument to index block size limit\n"+usage);
                            System.exit(1);
                        }
                        indexblocksize = p;
                        break;
                    default:
                        System.err.println("Unknown switch\n"+usage);
                        System.exit(1);
                        break;
                }
                continue;
            }
            if (i==args.length-1) {
                filename = args[i];
            }
            else {
                System.err.println("Filename must be specified\n"+usage);
                System.exit(1);
            }
        }
        // If filename of input file is not specified print usage and exit.
        if (filename == null) {
            System.err.println("Filename must be specified\n"+usage);
            System.exit(1);
        }
        
        // Split filename and its extension
        String name = filename;
        if (filename.contains(".")) {
            name = filename.substring(0,filename.lastIndexOf("."));
        }

        // Create XMLRead instance. And set it's parameters.
        XMLRead xml =  new XMLRead(filename);
        xml.blocksize = blocksize;
        xml.filelimit = filelimit;
        xml.sizelimit = blocklimit;
        xml.processLinks = processLinks;
        xml.removeLanguageLinks = removeLanguageLinks;
        for(Entry<String,Integer> e : addNs.entrySet()) {
            xml.addNamespace(e.getKey(), e.getValue());
        }

        // Get available namespaces
        Map<String,Integer> names = xml.getNamespaces();

        // List namespaces
        if (listnamespaces) {
            for(Entry<String,Integer> e : names.entrySet()) {
                System.out.println(e.getKey()+" : "+e.getValue());
            }
            System.exit(0);
        }

        // Include namespaces
        if (!includednamespaces.isEmpty()) {
            for(Integer val : names.values()) {
                if (!includednamespaces.contains(val))
                    xml.excludeNamespace(val);
            }
        }

        // Read XML file
        if (!index) {
            xml.read();
        }

        // Create title index
            // Sort raw title index
            System.out.println("Sorting index...\n");
            IndexSort externalsort = new IndexSort();
            externalsort.sort(name+Main.rawindexext, 0);
            System.out.println("Done: Sorting index\n");


            // Create final title index from sorted index
            System.out.println("Creating index...\n");
            IndexCreate indexcreate = new IndexCreate(name+Main.sortedindexext);
            indexcreate.blocksize = indexblocksize;
            indexcreate.multivolume = xml.getVolumes() != 1;
            indexcreate.create();
            System.out.println("Done: Creating index\n");

        // Output settings
        PrintWriter config = new PrintWriter(name+Main.configext,"UTF8");
        config.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
        config.append("<WikiFile>\n");
        config.append("  <content>WikiContent</content>\n");
        config.append("  <data>\n");
        config.append("    <volumes>"+xml.getVolumes()+"</volumes>\n");
        config.append("    <src>"+name+Main.zipext+"</src>\n");
        config.append("    <plugin>WZip</plugin>\n");
        config.append("  </data>\n");
        config.append("  <index>\n");
        config.append("    <title>\n");
        config.append("      <src>"+name+Main.indexext+"</src>\n");
        config.append("      <plugin>WikiTitleIndex</plugin>\n");
        config.append("    </title>\n");
        config.append("  </index>\n");
        config.append("  <sitename>"+xml.sitename+"</sitename>\n");
        config.append("  <basename>"+xml.basename+"</basename>\n");
        config.append("  <case>"+xml.caseprop+"</case>\n");
        config.append("  <namespaces>\n");
            for(Entry<String,Integer> e : names.entrySet()) {
                config.append("    <ns"+e.getValue()+">"+e.getKey()+"</ns"+e.getValue()+">\n");
            }
        config.append("  </namespaces>\n");
        config.append("  <contains>");
            Map<Integer,String> inclnames = xml.getIncludedNamespaces();
            boolean first = true;
            for(Entry<Integer,String> e: inclnames.entrySet()) {
                if (first) {
                    config.append("ns"+e.getKey());
                    first = false;
                }
                else
                    config.append(" ns"+e.getKey());
            }
        config.append("</contains>\n");
        config.append("  <articles>"+xml.getArticles()+"</articles>\n");
        config.append("</WikiFile>");
        config.close();
        // Delete temporary files
        if (!noclean) {
            File temp = new File(name+Main.rawindexext);
            temp.delete();
            temp = new File(name+Main.sortedindexext);
            temp.delete();
        }
    }

}
